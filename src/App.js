import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Search from './pages/Search';
import Classes from './pages/Classes';
import Faculties from './pages/Faculties';
import Faculty from './pages/Faculty';
import Users from './pages/Users';
import Class from './pages/Class';
import Personalize from './pages/Personalize';
import Student from './pages/Student';
import Login from './pages/Login';
import User from './pages/User';
import ScoreBoard from './pages/ScoreBoard';

import CurrentUserContextProvider from './providers/CurrentUserContextProvider';
import StudentListContextProvider from './providers/StudentListContextProvider';
import FacultyContextProvider from './providers/FacultyContextProvider';
import ClassListContextProvider from './providers/ClassListContextProvider';
import NoneStudentListContextProvider from './providers/NoneStudentListContextProvider';
import CurrentParentsContextProvider from './providers/CurrentParentsContextProvider';

const App = () => {
  return (
    <CurrentUserContextProvider>
      <CurrentParentsContextProvider>
        <FacultyContextProvider>
          <StudentListContextProvider>
            <ClassListContextProvider>
              <NoneStudentListContextProvider>
                <Switch>
                  <Route
                    exact
                    from="/"
                    render={(props) => <Home {...props} />}
                  />
                  <Route
                    exact
                    from="/login"
                    render={(props) => <Login {...props} />}
                  />
                  <Route
                    exact
                    from="/faculties"
                    render={(props) => <Faculties {...props} />}
                  />
                  <Route
                    exact
                    from="/faculty/:code/classes"
                    render={(props) => <Faculty {...props} />}
                  />
                  <Route
                    exact
                    from="/users"
                    render={(props) => <Users {...props} />}
                  />
                  <Route
                    exact
                    from="/classes"
                    render={(props) => <Classes {...props} />}
                  />
                  <Route
                    exact
                    from="/class/:classname"
                    render={(props) => <Class {...props} />}
                  />
                  <Route
                    exact
                    from="/student/:studentCode"
                    render={(props) => <Student {...props} />}
                  />
                  <Route
                    exact
                    from="/user/:role/:code"
                    render={(props) => <User {...props} />}
                  />
                  <Route
                    exact
                    from="/me/personalize"
                    render={(props) => <Personalize {...props} />}
                  />
                  <Route
                    exact
                    from="/me/scoreboard"
                    render={(props) => <ScoreBoard {...props} />}
                  />
                  <Route
                    exact
                    from="/search"
                    render={(props) => <Search {...props} />}
                  />
                </Switch>
              </NoneStudentListContextProvider>
            </ClassListContextProvider>
          </StudentListContextProvider>
        </FacultyContextProvider>
      </CurrentParentsContextProvider>
    </CurrentUserContextProvider>
  );
};

export default App;
