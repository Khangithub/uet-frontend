import React, {useContext} from 'react';
import clsx from 'clsx';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {Drawer, IconButton, Divider, List} from '@material-ui/core';

import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';
import LoadingModal from '../Common/LoadingModal';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DuoIcon from '@material-ui/icons/Duo';

import list from './ListItem';
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
}));

//handleDrawerClose: function, no params,
//open: bool
const Index = ({handleDrawerClose, open, history, match, location}) => {
  const classes = useStyles();
  const theme = useTheme();
  const {
    currentUser,
    getCurrentUserLoading,
    setCurrentUserLoading,
  } = useContext(CurrentUserContext);

  return getCurrentUserLoading ? (
    <LoadingModal
      loading={getCurrentUserLoading}
      setLoading={setCurrentUserLoading}
    />
  ) : (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        }),
      }}
    >
      <div className={classes.toolbar}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'rtl' ? (
            <ChevronRightIcon />
          ) : (
            <ChevronLeftIcon />
          )}
        </IconButton>
      </div>
      <Divider />
      <List>
        {list.map((listItem, index) => (
          <ListItem
            style={{
              display: listItem.visibleList.includes(currentUser.role)
                ? 'flex'
                : 'none',
            }}
            button
            key={index}
            onClick={() => history.push(`${listItem.pathname}`)}
          >
            <ListItemIcon>{listItem.icon}</ListItemIcon>
            <ListItemText primary={listItem.name} />
          </ListItem>
        ))}
        <ListItem>
          <ListItemIcon
            onClick={() =>
              window.open('https://zoom1205.herokuapp.com', '_blank')
            }
          >
            <DuoIcon fontSize="large" />
          </ListItemIcon>
          <ListItemText primary="Meeting" />
        </ListItem>
      </List>
    </Drawer>
  );
};

export default withRouter(Index);
