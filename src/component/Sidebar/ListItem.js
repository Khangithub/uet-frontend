import React from 'react';

import ClassIcon from '@material-ui/icons/Class';
import StudentIcon from '@material-ui/icons/Accessibility';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AirlineSeatReclineNormalIcon from '@material-ui/icons/AirlineSeatReclineNormal';
import ScoreIcon from '@material-ui/icons/Score';

const list = [
  {
    name: 'Faculties',
    pathname: '/faculties',
    icon: <AssessmentIcon fontSize="large" />,
    visibleList: [
      'specialist',
      'uet-leader',
      'student-affair-leader',
      'academic-leader',
    ],
    description:
      'This is a function for leaders, managers to track, statistic and edit about faculties in the school',
  },
  {
    name: 'Classes',
    pathname: '/classes',
    icon: <ClassIcon fontSize="large" />,
    visibleList: ['consultant', 'specialist', 'student-affair-leader'],
    description:
      'This is a function for leaders, managers to track, statistic and edit about classes in the school',
  },
  {
    name: 'Staff Management',
    pathname: '/users',
    icon: <AirlineSeatReclineNormalIcon fontSize="large" />,
    visibleList: ['specialist', 'student-affair-leader'],
    description:
      'This is a function for leaders, managers to track, statistic and edit about the staff in the school',
  },
  {
    name: 'Personalize',
    pathname: '/me/personalize',
    icon: <StudentIcon fontSize="large" />,
    visibleList: [
      'consultant',
      'parent',
      'student',
      'specialist',
      'uet-leader',
      'student-affair-leader',
      'academic-leader',
    ],
    description:
      'allows existing users to edit their own account profile picture and password',
  },
  {
    name: 'Score Board',
    pathname: '/me/scoreboard',
    icon: <ScoreIcon fontSize="large" />,
    visibleList: ['student'],
    description:
      'if the current user is a student, allow that student to view his / her transcripts and academic results, edit parental information',
  },
];

export default list;
