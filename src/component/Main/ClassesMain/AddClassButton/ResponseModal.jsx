import React, { useState } from 'react';
import JSONPretty from 'react-json-pretty';

import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { Backdrop, Button, Fade, Modal, Grid } from '@material-ui/core';
import flattenObject from '../../../../utils/object';
var JSONPrettyMon = require('react-json-pretty/dist/1337');

const useStyles = makeStyles((theme) => ({
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: '0px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
}));

// open: Boolean
// setOpen: Function
// responseJSON: Object
// isError: Boolean
const ResponseModal = ({ open, setOpen, responseJSON, isError, history, location, match }) => {
	const classes = useStyles();
	var flattenResponse = flattenObject(responseJSON);
	var messageKey = Object.keys(flattenResponse).filter((key, index) => {
		return index && key.includes('message');
	})[0];

	const [showFullJSON, setShowFullJSON] = useState(false);

	return (
		<div id="error">
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				className={classes.modal}
				open={open}
				onClose={() => {
					setOpen(false);
				}}
				closeAfterTransition
				BackdropComponent={Backdrop}
				BackdropProps={{
					timeout: 500,
				}}
			>
				<Fade in={open}>
					<div className={classes.paper}>
						<Grid container direction="column" justify="center" alignItems="center">
							<Grid item>
								<h4 style={{ color: isError ? '#f50057' : '#3f51b5' }}>
									{isError
										? `Error: ${flattenResponse[messageKey]}`
										: `Message: ${flattenResponse[messageKey]}`}
								</h4>
							</Grid>
							<Grid item style={showFullJSON ? {} : { display: 'none' }}>
								<JSONPretty
									style={{
										height: '400px',
										overflow: 'scroll',
									}}
									data={flattenResponse}
									theme={JSONPrettyMon}
								></JSONPretty>
							</Grid>

							<Grid container direction="row" justify="space-between" spacing={3} alignItems="center">
								<Grid item>
									<Button
										variant="contained"
										fullWidth
										onClick={() => {
											isError ? setOpen(false) : history.go();
										}}
									>
										{isError ? 'Cancel' : 'OK'}
									</Button>
								</Grid>
								<Grid item>
									<Button
										variant="contained"
										fullWidth
										color={isError ? 'secondary' : 'primary'}
										onClick={() => {
											setShowFullJSON(!showFullJSON);
										}}
									>
										{showFullJSON
											? `Hide ${isError ? 'Error' : 'Message'}`
											: `Show Full ${isError ? 'Error' : 'Message'}`}
									</Button>
								</Grid>
							</Grid>
						</Grid>
					</div>
				</Fade>
			</Modal>
		</div>
	);
};

export default withRouter(ResponseModal);
