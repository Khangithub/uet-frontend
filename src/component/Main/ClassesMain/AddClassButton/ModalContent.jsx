import React, {useEffect, useState, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Avatar,
  Button,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Grid,
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import ResponseModal from '../../../Common/ResponseModal';
import LoadingModal from '../../../Common/LoadingModal';
import {toCapitalize} from '../../../../utils/string';
import {ClassListContext} from '../../../../providers/ClassListContextProvider';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const ModalContent = () => {
  const classes = useStyles();
  const {setClassList} = useContext(ClassListContext);
  const [course, setCourse] = useState({
    classname: '',
    consultant: '',
    faculty: '',
    schoolYear: '',
    excel: '',
  });

  const [selectedExcel, setSelectedExcel] = useState('');
  const [consultantList, setConsultantList] = useState([]);
  const [facultyList, setFacultyList] = useState([]);
  const [open, setOpen] = useState(false);
  const [sending, setSending] = useState(false);
  const [getConsultantListLoading, setConsultantListLoading] = useState(true);
  const [getFacultyListLoading, setFacultyListLoading] = useState(true);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        var consultantListResponse = await fetch(
          'https://uet-backend.herokuapp.com/consultants'
        );
        var consultantListJson = await consultantListResponse.json();

        var facultyListResponse = await fetch(
          'https://uet-backend.herokuapp.com/faculties/'
        );
        var facultyListJson = await facultyListResponse.json();

        if (consultantListJson.docs.length && facultyListJson.docs.length) {
          setConsultantListLoading(false);
          setFacultyListLoading(false);
          setConsultantList(consultantListJson.docs);
          setFacultyList(facultyListJson.docs);
        }
      } catch (err) {
        console.error(err);
        JSON.stringify(err);
      }
    };
    fetchData();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    setSending(true);
    var {classname, consultant, schoolYear, faculty, excel} = course;
    var formData = new FormData();
    formData.append('classname', classname);
    formData.append('startYear', schoolYear);
    formData.append('isStartYear', true);
    formData.append('consultant', consultant);
    formData.append('faculty', faculty);
    formData.append('schoolYear', schoolYear);
    formData.append('semester', 1);
    formData.append('excel', excel);

    fetch('https://uet-backend.herokuapp.com/classes/', {
      method: 'POST',
      headers: {
        Accept: 'multipart/form-data',
      },
      body: formData,
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
          var classListJson = await classListResponse.json();

          if (classListJson.docs.length) {
            setSending(false);
            setClassList(classListJson.docs);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          console.error('AddClassButton', err);
          setSending(false);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
        setSending(false);
      });
  };

  return getConsultantListLoading || getFacultyListLoading ? (
    <CircularProgress size={160} />
  ) : (
    <div className={classes.root}>
      <form enctype="multipart/form-data">
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="text"
          label="Classname"
          name="classname"
          fullWidth
          onChange={(event) =>
            setCourse({...course, classname: event.target.value})
          }
        />
        <br />
        <TextField
          type="number"
          label="SchoolYear"
          name="schoolYear"
          fullWidth
          onChange={(event) =>
            setCourse({...course, schoolYear: event.target.value})
          }
        />
        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="consultant-select">Consultant</InputLabel>
          <Select
            labelId="consultant-select"
            onChange={(event) =>
              setCourse({...course, consultant: event.target.value})
            }
          >
            {consultantList.map((consultantListItem, index) => {
              const {
                _id,
                fullname,
                academicRank,
                profileImage,
              } = consultantListItem;
              return (
                <MenuItem key={index} value={_id}>
                  <Avatar
                    alt={fullname}
                    src={profileImage}
                    style={{marginRight: '10px'}}
                  />
                  {toCapitalize(academicRank)}. {fullname}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>

        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="faculty-select">Faculty</InputLabel>
          <Select
            labelId="faculty-select"
            onChange={(event) =>
              setCourse({...course, faculty: event.target.value})
            }
          >
            {facultyList.map((facultyListItem, index) => {
              const {code, facultyName, _id} = facultyListItem;
              return (
                <MenuItem key={index} value={_id}>
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    spacing={4}
                  >
                    <Grid item>{facultyName}</Grid>
                    <Grid item>
                      <small>{code}</small>
                    </Grid>
                  </Grid>
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>

        <br />

        <input
          className={classes.input}
          id="icon-button-file"
          type="file"
          accept=".xlsx"
          onChange={(event) => {
            setCourse({...course, excel: event.target.files[0]});
            setSelectedExcel(event.target.files[0].name);
          }}
        />

        <label htmlFor="icon-button-file">
          <IconButton
            color="primary"
            aria-label="upload picture"
            component="span"
          >
            <AttachFileIcon fontSize="large" /> {selectedExcel}
          </IconButton>
        </label>
        <br />

        <Button
          color="primary"
          variant="contained"
          fullWidth
          onClick={handleSubmit}
          type="submit"
        >
          Add Class
        </Button>
      </form>
      <LoadingModal loading={sending} setLoading={setSending} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </div>
  );
};

export default ModalContent;
