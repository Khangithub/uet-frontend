import React, {useState, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {CurrentUserContext} from '../../../../providers/CurrentUserContextProvider';
import {
  Backdrop,
  Fade,
  Grid,
  Button,
  Modal,
  CircularProgress,
} from '@material-ui/core';
import ModalContent from './ModalContent';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

const Index = () => {
  const classes = useStyles();
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const [open, setOpen] = useState(false);

  return getCurrentUserLoading ? (
    <CircularProgress size={160} />
  ) : (
    <Grid
      item
      style={{
        display: currentUser.isManager ? 'flex' : 'none',
      }}
    >
      <Button
        color="primary"
        size="large"
        variant="contained"
        style={{margin: '20px 0px'}}
        onClick={() => {
          setOpen(true);
        }}
      >
        Add class
      </Button>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <ModalContent />
          </div>
        </Fade>
      </Modal>
    </Grid>
  );
};

export default Index;
