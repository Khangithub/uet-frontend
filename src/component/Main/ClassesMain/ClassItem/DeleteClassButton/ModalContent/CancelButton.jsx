import React from 'react';
import { Button } from '@material-ui/core';

//setOpen: Function
const CancelButton = ({ setOpen }) => {
	return (
		<>
			<Button
				color="default"
				variant="text"
				fullWidth
				style={{ margin: '10px 0px' }}
				onClick={() => setOpen(false)}
			>
				Cancel
			</Button>
		</>
	);
};

export default CancelButton;
