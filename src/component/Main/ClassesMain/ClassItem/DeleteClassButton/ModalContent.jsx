import React from 'react';
import { Button, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { formatClassname } from '../../../../../utils/text';

const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	input: {
		display: 'none',
	},
}));

// selectedClass: String
// setOpen: Function
const ModalContent = ({ selectedClass, setOpen, history, location, match }) => {
	const classes = useStyles();

	const handleSubmit = (event) => {
		event.preventDefault();

		fetch(`http://localhost:5000/classes/${selectedClass}`, {
			method: 'DELETE',
			headers: {
				'Content-type': 'application/json',
			},
			// body: JSON.stringify({ classname: selectedClass }),
		})
			.then((response) => {
				return response.json();
			})
			.then((json) => {
				history.go();
			})
			.catch((error) => {
				console.error(error);
			});
	};
	return (
		<div className={classes.root}>
			<form>
				<Typography variant="h6" gutterBottom>
					<strong> Are you sure you want to delete the class {formatClassname(selectedClass)} ?</strong>
				</Typography>

				<Button
					color="secondary"
					variant="contained"
					fullWidth
					style={{ margin: '10px 0px' }}
					type="submit"
					onClick={handleSubmit}
				>
					Delete Class {formatClassname(selectedClass)}
				</Button>

				<Button
					color="default"
					variant="text"
					fullWidth
					style={{ margin: '10px 0px' }}
					onClick={() => setOpen(false)}
				>
					Cancel
				</Button>
			</form>
		</div>
	);
};

export default withRouter(ModalContent);
