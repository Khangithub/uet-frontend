import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Backdrop, Fade, IconButton, Modal} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import ModalContent from './ModalContent';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

// classname: String
// selectedConsultant: String, ObjectID
const Index = ({
  currentUser,
  classname,
  selectedConsultant,
  selectedFaculty,
}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  return (
    <div
      style={{
        display: currentUser.isManager ? 'flex' : 'none',
      }}
    >
      <IconButton>
        <EditIcon
          onClick={() => {
            setOpen(true);
          }}
        />
      </IconButton>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <ModalContent
              classname={classname}
              selectedConsultant={selectedConsultant}
              selectedFaculty={selectedFaculty}
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default Index;
