import React from 'react';
import {formatClassname, formatAcademicRank} from '../../../../utils/string';

import {withRouter} from 'react-router-dom';
import {Avatar, Grid, Paper} from '@material-ui/core';

import UpdateClassButton from './UpdateClassButton/';
import DeleteClassButton from './DeleteClassButton/';

//classList: Array
const ClassItem = ({classList, currentUser, history, location, match}) => {
  return (
    <Grid container spacing={2} direction="column" justify="flex-start">
      {classList
        .filter((classListItem) => {
          if (currentUser.role !== 'consultant') {
            return classListItem;
          }

          return classListItem.consultant.code === currentUser.code;
        })
        .map((classListItem, index) => {
          const {consultant, faculty, classname} = classListItem;
          const {facultyName} = faculty;
          const {profileImage, fullname, academicRank} = consultant;

          return (
            <Grid container spacing={3} style={{margin: '2px 0px'}} key={index}>
              <Grid
                item
                xs={currentUser.isManager ? 10 : 12}
                onClick={() => {
                  history.push(`/class/${classname}`);
                }}
              >
                <Paper
                  component="div"
                  elevation={8}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <div>
                    <strong>{formatClassname(classname)}</strong>
                  </div>

                  <div>
                    <Avatar alt={fullname} src={profileImage} />
                  </div>

                  <div>
                    <em>{`${formatAcademicRank(
                      academicRank
                    )}. ${fullname}`}</em>
                  </div>

                  <div>|</div>

                  <div>{facultyName}</div>
                </Paper>
              </Grid>
              <Grid item xs={currentUser.isManager ? 2 : 0}>
                <Paper
                  component="div"
                  elevation={0}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <UpdateClassButton
                    currentUser={currentUser}
                    classname={classname}
                    selectedConsultant={consultant._id}
                    selectedFaculty={faculty._id}
                  />
                  <DeleteClassButton
                    currentUser={currentUser}
                    classname={classname}
                  />
                </Paper>
              </Grid>
            </Grid>
          );
        })}
    </Grid>
  );
};

export default withRouter(ClassItem);
