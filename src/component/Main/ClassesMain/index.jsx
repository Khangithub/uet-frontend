import React, {useContext} from 'react';
import ClassChart from './ClassChart';
import ClassItem from './ClassItem/';
import AddClassButton from './AddClassButton/';
import {ClassListContext} from '../../../providers/ClassListContextProvider';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';

import {Grid} from '@material-ui/core/';
import LoadingModal from '../../Common/LoadingModal';

const Index = () => {
  const {classList, getClassListLoading, setClassListLoading} = useContext(
    ClassListContext
  );

  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);

  return (
    <div style={{marginTop: '65px'}}>
      {getClassListLoading || getCurrentUserLoading ? (
        <LoadingModal
          loading={getClassListLoading}
          setLoading={setClassListLoading}
        />
      ) : (
        <>
          <ClassChart classList={classList} currentUser={currentUser}/>
          <Grid container spacing={3}>
            <AddClassButton />
          </Grid>
          <ClassItem classList={classList} currentUser={currentUser}/>
        </>
      )}
    </div>
  );
};

export default Index;
