import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { AppBar, Box, Tabs, Tab, Typography } from '@material-ui/core';

import Overview from './Overview/';
import AcademicTrainningScore from './AcademicTrainningScore/';
import AcademicStudyHandler from './AcademicStudyHandler/';
import { StudentListContext } from '../../../providers/StudentListContextProvider';
import LoadingModal from '../../Common/LoadingModal';

const TabPanel = ({ children, value, index, ...other }) => {
	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`full-width-tabpanel-${index}`}
			aria-labelledby={`full-width-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box p={3}>
					<Typography>{children}</Typography>
				</Box>
			)}
		</div>
	);
};

const a11yProps = (index) => {
	return {
		id: `full-width-tab-${index}`,
		'aria-controls': `full-width-tabpanel-${index}`,
	};
};

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: theme.palette.background.paper,
		width: 500,
	},
}));

// classname:  String
const Index = ({ classname }) => {
	var { getStudentListLoading, setStudentListLoading, setStudentList } = useContext(StudentListContext);
	const classes = useStyles();
	const theme = useTheme();
	const [value, setValue] = useState(0);

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	const handleChangeIndex = (index) => {
		setValue(index);
	};

	useEffect(() => {
		fetch(`https://uet-backend.herokuapp.com/students/from/class/${classname}`)
			.then((response) => {
				return response.json();
			})
			.then((json) => {
				setStudentListLoading(false);
				setStudentList(json.docs);
			})
			.catch((error) => {
				setStudentListLoading(false);
				console.error(error);
				alert(JSON.stringify(error));
			});
	}, [classname, setStudentListLoading, setStudentList]);

	return (
		<div>
			{getStudentListLoading ? (
				<LoadingModal loading={getStudentListLoading} setLoading={setStudentListLoading} />
			) : (
				<div className={classes.root} style={{ width: '1000px' }}>
					<AppBar position="static" color="transparent" style={{ width: '1000px' }}>
						<Tabs
							value={value}
							onChange={handleChange}
							indicatorColor="primary"
							textColor="primary"
							variant="fullWidth"
							aria-label="full width tabs example"
						>
							<Tab label="Overview" {...a11yProps(0)} />
							<Tab label="Academic Training Score" {...a11yProps(1)} />
							<Tab label="handling academic studies" {...a11yProps(2)} />
						</Tabs>
					</AppBar>
					<SwipeableViews
						axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
						index={value}
						onChangeIndex={handleChangeIndex}
					>
						<TabPanel value={value} index={0} style={{ width: '1000px' }}>
							<Overview />
						</TabPanel>
						<TabPanel value={value} index={1}>
							<AcademicTrainningScore />
						</TabPanel>
						<TabPanel value={value} index={2}>
							<AcademicStudyHandler />
						</TabPanel>
					</SwipeableViews>
				</div>
			)}
		</div>
	);
};

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
};

export default Index;
