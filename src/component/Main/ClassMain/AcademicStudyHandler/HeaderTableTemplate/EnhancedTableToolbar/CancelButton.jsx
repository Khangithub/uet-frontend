import React from 'react';
import { IconButton } from '@material-ui/core';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import CancelIcon from '@material-ui/icons/Cancel';

// setSelected: Function, no params
const CancelButton = ({ setSelected }) => {
	return (
		<Tippy content="Cancel" placement="bottom">
			<IconButton aria-label="delete" onClick={() => setSelected([])}>
				<CancelIcon fontSize="large" />
			</IconButton>
		</Tippy>
	);
};

export default CancelButton;
