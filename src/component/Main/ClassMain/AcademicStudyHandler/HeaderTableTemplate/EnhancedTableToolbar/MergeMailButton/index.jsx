import React, {useContext, useEffect, useState} from 'react';
import {
  IconButton,
  AppBar,
  Toolbar,
  Slide,
  Dialog,
  Stepper,
  Step,
  StepLabel,
  Typography,
  Button,
  Grid,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import Cookies from 'universal-cookie';

import MailListSetup from './MailListSetup/';
import WriteMailTemplate from './WriteMailTemplate';
import PreviewMail from './PreviewMail';

import {StudentListContext} from '../../../../../../../providers/StudentListContextProvider';
import {CurrentUserContext} from '../../../../../../../providers/CurrentUserContextProvider';
import {getAllGPAFlattenScoreList} from '../../../../utils/array';
import {convertMailTemplate} from '../../../../utils/string';

import PeopleIcon from '@material-ui/icons/People';
import CloseIcon from '@material-ui/icons/Close';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';

import LoadingModal from '../../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../../Common/ResponseModal';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  paper: {
    outline: 'none',
    backgroundColor: 'white',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Index = ({selected, setSelected}) => {
  const classes = useStyles();
  const [openDialog, setOpenDialog] = useState(false);
  const [openResponseModal, setOpenResponseModal] = useState(false);
  const {studentList} = useContext(StudentListContext);
  const {currentUser} = useContext(CurrentUserContext);
  const [mailList, setMailList] = useState([]);
  const [mailTitleList, setMailTitleList] = useState([]);
  const [mailTemplate, setMailTemplate] = useState('');
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  useEffect(() => {
    var selectedWarningStudents = getAllGPAFlattenScoreList(studentList)
      .filter((student) => {
        return selected.includes(student._id);
      })
      .map((student) => {
        const {
          _id,
          vnumail,
          parentsMail,
          consultant,
          fullname,
          classname,
          code,
          semester,
          schoolYear,
          reason,
        } = student;

        return {
          _id,
          mails: [vnumail, ...parentsMail.parentsMailList, consultant.vnumail],
          dadFullname: parentsMail.dadFullname,
          momFullname: parentsMail.momFullname,
          consultant,
          fullname,
          classname,
          code,
          semester,
          schoolYear,
          reason,
        };
      });
    setMailList(selectedWarningStudents);
    setMailTitleList(Array(selectedWarningStudents.length).fill(''));
  }, [studentList, selected]);

  const [activeStep, setActiveStep] = useState(0);

  function getSteps() {
    return ['Set up mail list', 'Select Template', 'Preview Mail'];
  }

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <MailListSetup
            mailList={mailList}
            setMailList={setMailList}
            mailTitleList={mailTitleList}
            setMailTitleList={setMailTitleList}
          />
        );
      case 1:
        return (
          <WriteMailTemplate
            mailTemplate={mailTemplate}
            setMailTemplate={setMailTemplate}
          />
        );
      case 2:
        return (
          <PreviewMail
            mailList={mailList}
            setMailList={setMailList}
            mailTitleList={mailTitleList}
            setMailTitleList={setMailTitleList}
            mailTemplate={mailTemplate}
            setMailTemplate={setMailTemplate}
          />
        );
      default:
        return 'Unknown stepIndex';
    }
  }

  const steps = getSteps();

  const handleNext = () => {
    if (activeStep === steps.length - 1) {
      var mailOptionList = mailList.map((mailListItem, index) => {
        var {mails} = mailListItem;
        var mailContent = convertMailTemplate(
          mailTemplate,
          mailListItem,
          currentUser
        );
        var mailTitle = mailTitleList[index];
        return {mails, mailContent, mailTitle};
      });

      setLoading(true);
      fetch('https://uet-backend.herokuapp.com/users/merge/mail', {
        method: 'POST',
        headers: {
          Authorization: 'Bearer '.concat(token),
          'content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(mailOptionList),
      })
        .then((response) => {
          setError(response.status === 200 ? false : true);
          return response.json();
        })
        .then((json) => {
          setOpenResponseModal(true);
          setResponseJSON(json);
          setLoading(false);
          console.log(json);
        })
        .catch((err) => {
          setLoading(false);
          console.log(err);
        });
    } else {
      return setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  const handleOpen = () => {
    setOpenDialog(true);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  return (
    <>
      <Tippy content="Merged Mail" placement="bottom">
        <IconButton onClick={handleOpen}>
          <PeopleIcon fontSize="small" />
        </IconButton>
      </Tippy>

      <Dialog
        fullScreen
        open={openDialog}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              fontSize="large"
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>

        <div className={classes.paper}>
          <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <div>
            {activeStep === steps.length ? (
              <div>
                <Typography className={classes.instructions}>
                  All steps completed
                </Typography>
                <Button onClick={handleReset}>Reset</Button>
              </div>
            ) : (
              <div style={{margin: '0px 20px'}}>
                <Grid container justify="flex-end" alignItems="flex-end">
                  <Grid item>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={handleNext}
                    >
                      {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.backButton}
                    >
                      Back
                    </Button>
                  </Grid>
                </Grid>

                <Typography className={classes.instructions}>
                  {getStepContent(activeStep)}
                </Typography>
              </div>
            )}
          </div>
        </div>
        <LoadingModal loading={loading} setLoading={setLoading} />
        <ResponseModal
          open={openResponseModal}
          setOpen={setOpenResponseModal}
          responseJSON={responseJSON}
          isError={isError}
        />
      </Dialog>
    </>
  );
};

export default Index;
