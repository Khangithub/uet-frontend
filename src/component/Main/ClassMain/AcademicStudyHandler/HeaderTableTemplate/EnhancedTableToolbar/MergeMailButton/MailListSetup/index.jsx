import React from 'react';
import MuiChipRoot from 'material-ui-chip-input';
import {makeStyles} from '@material-ui/core/styles';

import {
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Paper,
} from '@material-ui/core';

import DeleteMail from './DeleteMail';

import SelectAllIcon from '@material-ui/icons/SelectAll';

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: 'hidden',
  },
  table: {
    width: '100%',
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
}));

// selected: Array
const Index = ({mailList, setMailList, mailTitleList, setMailTitleList}) => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper} className={classes.root}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>
              <h5>Index</h5>
            </TableCell>
            <TableCell>
              <h5>Mail Title</h5>
            </TableCell>
            <TableCell>
              <h5>Mails</h5>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {mailList.map((mailListItem, index) => {
            var {mails} = mailListItem;

            return (
              <TableRow key={index}>
                <TableCell>{index + 1}</TableCell>
                <TableCell>
                  <TextField
                    label="Title"
                    multiline={true}
                    required={true}
                    rows={2}
                    rowsMax={30}
                    defaultValue={mailTitleList[index]}
                    onChange={(event) => {
                      var newMailTitle = [...mailTitleList];
                      newMailTitle[index] = event.target.value;
                      setMailTitleList(newMailTitle);
                    }}
                    value={mailTitleList[index]}
                  />
                  <IconButton
                    onClick={() => {
                      setMailTitleList(
                        Array(mailTitleList.length).fill(mailTitleList[index])
                      );
                    }}
                  >
                    <SelectAllIcon fontSize="small" />
                  </IconButton>
                </TableCell>
                <TableCell>
                  <MuiChipRoot
                    defaultValue={mails}
                    label="Receivers"
                    onChange={(event) => {
                      var newMailList = [...mailList];
                      newMailList[index].mails = event;
                      setMailList(newMailList);
                    }}
                  />
                </TableCell>

                <TableCell>
                  <DeleteMail
                    mailList={mailList}
                    mailListItem={mailListItem}
                    setMailList={setMailList}
                  />
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Index;
