import React, {useContext} from 'react';
import {CurrentUserContext} from '../../../../../../../providers/CurrentUserContextProvider';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import {Carousel} from 'react-responsive-carousel';
import {Grid, Paper} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {convertMailTemplate} from '../../../../utils/string';

const useStyles = makeStyles((theme) => ({
  carousel: {
    width: '100%',
    minWidth: '1050px',
    height: '100%',
    minHeight: '400px',
  },
  paper: {
    backgroundColor: 'white',
    whiteSpace: 'pre-line',
    textAlign: 'left',
  },
  mailChip: {
    backgroundColor: '#e0e0e0',
    borderRadius: '20px',
    margin: '10px',
    padding: '10px',
  },
}));

const PreviewMail = ({
  mailList,
  setMailList,
  mailTitleList,
  setMailTitleList,
  mailTemplate,
  setMailTemplate,
}) => {
  const {currentUser} = useContext(CurrentUserContext);
  const classes = useStyles();

  return (
    <Carousel className={classes.carousel}>
      {mailList.map((mailListItem, index) => {
        var {mails} = mailListItem;

        var convertedMail = convertMailTemplate(
          mailTemplate,
          mailListItem,
          currentUser
        );

        return (
          <div>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              className={classes.paper}
              spacing={5}
            >
              <Grid item>
                <h1>Title: {mailTitleList[index]}</h1>
              </Grid>
              <Grid item>
                {mails.map((mail, index) => {
                  return (
                    <span key={index} className={classes.mailChip}>
                      {mail}
                    </span>
                  );
                })}
              </Grid>
              <Grid item>
                <Paper className={classes.paper}>{convertedMail}</Paper>
              </Grid>
            </Grid>
          </div>
        );
      })}
    </Carousel>
  );
};

export default PreviewMail;
