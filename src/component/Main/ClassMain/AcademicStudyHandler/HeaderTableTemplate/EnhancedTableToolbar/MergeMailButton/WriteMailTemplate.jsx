import React from "react";
import TextInput from "react-autocomplete-input";
import "react-autocomplete-input/dist/bundle.css";

// mailTemplate: String
// setMailTemplate: Function, no params
const WriteMailTemplate = ({ mailTemplate, setMailTemplate }) => {
  return (
    <TextInput
      style={{
        width: "100%",
        minWidth: "1050px",
        height: "100%",
        minHeight: "300px",
        padding: "0",
        margin: "20px 0",
        outline: "none",
        border: "0",
        fontFamily: "medium-content-sans-serif-font",
        fontStyle: "normal",
        fontSize: "larger",
      }}
      options={[
        "dad-fullname",
        "mom-fullname",
        "consultant-fullname",
        "my-name",
        "student-fullname",
        "classname",
        "student-code",
        "semester",
        "school-year",
        "reasons",
      ]}
      placeholder="What is in your mind ..."
      value={mailTemplate}
      onChange={(value) => {
        setMailTemplate(value);
      }}
    />
  );
};

export default WriteMailTemplate;
