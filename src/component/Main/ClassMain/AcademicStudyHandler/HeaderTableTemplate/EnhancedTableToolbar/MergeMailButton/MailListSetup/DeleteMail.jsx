import React from 'react';
import {IconButton} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';

const DeleteMail = ({mailList, mailListItem, setMailList}) => {
  const removeMail = () => {
    return setMailList(
      mailList.filter((mail) => {
        return mail._id !== mailListItem._id;
      })
    );
  };

  return (
    <IconButton onClick={removeMail}>
      <CancelIcon fontSize="small" />
    </IconButton>
  );
};

export default DeleteMail;
