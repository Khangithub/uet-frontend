import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";

import {
  Toolbar,
  IconButton,
  Tooltip,
  Typography,
  Grid,
} from "@material-ui/core";
import { lighten, makeStyles } from "@material-ui/core/styles";

import MergeMailButton from "./MergeMailButton/";
import CancelButton from "./CancelButton";

import FilterListIcon from "@material-ui/icons/FilterList";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: "1 1 100%",
  },
}));

const Index = ({ title, selected, setSelected }) => {
  const classes = useStyles();

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: selected.length > 0,
      })}
    >
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item>
          {selected.length > 0 ? (
            <Typography
              className={classes.title}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {selected.length} selected
            </Typography>
          ) : (
            <Typography variant="caption" display="block" gutterBottom>
              {title}
            </Typography>
          )}
        </Grid>
        <Grid item>
          {selected.length > 0 ? (
            <>
              <MergeMailButton selected={selected} setSelected={setSelected} />
              <CancelButton setSelected={setSelected} />
            </>
          ) : (
            <Tooltip title="Filter list">
              <IconButton aria-label="filter list">
                <FilterListIcon />
              </IconButton>
            </Tooltip>
          )}
        </Grid>
      </Grid>
    </Toolbar>
  );
};

Index.propTypes = {
  selected: PropTypes.array.isRequired,
  setSelected: PropTypes.func.isRequired,
};

export default Index;
