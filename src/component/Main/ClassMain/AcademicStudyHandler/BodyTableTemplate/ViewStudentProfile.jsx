import React from "react";
import { Grid, Avatar } from "@material-ui/core";
import {withRouter} from "react-router-dom"

const ViewStudentProfile = ({ student, history, location, match }) => {
  const { profileImage, fullname, code } = student;

  return (
    <Grid container direction="column" onClick={() => history.push(`/student/${code}`)}>
      <Grid item>
        <Avatar src={profileImage} />
      </Grid>
      <Grid item>{fullname}</Grid>
    </Grid>
  );
};

export default withRouter(ViewStudentProfile);
