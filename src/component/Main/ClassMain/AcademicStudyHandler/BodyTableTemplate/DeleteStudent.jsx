import React, {useState, useContext} from 'react';
import {CircularProgress, IconButton} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import LoadingModal from '../../../../Common/LoadingModal';
import ResponseModal from '../../../../Common/ResponseModal';
import {StudentListContext} from '../../../../../providers/StudentListContextProvider';
import {ClassListContext} from '../../../../../providers/ClassListContextProvider';
import {CurrentUserContext} from '../../../../../providers/CurrentUserContextProvider';
import Cookies from 'universal-cookie';

const DeleteStudent = ({student}) => {
  const cookies = new Cookies();
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const token = cookies.get('token');
  const [open, setOpen] = useState(false);
  var {
    setStudentList,
    getStudentListLoading,
    setStudentListLoading,
  } = useContext(StudentListContext);
  var {setClassList} = useContext(ClassListContext);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  const handleDelete = () => {
    setStudentListLoading(true);
    fetch(
      `https://uet-backend.herokuapp.com/students/${student.classname}/${student.code}`,
      {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer '.concat(token),
          'Content-type': 'application/json',
        },
      }
    )
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var studentListResponse = await fetch(
            `https://uet-backend.herokuapp.com/students/from/class/${student.classname}`
          );
          var studentListJson = await studentListResponse.json();

          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
          var classListJson = await classListResponse.json();

          if (studentListJson.docs.length && classListJson.docs.length) {
            setStudentList(studentListJson.docs);
            setClassList(classListJson.docs);
            setStudentListLoading(false);
            setResponseJSON(json);
            setOpen(true);
          }
        } catch (err) {
          console.error('DeleteStudent', err);
          setStudentListLoading(false);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <>
      <IconButton
        onClick={handleDelete}
        style={{display: currentUser.isManager ? 'flex' : 'none'}}
      >
        <CancelIcon fontSize="small" />
      </IconButton>
      <LoadingModal
        loading={getStudentListLoading}
        setLoading={setStudentListLoading}
      />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default DeleteStudent;
