import React, {useState, forwardRef} from 'react';
import ChipInput from 'material-ui-chip-input';

import ResponseModal from '../../../../Common/ResponseModal';
import LoadingModal from '../../../../Common/LoadingModal';

import {
  IconButton,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Slide,
  Grid,
  TextField,
  InputBase,
} from '@material-ui/core';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import MailIcon from '@material-ui/icons/Mail';
import MinimizeIcon from '@material-ui/icons/Minimize';
import MaximizeIcon from '@material-ui/icons/Maximize';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import AttachFileIcon from '@material-ui/icons/AttachFile';

import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiDialog-scrollPaper': {
      display: ' flex',
      [theme.breakpoints.down('sm')]: {
        alignItems: 'center',
        justifyContent: 'center',
      },
      alignItems: 'flex-end',
      justifyContent: 'flex-start',
    },
  },
  mailHeader: {
    padding: '10px 0px',
  },
  content: {padding: '0px 10px'},
  invisbleInput: {
    display: 'none',
  },
}));

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const SendMailButton = ({vnumail}) => {
  const classes = useStyles();
  const [openMail, setOpenMail] = useState(false);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const [selectedFilename, setSelectedFilename] = useState('');
  const [minimize, setMinimize] = useState(false);

  const [mail, setMail] = useState({
    subjectTo: '',
    mailList: [vnumail],
    content: '',
    attach: '',
  });

  const handleClickOpen = () => {
    setOpenMail(true);
  };

  const handleClose = () => {
    setMail({
      ...mail,
      subjectTo: '',
      mailList: [vnumail],
      content: '',
      attach: '',
    });
    setSelectedFilename('');
    setOpenMail(false);
  };

  const handleSendMail = (event) => {
    event.preventDefault();
    setLoading(true);
    var {subjectTo, mailList, content, attach} = mail;
    console.log(mail, "mail")
    var formData = new FormData();
    formData.append('subjectTo', subjectTo);
    formData.append('mailList', mailList);
    formData.append('content', content);
    formData.append('attach', attach);

    fetch('https://uet-backend.herokuapp.com/users/mail', {
      method: 'POST',
      headers: {
        Accept: 'multipart/form-data',
      },
      body: formData,
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then((json) => {
        setOpen(true);
        setLoading(false);
        setResponseJSON(json);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };
  return (
    <>
      <Tippy content="Mail" placement="bottom">
        <IconButton aria-label="mail" onClick={handleClickOpen}>
          <MailIcon />
        </IconButton>
      </Tippy>
      <Dialog
        open={openMail}
        className={classes.root}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid
              item
              style={{
                whiteSpace: 'nowrap',
                width: '450px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
              }}
            >
              <strong>New Mail</strong>
            </Grid>
            <Grid item>
              <IconButton
                onClick={() => {
                  setMinimize(!minimize);
                }}
              >
                {minimize ? <MaximizeIcon /> : <MinimizeIcon />}
              </IconButton>
              <IconButton onClick={handleClose}>
                <CloseIcon />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent style={minimize ? {display: 'none'} : {}}>
          <DialogContentText>
            <form enctype="multipart/form-data">
              <TextField
                className={classes.mailHeader}
                value={mail.subjectTo}
                label="Subject To: "
                fullWidth={true}
                onChange={(event) => {
                  setMail({...mail, subjectTo: event.target.value});
                }}
                defaultValue={mail.subjectTo.toUpperCase()}
              />
              <ChipInput
                defaultValue={mail.mailList}
                fullWidth={true}
                label="To: "
                fullWidthInput={true}
                onChange={(event) =>
                  setMail({
                    ...mail,
                    mailList: event,
                  })
                }
              />
            </form>
          </DialogContentText>
        </DialogContent>
        <DialogActions
          className={classes.MuiInputBaseRoot}
          style={minimize ? {display: 'none'} : {}}
        >
          <Grid container direction="column" justify="space-between">
            <Grid item>
              <InputBase
                placeholder="What is on your mind"
                fullWidth={true}
                rows={10}
                maxRows={1000}
                className={classes.content}
                multiline={true}
                value={mail.content}
                onChange={(event) => {
                  setMail({...mail, content: event.target.value});
                }}
              />
            </Grid>
            <Grid item>
              <input
                className={classes.invisbleInput}
                id="icon-button-file"
                type="file"
                onChange={(event) => {
                  console.log('adding');
                  setMail({...mail, attach: event.target.files[0]});
                  setSelectedFilename(event.target.files[0].name);
                }}
              />

              <label htmlFor="icon-button-file">
                <IconButton
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                >
                  <AttachFileIcon fontSize="large" /> {selectedFilename}
                </IconButton>
              </label>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={handleSendMail}
              >
                Send &nbsp; <SendIcon />
              </Button>
            </Grid>
          </Grid>
          <LoadingModal loading={loading} setLoading={setLoading} />
          <ResponseModal
            open={open}
            setOpen={setOpen}
            responseJSON={responseJSON}
            isError={isError}
          />
        </DialogActions>
      </Dialog>
    </>
  );
};

export default SendMailButton;
