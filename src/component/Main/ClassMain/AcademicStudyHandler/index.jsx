import React from 'react';
import DropedOutStudentList from './DropedOutStudentList';
import WarnedStudentList from "./WarnedStudentList";
import NotInteractedStudentList from "./NotInteractedStudentList";
import PromptedStudentList from "./PromptedStudentList";

const Index = () => {
	return (
		<>
			<DropedOutStudentList />
			<WarnedStudentList />
			<NotInteractedStudentList />
			<PromptedStudentList />
		</>
	);
};

export default Index;
