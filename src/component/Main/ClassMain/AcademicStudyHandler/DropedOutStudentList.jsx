import React, {useState, useContext} from 'react';

import {getLatestGPAFlattenScoreList} from '../utils/array';
import {StudentListContext} from '../../../../providers/StudentListContextProvider';
import {getComparator, stableSort} from '../utils/number';
import ViewStudentProfile from './BodyTableTemplate/ViewStudentProfile';

import EnhancedTableHead from './HeaderTableTemplate/EnhancedTableHead';
import EnhancedTableToolbar from './HeaderTableTemplate/EnhancedTableToolbar';
import SendMailButton from './BodyTableTemplate/SendMailButton';
import DeleteStudent from './BodyTableTemplate/DeleteStudent';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import 'tippy.js/animations/perspective.css';

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  Paper,
  Checkbox,
  FormControlLabel,
  Switch,
  Grid,
  ClickAwayListener,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const headCells = [
  {
    id: 'englishName',

    disablePadding: true,
    label: <h5>Fullname</h5>,
  },
  {id: 'code', disablePadding: false, label: <h5>Student Code</h5>},
  {
    id: 'schoolYear',

    disablePadding: false,
    label: <h5>School Year</h5>,
  },
  {
    id: 'semester',
    disablePadding: false,
    label: <h5>Semester</h5>,
  },
  {id: 'reason', disablePadding: false, label: <h5>Warning Reasons</h5>},
  {id: '', disablePadding: false, label: <h5>Actions</h5>},
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  MuiTableCellRoot: {
    '& .MuiTableCell-root': {
      border: '0',
    },
  },
}));

const DropedOutStudentList = () => {
  const classes = useStyles();
  const {studentList} = useContext(StudentListContext);
  var DropedOutList = getLatestGPAFlattenScoreList(studentList).filter(
    (student) => {
      return student.reason.length > 2;
    }
  );
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('calories');
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [dense, setDense] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = DropedOutList.map((n) => n._id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, _id) => {
    const selectedIndex = selected.indexOf(_id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, _id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (_id) => selected.indexOf(_id) !== -1;

  const emptyRows =
    rowsPerPage -
    Math.min(rowsPerPage, DropedOutList.length - page * rowsPerPage);

  const handleClickAway = () => {
    return setSelected([]);
  };
  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <Accordion defaultExpanded={DropedOutList.length > 0 ? true : false}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="overline" display="block" gutterBottom>
            list of students subject to drop out of school
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className={classes.root}>
            <Paper className={classes.paper}>
              <EnhancedTableToolbar
                title="list of students with more than 3 times the study warning"
                selected={selected}
                setSelected={setSelected}
              />
              <TableContainer>
                <Table
                  className={classes.table}
                  aria-labelledby="tableTitle"
                  size={dense ? 'small' : 'medium'}
                  aria-label="enhanced table"
                >
                  <EnhancedTableHead
                    classes={classes}
                    numSelected={selected.length}
                    order={order}
                    orderBy={orderBy}
                    onSelectAllClick={handleSelectAllClick}
                    onRequestSort={handleRequestSort}
                    rowCount={DropedOutList.length}
                    headCells={headCells}
                  />
                  <TableBody>
                    {stableSort(DropedOutList, getComparator(order, orderBy))
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((student, index) => {
                        var {
                          _id,
                          englishName,
                          schoolYear,
                          semester,
                          code,
                          reason,
                          vnumail,
                        } = student;
                        const isItemSelected = isSelected(_id);
                        const labelId = `enhanced-table-checkbox-${index}`;

                        return (
                          <TableRow
                            hover
                            onClick={(event) => handleClick(event, _id)}
                            role="checkbox"
                            aria-checked={isItemSelected}
                            tabIndex={-1}
                            key={_id}
                            selected={isItemSelected}
                            className={classes.MuiTableCellRoot}
                          >
                            <TableCell padding="checkbox">
                              <Checkbox
                                checked={isItemSelected}
                                inputProps={{'aria-labelledby': labelId}}
                              />
                            </TableCell>
                            <TableCell
                              component="th"
                              id={labelId}
                              scope="student"
                              padding="none"
                            >
                              <TableCell align="left">
                                <Tippy
                                  theme="light"
                                  interactive={true}
                                  animation="perspective"
                                  placement={'right'}
                                  content={
                                    <ViewStudentProfile student={student} />
                                  }
                                >
                                  <TableCell align="left">
                                    {englishName}
                                  </TableCell>
                                </Tippy>
                              </TableCell>
                            </TableCell>
                            <TableCell align="left">{code}</TableCell>
                            <TableCell align="left">{schoolYear}</TableCell>
                            <TableCell align="left">{semester}</TableCell>
                            <TableCell align="left">
                              {reason.map((item, index) => {
                                return (
                                  <div key={index}>
                                    <small>{item}</small>
                                    <hr
                                      style={{
                                        display:
                                          index === reason.length - 1
                                            ? 'none'
                                            : 'flex',
                                      }}
                                    />
                                  </div>
                                );
                              })}
                            </TableCell>
                            <TableCell align="left">
                              <Grid container direction="row">
                                <Grid item>
                                  <SendMailButton vnumail={vnumail} />
                                </Grid>
                                <Grid item>
                                  <DeleteStudent student={student} />
                                </Grid>
                              </Grid>
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    {emptyRows > 0 && (
                      <TableRow style={{height: (dense ? 33 : 53) * emptyRows}}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[
                  5,
                  10,
                  25,
                  50,
                  100,
                  200,
                  DropedOutList.length,
                ].sort((a, b) => {
                  return a - b;
                })}
                component="div"
                count={DropedOutList.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Paper>
            <FormControlLabel
              control={<Switch checked={dense} onChange={handleChangeDense} />}
              label="Dense padding"
            />
          </div>
        </AccordionDetails>
      </Accordion>
    </ClickAwayListener>
  );
};

export default DropedOutStudentList;
