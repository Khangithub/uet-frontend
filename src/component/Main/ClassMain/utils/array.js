exports.convertHeadCellIdToText = (id) => {
  return (
    (id === 'englishName' && 'Fullname') ||
    (id === 'code' && 'Student Code') ||
    (id === 'schoolYear' && 'School Year') ||
    (id === 'semester' && 'Semester') ||
    (id === 'score' && 'Trainning Score') ||
    (id === 'classification' && 'Trainning Classification') ||
    (id === 'gpa' && 'GPA') ||
    (id === 'gpaClassification' && 'GPA Classification') ||
    (id === 'fCredits' && 'Shorten Credits')
  );
};
// ClassMain/AcademicTrainingScore/ScoreChart.jsx
const formatArray = (oldArray) => {
  const newArray = [];
  oldArray.forEach((oldEntry) => {
    let newObjIndex = newArray.findIndex(
      (e) => e.schoolYear === oldEntry.schoolYear
    );
    if (newObjIndex === -1) {
      newArray.push({
        schoolYear: oldEntry.schoolYear,
      });
      newObjIndex = newArray.length - 1;
    }
    if (!newArray[newObjIndex].semester) {
      newArray[newObjIndex].semester = [];
    }
    if (!newArray[newObjIndex].semester[oldEntry.semester]) {
      newArray[newObjIndex].semester[oldEntry.semester] = {};
    }
    newArray[newObjIndex].semester[oldEntry.semester][
      oldEntry.classification
    ] = newArray[newObjIndex].semester[oldEntry.semester][
      oldEntry.classification
    ]
      ? newArray[newObjIndex].semester[oldEntry.semester][
          oldEntry.classification
        ] + 1
      : 1;
  });
  return newArray;
};

exports.scoreStatistics = (studentList) => {
  var list = studentList.map((student) => {
    var {scoreList} = student;
    return scoreList;
  });

  var flatten = [].concat.apply([], list).map((student) => {
    var {semester, classification, schoolYear} = student;
    return {semester, classification, schoolYear};
  });

  var formated = formatArray(flatten);
  return formated;
};

// ClassMain/AcademicTrainningScore/StudentListTable.jsx
const convertFullnametoEngLish = (fullname) => {
  if (fullname === null || fullname === undefined) return fullname;
  fullname = fullname.toLowerCase();
  fullname = fullname.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  fullname = fullname.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  fullname = fullname.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  fullname = fullname.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  fullname = fullname.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  fullname = fullname.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  fullname = fullname.replace(/đ/g, 'd');

  var order =
    fullname.split(' ')[fullname.split(' ').length - 2] +
    ' , ' +
    fullname
      .split(' ')
      .slice(0, fullname.split(' ').length - 2)
      .join(' ');
  return order.replace(/\b\w/g, (l) => l.toUpperCase());
};

// ClassMain/AcademicStudyHandler/DropedOutStudentList.jsx
exports.getAllGPAFlattenScoreList = (studentList) => {
  var list = studentList.map((student) => {
    var {
      _id,
      fullname,
      code,
      classname,
      gender,
      birthday,
      profileImage,
      scoreList,
      fCredits,
      warnings,
      vnumail,
      parentsMail,
      consultant,
    } = student;

    scoreList.forEach((item, index) => {
      const {semester, schoolYear} = item;
      var {credits} = fCredits.filter((item, index) => {
        return item.schoolYear === schoolYear && item.semester === semester;
      })[0];

      var {reason} = warnings.filter((item, index) => {
        return item.schoolYear === schoolYear && item.semester === semester;
      })[0];

      var {
        dadAvatar,
        dadFullname,
        dadEmail,
        momAvatar,
        momFullname,
        momEmail,
      } = parentsMail;

      item._id = _id + '-' + code + '-' + schoolYear + '-' + semester;
      item.englishName = convertFullnametoEngLish(fullname);
      item.fullname = fullname;
      item.gender = gender;
      item.classname = classname;
      item.birthday = birthday;
      item.code = code;
      item.profileImage = profileImage;
      item.fCredits = credits;
      item.reason = reason;
      item.vnumail = vnumail;
      item.parentsMail = {
        dadAvatar,
        dadFullname,
        momAvatar,
        momFullname,
        parentsMailList: [dadEmail, momEmail],
      };
      item.consultant = consultant;
    });

    return scoreList;
  });

  return [].concat.apply([], list);
};

exports.getLatestGPAFlattenScoreList = (studentList) => {
  var latestSemester = studentList.map((student) => {
    var {scoreList, fCredits, warnings} = student;

    var latestGPAScore = scoreList[scoreList.length - 1];
    var latestFCredit = fCredits[fCredits.length - 1];
    var latestWarning = warnings[warnings.length - 1];

    student.scoreList = [latestGPAScore];
    student.fCredits = [latestFCredit];
    student.warnings = [latestWarning];

    return student;
  });

  var list = latestSemester.map((student) => {
    var {
      _id,
      fullname,
      code,
      classname,
      gender,
      birthday,
      profileImage,
      scoreList,
      fCredits,
      warnings,
      vnumail,
      parentsMail,
      consultant,
    } = student;

    scoreList.forEach((item, index) => {
      const {semester, schoolYear} = item;
      var {credits} = fCredits.filter((item, index) => {
        return item.schoolYear === schoolYear && item.semester === semester;
      })[0];

      var {reason} = warnings.filter((item, index) => {
        return item.schoolYear === schoolYear && item.semester === semester;
      })[0];

      var {
        dadAvatar,
        dadFullname,
        dadEmail,
        momAvatar,
        momFullname,
        momEmail,
      } = parentsMail;

      item._id = _id + '-' + code + '-' + schoolYear + '-' + semester;
      item.englishName = convertFullnametoEngLish(fullname);
      item.fullname = fullname;
      item.gender = gender;
      item.classname = classname;
      item.birthday = birthday;
      item.code = code;
      item.profileImage = profileImage;
      item.fCredits = credits;
      item.reason = reason;
      item.vnumail = vnumail;
      item.parentsMail = {
        dadAvatar,
        dadFullname,
        momAvatar,
        momFullname,
        parentsMailList: [dadEmail, momEmail],
      };
      item.consultant = consultant;
    });

    return scoreList;
  });

  return [].concat.apply([], list);
};

// AcademicTraingScore/ScoreChart
exports.gpaStatistics = (studentList) => {
  var gpaList = studentList.map((student) => {
    var {scoreList} = student;
    return scoreList;
  });

  var flatten = [].concat.apply([], gpaList).map((student) => {
    var {semester, classification, schoolYear} = student;
    return {semester, classification, schoolYear};
  });

  var formated = formatArray(flatten);
  return formated;
};

// FacultyMail/WarnedStudentStatisticChart
exports.groupClassesSameFaculty = (classList) => {
  const input = classList.map((classItem) => {
    const {faculty, classSize, warnedLength} = classItem;
    return {faculty, classSize, warnedLength};
  });

  const groupBy = input.reduce((acc, cur) => {
    if (acc[cur.faculty['_id']]) {
      acc[cur.faculty['_id']].totalClassSize += cur.classSize;
      acc[cur.faculty['_id']].totalWarnedLength += cur.warnedLength;
    } else {
      acc[cur.faculty['_id']] = {
        faculty: cur.faculty,
        totalClassSize: cur.classSize,
        totalWarnedLength: cur.warnedLength,
      };
    }
    return acc;
  }, {});
  const output = Object.values(groupBy);
  return output;
};
