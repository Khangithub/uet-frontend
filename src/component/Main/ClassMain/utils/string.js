//MergeMailButton/PreviewMail.js

exports.convertMailTemplate = (template, studentData, currentUser) => {
  var {
    dadFullname,
    momFullname,
    consultant,
    fullname,
    classname,
    code,
    semester,
    schoolYear,
    reason,
  } = studentData;

  return template
    .replace('@dad-fullname', dadFullname)
    .replace('@mom-fullname', momFullname)
    .replace('@consultant-fullname', consultant.fullname)
    .replace('@student-fullname', fullname)
    .replace('@classname', classname)
    .replace('@student-code', code)
    .replace('@semester', semester)
    .replace('@school-year', schoolYear)
    .replace('@my-name', currentUser.fullname)
    .replace(
      '@reasons',
      reason
        .map((reasonItem) => {
          return '- ' + reasonItem;
        })
        .join(', \n')
    );
};
