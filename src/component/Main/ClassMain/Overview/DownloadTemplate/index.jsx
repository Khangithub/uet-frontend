import React from 'react';
import Cookies from 'universal-cookie';
import { Grid, IconButton, Typography } from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';

const Index = ({ selectedClass }) => {
	const cookies = new Cookies();
	const token = cookies.get('token');

	const getTemplate = (event) => {
		event.preventDefault();

		fetch(`http://localhost:5000/classes/template/${selectedClass}`, {
			method: 'GET',
			headers: {
				Authorization: 'Bearer '.concat(token),
				'content-type': 'application/json; charset=UTF-8',
			},
		})
			.then((response) => {
				return response.json();
			})
			.then((json) => {
				console.log(json);
				var a = document.createElement('a');
				a.href = json.downloadURL;
				a.style.display = 'none';
				a.download = 'download';
				a.click();
			})
			.catch((error) => {
				console.error(error);
			});
	};

	return (
		<Tippy
			content={
				<Grid container justify="center" alignItems="center">
					<Grid item>
						<Typography align="center">Download template</Typography>
					</Grid>
					<Grid item>
						<img
							src="https://firebasestorage.googleapis.com/v0/b/fir-fb-auth.appspot.com/o/excel.png?alt=media&token=a5150bd3-76da-487c-9ac6-10f6ef13766c"
							alt="xlsx template"
							style={{ height: '120px', width: '240px' }}
						/>
					</Grid>
				</Grid>
			}
			placement="bottom"
		>
			<IconButton>
				<GetAppIcon color="action" fontSize="large" onClick={getTemplate} />
			</IconButton>
		</Tippy>
	);
};

export default Index;
