import React, { useState } from 'react';

import PublishIcon from '@material-ui/icons/Publish';
import { makeStyles } from '@material-ui/core/styles';
import { Backdrop, Fade, IconButton, Modal } from '@material-ui/core';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import ModalContent from './ModalContent/';

const useStyles = makeStyles((theme) => ({
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: '0px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
}));

// selectedClass: String
const Index = ({ selectedClass }) => {
	const classes = useStyles();
	const [open, setOpen] = useState(false);
	return (
		<>
			<Tippy content="Upload xlsx" placement="bottom">
				<IconButton>
					<PublishIcon
						color="action"
						fontSize="large"
						onClick={() => {
							setOpen(true);
						}}
					/>
				</IconButton>
			</Tippy>
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				className={classes.modal}
				open={open}
				onClose={() => {
					setOpen(false);
				}}
				closeAfterTransition
				BackdropComponent={Backdrop}
				BackdropProps={{
					timeout: 500,
				}}
			>
				<Fade in={open}>
					<div className={classes.paper}>
						<ModalContent selectedClass={selectedClass} />
					</div>
				</Fade>
			</Modal>
		</>
	);
};

export default Index;
