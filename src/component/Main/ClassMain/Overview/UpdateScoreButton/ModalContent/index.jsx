import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, TextField, FormControl, IconButton, InputLabel, MenuItem, Select } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

import AttachFileIcon from '@material-ui/icons/AttachFile';

const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	input: {
		display: 'none',
	},
}));

// selectedClass: String
const Index = ({ selectedClass, history, location, match }) => {
	const classes = useStyles();

	const [course, setCourse] = useState({
		schoolYear: '',
		semester: '',
		excel: '',
	});

	const [selectedExcel, setSelectedExcel] = useState('');

	const handleSubmit = (event) => {
		event.preventDefault();
		var { schoolYear, semester, excel } = course;
		var formData = new FormData();
		formData.append('schoolYear', schoolYear);
		formData.append('semester', semester);
		formData.append('excel', excel);
		formData.append('classname', selectedClass);

		fetch(`http://localhost:5000/classes/${selectedClass}/update`, {
			method: 'POST',
			headers: {
				Accept: 'multipart/form-data',
			},
			body: formData,
		})
			.then((response) => {
				return response.json();
			})
			.then((json) => {
				console.log(json);
			})
			.catch((error) => {
				console.error(error);
				alert(JSON.stringify(error));
			});
	};
	return (
		<div>
			<form enctype="multipart/form-data">
				<img src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png" alt="" style={{ height: '65px' }} />
				<TextField
					type="text"
					label="SchoolYear"
					name="schoolYear"
					fullWidth
					onChange={(event) => setCourse({ ...course, schoolYear: event.target.value })}
				/>
				<br />
				<FormControl style={{ minWidth: 400 }}>
					<InputLabel id="semester-select">Semester</InputLabel>
					<Select
						labelId="semester-select"
						onChange={(event) => setCourse({ ...course, semester: event.target.value })}
					>
						<MenuItem value={1}>1</MenuItem>
						<MenuItem value={2}>2</MenuItem>
					</Select>
				</FormControl>
				<br />
				<input
					className={classes.input}
					id="icon-button-file"
					type="file"
					accept=".xlsx"
					onChange={(event) => {
						setCourse({ ...course, excel: event.target.files[0] });
						setSelectedExcel(event.target.files[0].name);
					}}
				/>

				<label htmlFor="icon-button-file">
					<IconButton color="primary" aria-label="upload picture" component="span">
						<AttachFileIcon fontSize="large" /> {selectedExcel}
					</IconButton>
				</label>
				<br />

				<Button color="primary" variant="contained" fullWidth type="submit" onClick={handleSubmit}>
					Add Class
				</Button>
			</form>
		</div>
	);
};

export default withRouter(Index);
