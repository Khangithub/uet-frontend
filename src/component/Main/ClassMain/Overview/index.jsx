import React, { useState, useContext } from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
	Card,
	CardActionArea,
	CardMedia,
	CardContent,
	Grid,
	Typography,
	Select,
	MenuItem,
	FormControl,
	FormHelperText,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Avatar,
} from '@material-ui/core';
import { StudentListContext } from '../../../../providers/StudentListContextProvider';
import Pagination from './Pagination';

import { formatClassname, toCapitalize } from '../../../../utils/string';

const useStyles = makeStyles((theme) => ({
	container: {
		width: '100%',
	},
	cardContainer: {
		maxWidth: 180,
	},
	listContainer: {
		width: '50%',
	},
}));

const Index = ({ history, match, location }) => {
	const { studentList } = useContext(StudentListContext);
	var { classname } = match.params;
	const classes = useStyles();
	const [viewMode, setViewMode] = useState('list');
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);

	const handleChange = () => {
		viewMode === 'list' ? setViewMode('card') : setViewMode('list');
	};

	return (
		<div className={classes.container}>
			<Grid container direction="row" justify="space-between" alignItems="center">
				<Grid item>
					<Typography gutterBottom align="left" display="block" variant="overline">
						Student List Of Class {formatClassname(classname)}
					</Typography>
				</Grid>

				<Grid item>
					<FormControl className={classes.formControl}>
						<Select value={viewMode} onChange={handleChange}>
							<MenuItem value="card">Card</MenuItem>
							<MenuItem value="list">List</MenuItem>
						</Select>
						<FormHelperText>View Mode: {toCapitalize(viewMode)}</FormHelperText>
					</FormControl>
				</Grid>
				<Grid item>
					<Pagination
						page={page}
						setPage={setPage}
						rowsPerPage={rowsPerPage}
						setRowsPerPage={setRowsPerPage}
						count={studentList.length}
					/>
				</Grid>
			</Grid>

			<Grid container spacing={1} justify="center">
				{studentList.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((student, index) => {
					const { profileImage, fullname, code } = student;
					return (
						<>
							{viewMode === 'card' ? (
								<Grid
									item
									key={index}
									onClick={() =>
										history.push({
											pathname: `/student/${code}`,
										})
									}
								>
									<Card className={classes.cardContainer}>
										<CardActionArea>
											<CardMedia
												component="img"
												alt="profileImage"
												height="100"
												image={profileImage}
												title="profileImage"
											/>
											<CardContent>
												<Typography gutterBottom component="h5">
													{fullname}
												</Typography>
												<Typography variant="body2" color="textSecondary" component="p">
													{code}
												</Typography>
											</CardContent>
										</CardActionArea>
									</Card>
								</Grid>
							) : (
								<List className={classes.listContainer}>
									<ListItem
										onClick={() =>
											history.push({
												pathname: `/student/${code}`,
												state: { student: student },
											})
										}
									>
										<ListItemAvatar>
											<Avatar src={profileImage} sizes="small" alt="profileImage" />
										</ListItemAvatar>
										<ListItemText primary={fullname} secondary={code} />
									</ListItem>
								</List>
							)}
						</>
					);
				})}
			</Grid>
		</div>
	);
};

export default withRouter(Index);
