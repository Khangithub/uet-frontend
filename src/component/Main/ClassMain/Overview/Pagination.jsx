import React from 'react';
import TablePagination from '@material-ui/core/TablePagination';

// page: Number
// setPage: Function
// rowsPerPage: Number
// setRowsPerPage: Function
// count: Number
const Pagination = ({ page, setPage, rowsPerPage, setRowsPerPage, count }) => {
	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	return (
		<TablePagination
			component="div"
			count={count}
			page={page}
			onChangePage={handleChangePage}
			rowsPerPage={rowsPerPage}
			onChangeRowsPerPage={handleChangeRowsPerPage}
		/>
	);
};

export default Pagination;
