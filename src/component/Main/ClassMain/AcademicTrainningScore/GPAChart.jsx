import React, {useState, useContext} from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import {Carousel} from 'react-responsive-carousel';
import {FormControlLabel, Grid, Switch, Typography} from '@material-ui/core';
import {gpaStatistics} from '../utils/array';
import RandomColor from 'randomcolor';
import {StudentListContext} from '../../../../providers/StudentListContextProvider';

import Chart from 'react-google-charts';

const GPAChart = () => {
  const {studentList} = useContext(StudentListContext);
  const [visible, setVisile] = useState(false);
  var gpaList = gpaStatistics(studentList);

  return (
    <div>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item>
          <Typography
            gutterBottom
            align="left"
            display="block"
            variant="overline"
          >
            GPA statistics chart
          </Typography>
        </Grid>
        <Grid item>
          <FormControlLabel
            control={
              <Switch
                checked={visible}
                color="primary"
                onChange={() => {
                  setVisile(!visible);
                }}
              />
            }
            label={visible ? 'Close Chart' : 'Open Chart'}
          />
        </Grid>
      </Grid>

      <Carousel>
        {gpaList.map((item) => {
          var {schoolYear, semester} = item;
          return (
            <div style={visible ? {} : {display: 'none'}}>
              <div>
                {semester.map((item, index) => {
                  var {
                    Aplus,
                    Ascore,
                    Bplus,
                    Bscore,
                    Cscore,
                    Dscore,
                    Fscore,
                  } = item;
                  var data = [
                    ['Classname', `Semester #${index}`, {role: 'style'}],
                  ].concat(
                    [['A+', Aplus ? Aplus : 0, RandomColor()]],
                    [['A', Ascore ? Ascore : 0, RandomColor()]],
                    [['B+', Bplus ? Bplus : 0, RandomColor()]],
                    [['B', Bscore ? Bscore : 0, RandomColor()]],
                    [['C', Cscore ? Cscore : 0, RandomColor()]],
                    [['D', Dscore ? Dscore : 0, RandomColor()]],
                    [['F', Fscore ? Fscore : 0, RandomColor()]]
                  );

                  return (
                    <div>
                      <Chart
                        chartType="ColumnChart"
                        width="100%"
                        height="400px"
                        data={data}
                      />
                    </div>
                  );
                })}
              </div>
              <p className="legend">School Year: {schoolYear}</p>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
};

export default GPAChart;
