import React from 'react';
import StudentListTable from './StudentListTable/';
import GPAChart from './GPAChart';

const Index = () => {
	return (
		<>
			<GPAChart />
			<StudentListTable />
		</>
	);
};

export default Index;
