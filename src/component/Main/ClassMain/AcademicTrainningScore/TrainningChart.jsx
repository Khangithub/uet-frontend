import React, { useState } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import { FormControlLabel, Grid, Switch, Typography } from '@material-ui/core';
import { scoreStatistics } from './utils/array';
import RandomColor from 'randomcolor';

import Chart from 'react-google-charts';

// studentList: Array
const TrainningChart = ({ studentList }) => {
	const [visible, setVisile] = useState(false);
	var scoreList = scoreStatistics(studentList.studentList);
	return (
		<div>
			<Grid container direction="row" justify="space-between" alignItems="center">
				<Grid item>
					<Typography gutterBottom align="left" display="block" variant="overline">
						Training score statistics chart
					</Typography>
				</Grid>
				<Grid item>
					<FormControlLabel
						control={
							<Switch
								checked={visible}
								color="primary"
								onChange={() => {
									setVisile(!visible);
								}}
							/>
						}
						label={visible ? 'Close Chart' : 'Open Chart'}
					/>
				</Grid>
			</Grid>

			<Carousel>
				{scoreList.map((item) => {
					var { schoolYear, semester } = item;
					return (
						<div style={visible ? {} : { display: 'none' }}>
							<div>
								{semester.map((item, index) => {
									var { Excellent, Good, Intermediate, Average, Weak, Fail } = item;
									var data = [['Classname', `Semester #${index}`, { role: 'style' }]].concat(
										[['Excellent', Excellent ? Excellent : 0, RandomColor()]],
										[['Good', Good ? Good : 0, RandomColor()]],
										[['Intermediate', Intermediate ? Intermediate : 0, RandomColor()]],
										[['Average', Average ? Average : 0, RandomColor()]],
										[['Weak', Weak ? Weak : 0, RandomColor()]],
										[['Fail', Fail ? Fail : 0, RandomColor()]]
									);

									return (
										<div>
											<Chart chartType="ColumnChart" width="100%" height="400px" data={data} />
										</div>
									);
								})}
							</div>
							<p className="legend">School Year: {schoolYear}</p>
						</div>
					);
				})}
			</Carousel>
		</div>
	);
};

export default TrainningChart;
