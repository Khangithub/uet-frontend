exports.convertHeadCellIdToText = id => {
  return (
    (id === 'englishName' && 'Fullname') ||
    (id === 'code' && 'Student Code') ||
    (id === 'schoolYear' && 'School Year') ||
    (id === 'semester' && 'Semester') ||
    (id === 'score' && 'Trainning Score') ||
    (id === 'classification' && 'Trainning Classification') ||
    (id === 'gpa' && 'GPA') ||
    (id === 'gpaClassification' && 'GPA Classification') ||
    (id === 'shortenCredits' && 'Shorten Credits')
  )
}
// ClassMain/AcademicTrainingScore/ScoreChart.jsx
const formatArray = oldArray => {
  const newArray = []
  oldArray.forEach(oldEntry => {
    let newObjIndex = newArray.findIndex(
      e => e.schoolYear === oldEntry.schoolYear
    )
    if (newObjIndex === -1) {
      newArray.push({
        schoolYear: oldEntry.schoolYear
      })
      newObjIndex = newArray.length - 1
    }
    if (!newArray[newObjIndex].semester) {
      newArray[newObjIndex].semester = []
    }
    if (!newArray[newObjIndex].semester[oldEntry.semester]) {
      newArray[newObjIndex].semester[oldEntry.semester] = {}
    }
    newArray[newObjIndex].semester[oldEntry.semester][
      oldEntry.classification
    ] = newArray[newObjIndex].semester[oldEntry.semester][
      oldEntry.classification
    ]
      ? newArray[newObjIndex].semester[oldEntry.semester][
          oldEntry.classification
        ] + 1
      : 1
  })
  return newArray
}

exports.scoreStatistics = studentList => {
  var scoreList = studentList.map(student => {
    var { accademicTrainningList } = student
    return accademicTrainningList
  })

  var flatten = [].concat.apply([], scoreList).map(student => {
    var { semester, classification, schoolYear } = student
    return { semester, classification, schoolYear }
  })

  var formated = formatArray(flatten)
  return formated
}

// ClassMain/AcademicTrainningScore/StudentListTable.jsx
const convertFullnametoEngLish = fullname => {
  if (fullname === null || fullname === undefined) return fullname
  fullname = fullname.toLowerCase()
  fullname = fullname.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
  fullname = fullname.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
  fullname = fullname.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
  fullname = fullname.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
  fullname = fullname.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
  fullname = fullname.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
  fullname = fullname.replace(/đ/g, 'd')

  var order =
    fullname.split(' ')[fullname.split(' ').length - 2] +
    ' , ' +
    fullname
      .split(' ')
      .slice(0, fullname.split(' ').length - 2)
      .join(' ')
  return order.replace(/\b\w/g, l => l.toUpperCase())
}

exports.flattenScoreList = studentList => {
  var list = studentList.map(student => {
    var {
      _id,
      fullname,
      code,
      classname,
      gender,
      birthday,
      accademicTrainningList,
      profileImage,
      scoreList
    } = student
    accademicTrainningList.forEach((item, index) => {
      const { semester, schoolYear, classification } = item

      item._id = _id + '-' + schoolYear + '-' + semester + '-' + classification
      item.englishName = convertFullnametoEngLish(fullname)
      item.fullname = fullname
      item.gender = gender
      item.classname = classname
      item.birthday = birthday
      item.code = code
      item.profileImage = profileImage
      item.gpa = scoreList[index].gpa
      item.gpaClassification = scoreList[index].classification
      item.shortenCredits = scoreList[index].shortenCredits
    })
    return accademicTrainningList
  })

  return [].concat.apply([], list)
}

exports.gpaStatistics = studentList => {
  var gpaList = studentList.map(student => {
    var { scoreList } = student
    return scoreList
  })

  var flatten = [].concat.apply([], gpaList).map(student => {
    var { semester, classification, schoolYear } = student
    return { semester, classification, schoolYear }
  })

  var formated = formatArray(flatten)
  return formated
}
