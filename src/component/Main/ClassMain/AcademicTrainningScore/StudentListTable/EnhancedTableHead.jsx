import React from 'react';
import PropTypes from 'prop-types';
import { TableHead, TableSortLabel, TableRow, TableCell, Checkbox, IconButton, Grid } from '@material-ui/core';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import 'tippy.js/animations/perspective.css';

import CancelIcon from '@material-ui/icons/Cancel';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import { convertHeadCellIdToText } from '../../utils/array';

const EnhancedTableHead = ({
	classes,
	onSelectAllClick,
	order,
	orderBy,
	numSelected,
	rowCount,
	onRequestSort,
	headCells,
	invisibleList,
	setInvisibleList,
}) => {
	const createSortHandler = (property) => (event) => {
		onRequestSort(event, property);
	};

	return (
		<TableHead>
			<TableRow>
				<TableCell padding="checkbox">
					<Checkbox
						indeterminate={numSelected > 0 && numSelected < rowCount}
						checked={rowCount > 0 && numSelected === rowCount}
						onChange={onSelectAllClick}
						inputProps={{ 'aria-label': 'select all students' }}
					/>
				</TableCell>
				{headCells.map((headCell) => (
					<TableCell
						key={headCell.id}
						align="left"
						padding={headCell.disablePadding ? 'none' : 'default'}
						sortDirection={orderBy === headCell.id ? order : false}
					>
						<Grid container direction="column">
							<Grid item>
								<TableSortLabel
									active={orderBy === headCell.id}
									direction={orderBy === headCell.id ? order : 'asc'}
									onClick={createSortHandler(headCell.id)}
									style={invisibleList.includes(headCell.id) ? { display: 'none' } : {}}
								>
									{headCell.label}
									{orderBy === headCell.id ? (
										<span className={classes.visuallyHidden}>
											{order === 'desc' ? 'sorted descending' : 'sorted ascending'}
										</span>
									) : null}
								</TableSortLabel>
							</Grid>

							<Grid item>
								<IconButton
									onClick={() => {
										if (invisibleList.includes(headCell.id)) {
											const indexCell = invisibleList.indexOf(headCell.id);
											var newList = invisibleList.filter((item, index) => {
												return index !== indexCell;
											});
											return setInvisibleList(newList);
										} else {
											return setInvisibleList([...invisibleList, headCell.id]);
										}
									}}
								>
									{invisibleList.includes(headCell.id) ? (
										<Tippy
											theme={'translucent'}
											interactive={true}
											animation="perspective"
											placement={'bottom'}
											content={`open "${convertHeadCellIdToText(headCell.id)}"`}
										>
											<OpenInNewIcon fontSize="small" />
										</Tippy>
									) : (
										<Tippy
											theme={'translucent'}
											interactive={true}
											animation="perspective"
											placement={'bottom'}
											content={`close "${convertHeadCellIdToText(headCell.id)}"`}
										>
											<CancelIcon fontSize="small" />
										</Tippy>
									)}
								</IconButton>
							</Grid>
						</Grid>
					</TableCell>
				))}
			</TableRow>
		</TableHead>
	);
};

EnhancedTableHead.propTypes = {
	classes: PropTypes.object.isRequired,
	numSelected: PropTypes.number.isRequired,
	onRequestSort: PropTypes.func.isRequired,
	onSelectAllClick: PropTypes.func.isRequired,
	order: PropTypes.oneOf(['asc', 'desc']).isRequired,
	orderBy: PropTypes.string.isRequired,
	rowCount: PropTypes.number.isRequired,
};

export default EnhancedTableHead;
