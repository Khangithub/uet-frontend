import React, {useState, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Highlighter from 'react-highlight-words';
import {
  Table,
  TableBody,
  TableRow,
  TableCell,
  Checkbox,
  Paper,
  TableContainer,
  Switch,
  TablePagination,
  FormControlLabel,
  FormControl,
  Select,
  MenuItem,
  ClickAwayListener,
} from '@material-ui/core';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import 'tippy.js/animations/perspective.css';
import EnhancedTableHead from './EnhancedTableHead';
import EnhancedTableToolbar from './EnhancedTableToolbar/';
import {StudentListContext} from '../../../../../providers/StudentListContextProvider';

import {getAllGPAFlattenScoreList, scoreStatistics} from '../../utils/array';
import {getComparator, stableSort} from '../../utils/number';

import UpdateStudentInfo from './UpdateStudentInfo/';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

// studentList: Array
const Index = () => {
  const {studentList} = useContext(StudentListContext);

  const classes = useStyles();
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('classification');
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [dense, setDense] = useState(false);
  const [invisibleList, setInvisibleList] = useState([]);

  const [filter, setFilter] = useState({
    keyword: '',
    semester: 'all',
    schoolYear: 'all',
    classification: 'all',
    fCredits: 'all',
  });

  const [rowsPerPage, setRowsPerPage] = useState(5);

  var scoreList = getAllGPAFlattenScoreList(studentList)
    .filter((student) => {
      if (filter.semester === 'all') {
        return student;
      }
      return student.semester === filter.semester;
    })
    .filter((student) => {
      if (filter.schoolYear === 'all') {
        return student;
      }
      return student.schoolYear === filter.schoolYear;
    })
    .filter((student) => {
      if (filter.classification === 'all') {
        return student;
      }
      return student.classification === filter.classification;
    })
    .filter((student) => {
      if (filter.fCredits === 'all') {
        return student;
      }

      if (filter.fCredits === 24) {
        return student.fCredits > 24;
      }
      return true;
    })
    .filter((student) => {
      return filter.keyword === ''
        ? student
        : student.englishName.includes(filter.keyword) ||
            student.fullname.includes(filter.keyword);
    });

  var schoolYearList = scoreStatistics(studentList).map((item) => {
    var {schoolYear} = item;
    return schoolYear;
  });

  const headCells = [
    {
      id: 'englishName',

      disablePadding: true,
      label: <h5>Fullname</h5>,
    },
    {id: 'code', disablePadding: false, label: <h5>Student Code</h5>},
    {
      id: 'schoolYear',

      disablePadding: false,
      label: (
        <div>
          <h5>School Year</h5>
          <FormControl>
            <Select
              value={filter.schoolYear}
              onChange={(event) => {
                setFilter({...filter, schoolYear: event.target.value});
              }}
              displayEmpty
              inputProps={{'aria-label': 'Without label'}}
            >
              <MenuItem value="all">All</MenuItem>
              {schoolYearList.map((schoolYear, index) => {
                return (
                  <MenuItem key={index} value={schoolYear}>
                    {schoolYear}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </div>
      ),
    },
    {
      id: 'semester',
      numeric: true,
      disablePadding: false,
      label: (
        <div>
          <h5>Semester</h5>
          <FormControl>
            <Select
              value={filter.semester}
              onChange={(event) => {
                setFilter({...filter, semester: event.target.value});
              }}
              displayEmpty
              inputProps={{'aria-label': 'Without label'}}
            >
              <MenuItem value="all">All</MenuItem>
              <MenuItem value="1">1</MenuItem>
              <MenuItem value="2">2</MenuItem>
            </Select>
          </FormControl>
        </div>
      ),
    },
    {id: 'gpaSemester', disablePadding: false, label: <h5>GPA Semester</h5>},
    {id: 'gpa', disablePadding: false, label: <h5>Total GPA</h5>},
    {
      id: 'classification',
      disablePadding: false,
      label: (
        <div>
          <h5>GPA Classification</h5>
          <FormControl>
            <Select
              value={filter.classification}
              onChange={(event) => {
                setFilter({...filter, classification: event.target.value});
              }}
              displayEmpty
              inputProps={{'aria-label': 'Without label'}}
            >
              <MenuItem value="all">All</MenuItem>
              <MenuItem value="Fscore">F</MenuItem>
              <MenuItem value="Dscore">D</MenuItem>
              <MenuItem value="Cscore">C</MenuItem>
              <MenuItem value="Bscore">B</MenuItem>
              <MenuItem value="Bplus">B+</MenuItem>
              <MenuItem value="Ascore">A</MenuItem>
              <MenuItem value="Aplus">A+</MenuItem>
            </Select>
          </FormControl>
        </div>
      ),
    },
    {
      id: 'fCredits',
      disablePadding: false,
      label: (
        <div>
          <h5>The Number Of Credits is F</h5>
          <FormControl>
            <Select
              value={filter.fCredits}
              onChange={(event) => {
                setFilter({...filter, fCredits: event.target.value});
              }}
              displayEmpty
              inputProps={{'aria-label': 'Without label'}}
            >
              <MenuItem value="all">All</MenuItem>
              <MenuItem value={24}> &gt; 24</MenuItem>
            </Select>
          </FormControl>
        </div>
      ),
    },
    {id: 'reason', disablePadding: false, label: <h5>Warning Reasons</h5>},
  ];

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = scoreList.map((n) => n._id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, _id) => {
    const selectedIndex = selected.indexOf(_id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, _id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (_id) => selected.indexOf(_id) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, scoreList.length - page * rowsPerPage);

  const handleClickAway = () => {
    return setSelected([]);
  };

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <EnhancedTableToolbar
            selected={selected}
            setSelected={setSelected}
            scoreList={scoreList}
            filter={filter}
            setFilter={setFilter}
          />
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={dense ? 'small' : 'medium'}
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                headCells={headCells}
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={scoreList.length}
                invisibleList={invisibleList}
                setInvisibleList={setInvisibleList}
              />
              <TableBody>
                {stableSort(scoreList, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((student, index) => {
                    var {
                      _id,
                      englishName,
                      schoolYear,
                      semester,
                      gpaSemester,
                      code,
                      classification,
                      gpa,
                      reason,
                      fCredits,
                    } = student;
                    const isItemSelected = isSelected(_id);
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        onClick={(event) => handleClick(event, _id)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={_id}
                        selected={isItemSelected}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{'aria-labelledby': labelId}}
                          />
                        </TableCell>
                        <TableCell
                          component="th"
                          id={labelId}
                          scope="student"
                          padding="none"
                        >
                          <Tippy
                            theme="light"
                            interactive={true}
                            animation="perspective"
                            placement={'right'}
                            content={
                              <UpdateStudentInfo
                                filter={filter}
                                student={student}
                              />
                            }
                          >
                            <TableCell align="left">
                              {invisibleList.includes('englishName') ? (
                                ''
                              ) : (
                                <Highlighter
                                  searchWords={[filter.keyword]}
                                  autoEscape={true}
                                  textToHighlight={englishName}
                                />
                              )}
                            </TableCell>
                          </Tippy>
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('code') ? '' : code}
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('schoolYear') ? (
                            ''
                          ) : (
                            <Highlighter
                              searchWords={[filter.schoolYear]}
                              autoEscape={true}
                              textToHighlight={schoolYear}
                            />
                          )}
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('semester') ? (
                            ''
                          ) : (
                            <Highlighter
                              searchWords={[filter.semester]}
                              autoEscape={true}
                              textToHighlight={semester}
                            />
                          )}
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('gpaSemester')
                            ? ''
                            : gpaSemester}
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('gpa') ? '' : gpa}
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('classification') ? (
                            ''
                          ) : (
                            <span
                              style={{
                                backgroundColor:
                                  filter.classification === 'all'
                                    ? 'white'
                                    : 'yellow',
                              }}
                            >
                              {classification
                                .replace('plus', '+')
                                .replace('score', '')}
                            </span>
                          )}
                        </TableCell>

                        <TableCell align="left">
                          {invisibleList.includes('fCredits') ? (
                            ''
                          ) : (
                            <span
                              style={{
                                backgroundColor:
                                  filter.fCredits === 'all'
                                    ? 'white'
                                    : 'yellow',
                              }}
                            >
                              {fCredits}
                            </span>
                          )}
                        </TableCell>
                        <TableCell align="left">
                          {invisibleList.includes('reason')
                            ? ''
                            : reason.map((item, index) => {
                                return (
                                  <div key={index}>
                                    <small>{item}</small>
                                    <hr
                                      style={{
                                        display:
                                          index === reason.length - 1
                                            ? 'none'
                                            : 'flex',
                                      }}
                                    />
                                  </div>
                                );
                              })}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{height: (dense ? 33 : 53) * emptyRows}}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[
              5,
              10,
              25,
              50,
              100,
              200,
              scoreList.length,
            ].sort((a, b) => {
              return a - b;
            })}
            component="div"
            count={scoreList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
        <FormControlLabel
          control={<Switch checked={dense} onChange={handleChangeDense} />}
          label="Dense padding"
        />
      </div>
    </ClickAwayListener>
  );
};

export default Index;
