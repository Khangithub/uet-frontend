import React, {useContext} from 'react';

import {CurrentUserContext} from '../../../../../../providers/CurrentUserContextProvider';
import Cookies from 'universal-cookie';
import {withRouter} from 'react-router-dom';
import {IconButton, CircularProgress} from '@material-ui/core';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import GetAppIcon from '@material-ui/icons/GetApp';

const DownloadButton = ({selected, scoreList, history, location, match}) => {
  const {classname} = match.params;
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const cookies = new Cookies();
  const token = cookies.get('token');

  var data = scoreList
    .filter((student) => {
      return selected.indexOf(student._id) > -1;
    })
    .map((student) => {
      delete student.englishName;
      return student;
    });

  const handleDownload = (event) => {
    event.preventDefault();

    fetch('https://uet-backend.herokuapp.com/users/export/xlsx', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({data, classname}),
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log(json);
        var a = document.createElement('a');
        a.href = json.downloadURL;
        a.style.display = 'none';
        a.download = 'download';
        a.click();
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <Tippy content="Download" placement="bottom">
      <IconButton
        onClick={handleDownload}
        style={{display: currentUser.isManager ? 'flex' : 'none'}}
      >
        <GetAppIcon />
      </IconButton>
    </Tippy>
  );
};

export default withRouter(DownloadButton);
