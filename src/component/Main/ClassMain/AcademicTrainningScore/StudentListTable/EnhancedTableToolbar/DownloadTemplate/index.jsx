import React, {useContext} from 'react';
import Cookies from 'universal-cookie';
import {CurrentUserContext} from '../../../../../../../providers/CurrentUserContextProvider';
import {
  Grid,
  IconButton,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import DescriptionIcon from '@material-ui/icons/Description';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';

const Index = ({classname}) => {
  const cookies = new Cookies();
  const token = cookies.get('token');
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const getTemplate = (event) => {
    event.preventDefault();

    fetch(`https://uet-backend.herokuapp.com/classes/template/${classname}`, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        var a = document.createElement('a');
        a.href = json.downloadURL;
        a.style.display = 'none';
        a.download = 'download';
        a.click();
      })
      .catch((err) => {
        console.error('Download Template', err);
        alert(JSON.stringify(err));
      });
  };

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <Tippy
      content={
        <Grid container justify="center" alignItems="center">
          <Grid item>
            <Typography align="center">Download template</Typography>
          </Grid>
          <Grid item>
            <img
              src="https://firebasestorage.googleapis.com/v0/b/fir-fb-auth.appspot.com/o/excel.png?alt=media&token=a5150bd3-76da-487c-9ac6-10f6ef13766c"
              alt="xlsx template"
              style={{height: '120px', width: '240px'}}
            />
          </Grid>
        </Grid>
      }
      placement="bottom"
    >
      <IconButton style={{display: currentUser.isManager ? 'flex' : 'none'}}>
        <DescriptionIcon
          color="action"
          fontSize="large"
          onClick={getTemplate}
        />
      </IconButton>
    </Tippy>
  );
};

export default Index;
