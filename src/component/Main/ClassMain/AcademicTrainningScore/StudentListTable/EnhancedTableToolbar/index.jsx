import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import { Toolbar, Typography, IconButton, Grid, TextField } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

import CancelButton from './CancelButton';
import DownloadButton from './DownloadButton';
import UpdateScoreButton from './UpdateScoreButton/';
import DownloadTemplate from './DownloadTemplate/';

import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
	root: {
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(1),
	},
	highlight:
		theme.palette.type === 'light'
			? {
					color: theme.palette.secondary.main,
					backgroundColor: lighten(theme.palette.secondary.light, 0.85),
			  }
			: {
					color: theme.palette.text.primary,
					backgroundColor: theme.palette.secondary.dark,
			  },
	title: {
		flex: '1 1 100%',
	},
}));

// selected: Array
// setSelected: Function, no params
// scoreList: Array
// filter: Object
// setFilter: Function, no params
const Index = ({ selected, setSelected, scoreList, filter, setFilter, history, location, match }) => {
	const classes = useStyles();
	var { classname } = match.params;
	return (
		<Toolbar
			className={clsx(classes.root, {
				[classes.highlight]: selected.length > 0,
			})}
		>
			{selected.length > 0 ? (
				<Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
					{selected.length} selected
				</Typography>
			) : (
				<Grid container alignItems="flex-end" spacing={6}>
					<Grid item>
						<Typography className={classes.title} variant="h6" id="tableTitle" component="div">
							Student List
						</Typography>
					</Grid>
					<Grid item>
						<TextField
							label="Student Name"
							value={filter.keyword}
							onChange={(event) => {
								setFilter({ ...filter, keyword: event.target.value });
							}}
						/>
					</Grid>
					<Grid item>
						<IconButton
							style={{ position: 'relative', bottom: '-10px', left: '-80px' }}
							onClick={() => {
								setFilter({ ...filter, keyword: '' });
							}}
						>
							<CloseIcon fontSize="small" />
						</IconButton>
					</Grid>
				</Grid>
			)}

			{selected.length > 0 ? (
				<>
					<DownloadButton selected={selected} scoreList={scoreList} />
					<CancelButton setSelected={setSelected} />
				</>
			) : (
				<>
					<UpdateScoreButton classname={classname}/>
					<DownloadTemplate classname={classname} />
				</>
			)}
		</Toolbar>
	);
};

Index.propTypes = {
	selected: PropTypes.array.isRequired,
	setSelected: PropTypes.func.isRequired,
};

export default withRouter(Index);
