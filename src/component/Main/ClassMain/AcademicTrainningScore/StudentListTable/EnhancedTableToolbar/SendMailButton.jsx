import React, { useState, forwardRef } from 'react';
import {
    IconButton,
    Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Slide,
} from '@material-ui/core';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import MailIcon from '@material-ui/icons/Mail';

import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	root: {
		'& .MuiDialog-scrollPaper': {
			display: ' flex',
			alignItems: 'flex-end',
			justifyContent: 'flex-start',
		},
	},
}));

const Transition = forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});

const SendMailButton = ({ selected, setSelected }) => {
	const [open, setOpen] = useState(false);
	const classes = useStyles();
	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	return (
		<>
			<Tippy content="Mail" placement="bottom">
				<IconButton aria-label="mail" onClick={handleClickOpen}>
					<MailIcon />
				</IconButton>
			</Tippy>
			<Dialog
				open={open}
				className={classes.root}
				TransitionComponent={Transition}
				keepMounted
				onClose={handleClose}
				aria-labelledby="alert-dialog-slide-title"
				aria-describedby="alert-dialog-slide-description"
			>
				<DialogTitle id="alert-dialog-slide-title">{"Use Google's location service?"}</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-slide-description">
						Let Google help apps determine location. This means sending anonymous location data to Google,
						even when no apps are running.
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="primary">
						Disagree
					</Button>
					<Button onClick={handleClose} color="primary">
						Agree
					</Button>
				</DialogActions>
			</Dialog>
		</>
	);
};

export default SendMailButton;
