import React, {useState, useContext} from 'react';
import {StudentListContext} from '../../../../../../../providers/StudentListContextProvider';
import {ClassListContext} from '../../../../../../../providers/ClassListContextProvider';
import {makeStyles} from '@material-ui/core/styles';
import {
  Button,
  TextField,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';
import LoadingModal from '../../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../../Common/ResponseModal';

import {withRouter} from 'react-router-dom';

import AttachFileIcon from '@material-ui/icons/AttachFile';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

// classname: String
const ModalContent = ({classname, history, location, match}) => {
  const classes = useStyles();
  var {setStudentList} = useContext(StudentListContext);
  var {setClassList} = useContext(ClassListContext);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  const [course, setCourse] = useState({
    schoolYear: '',
    semester: '',
    excel: '',
  });

  const [selectedExcel, setSelectedExcel] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);

    var {schoolYear, semester, excel} = course;
    var formData = new FormData();
    formData.append('schoolYear', schoolYear);
    formData.append('isStartYear', false);
    formData.append('semester', semester);
    formData.append('excel', excel);
    formData.append('classname', classname);

    fetch(`https://uet-backend.herokuapp.com/classes/${classname}/update`, {
      method: 'POST',
      headers: {
        Accept: 'multipart/form-data',
      },
      body: formData,
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var studentListResponse = await fetch(
            `https://uet-backend.herokuapp.com/students/from/class/${classname}`
          );
          var studentListJson = await studentListResponse.json();

          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes');
          var classListJson = await classListResponse.json();

          if (studentListJson.docs.length && classListJson.docs.length) {
            setStudentList(studentListJson.docs);
            setClassList(classListJson.docs);
            setLoading(false);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          setLoading(false);
          console.error('UpdateScoreButton', err);
          alert(JSON.stringify(err));
        }
        // fetch(`https://uet-backend.herokuapp.com/students/from/class/${classname}`)
        //   .then((response) => {
        //     return response.json();
        //   })
        //   .then((json) => {
        //     // setLoading(false);
        //     setStudentList(json.docs);
        //     fetch('https://uet-backend.herokuapp.com/classes')
        //       .then((response) => {
        //         return response.json();
        //       })
        //       .then((json) => {
        //         setLoading(false);
        //         setClassList(json.docs);
        //         return true;
        //       })
        //       .catch((error) => {
        //         setLoading(false);
        //         console.error(error);
        //       });
        //   })
        //   .catch((error) => {
        //     setLoading(false);
        //     console.error(error);
        //   });
      })
      .catch((error) => {
        console.error(error);
        alert(JSON.stringify(error));
      });
  };
  return (
    <div>
      <form enctype="multipart/form-data">
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="number"
          label="SchoolYear"
          name="schoolYear"
          fullWidth
          onChange={(event) =>
            setCourse({...course, schoolYear: event.target.value})
          }
        />
        <br />
        <FormControl style={{minWidth: 400}}>
          <InputLabel id="semester-select">Semester</InputLabel>
          <Select
            labelId="semester-select"
            onChange={(event) =>
              setCourse({...course, semester: event.target.value})
            }
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
          </Select>
        </FormControl>
        <br />
        <input
          className={classes.input}
          id="icon-button-file"
          type="file"
          accept=".xlsx"
          onChange={(event) => {
            setCourse({...course, excel: event.target.files[0]});
            setSelectedExcel(event.target.files[0].name);
          }}
        />

        <label htmlFor="icon-button-file">
          <IconButton
            color="primary"
            aria-label="upload picture"
            component="span"
          >
            <AttachFileIcon fontSize="large" /> {selectedExcel}
          </IconButton>
        </label>
        <br />

        <Button
          color="primary"
          variant="contained"
          fullWidth
          type="submit"
          onClick={handleSubmit}
        >
          Update Class
        </Button>
      </form>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </div>
  );
};

export default withRouter(ModalContent);
