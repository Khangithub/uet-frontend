import React, {useState, useContext} from 'react';

import {CurrentUserContext} from '../../../../../../../providers/CurrentUserContextProvider';
import {
  Backdrop,
  Fade,
  IconButton,
  Modal,
  CircularProgress,
} from '@material-ui/core';

import PublishIcon from '@material-ui/icons/Publish';
import {makeStyles} from '@material-ui/core/styles';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import ModalContent from './ModalContent';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

// classname: String
const Index = ({classname}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <>
      <Tippy content="Upload xlsx" placement="bottom">
        <IconButton style={{display: currentUser.isManager ? 'flex' : 'none'}}>
          <PublishIcon
            color="action"
            fontSize="large"
            onClick={() => {
              setOpen(true);
            }}
          />
        </IconButton>
      </Tippy>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <ModalContent classname={classname} />
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default Index;
