import React, {useState, useContext} from 'react';
import {
  IconButton,
  Grid,
  Avatar,
  Dialog,
  AppBar,
  Button,
  TableContainer,
  Paper,
  TableHead,
  TableRow,
  Toolbar,
  Typography,
  Slide,
  TableCell,
  Table,
  TableBody,
  TextField,
} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Cookies from 'universal-cookie';
import Highlighter from 'react-highlight-words';

import {formatClassname} from '../../../../../../utils/string';
import LoadingModal from '../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../Common/ResponseModal';
import {StudentListContext} from '../../../../../../providers/StudentListContextProvider';
import AddWarningReasonButton from './AddWarningReasonButton';
import EditWarningReasonButton from './EditWarningReasonButton';
import DeleteWarningReasonButton from './DeleteWarningReasonButton';

import EditIcon from '@material-ui/icons/Edit';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  snackbar: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#ccd3ff',
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const Index = ({filter, student, history, location, match}) => {
  const classes = useStyles();
  const [openDialog, setOpenDialog] = useState(false);
  var {setStudentList} = useContext(StudentListContext);
  const cookies = new Cookies();
  const token = cookies.get('token');

  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  var {
    profileImage,
    fullname,
    code,
    gpa,
    gpaSemester,
    classification,
    classname,
    fCredits,
    reason,
    schoolYear,
    semester,
  } = student;

  const [newValue, setNewValue] = useState({
    gpa,
    gpaSemester,
    fCredits,
    reason: [],
  });

  const handleDialogOpen = () => {
    setOpenDialog(true);
  };

  const handleDialogClose = () => {
    setOpenDialog(false);
  };

  const handleUpdate = (event) => {
    event.preventDefault();
    var {gpa, gpaSemester, fCredits, warnings, reason} = newValue;

    fetch(` https://uet-backend.herokuapp.com/students/${code}`, {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({
        gpa,
        gpaSemester,
        fCredits,
        warnings,
        reason,
        classname,
        schoolYear,
        semester,
      }),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        if (json.doc) {
          try {
            var studentListResponse = await fetch(
              `https://uet-backend.herokuapp.com/students/from/class/${classname}`
            );
            var studentListJson = await studentListResponse.json();

            if (studentListJson.docs.length) {
              setStudentList(studentListJson.docs);
              setLoading(false);
              setResponseJSON(json);
              setOpen(true);
            }
          } catch (err) {
            console.error('UpdateStudentInfo', err);
            setLoading(false);
            alert(JSON.stringify(err));
          }
        }
      })
      .catch((error) => {
        console.error(error);
        alert(JSON.stringify(error));
      });
  };

  return (
    <>
      <Grid container alignItems="center">
        <Grid item onClick={() => history.push(`/student/${code}`)}>
          <Grid container direction="column">
            <Grid item>
              <Avatar src={profileImage} />
            </Grid>
            <Grid item>
              <Highlighter
                searchWords={[filter.keyword]}
                autoEscape={true}
                textToHighlight={fullname}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <IconButton onClick={handleDialogOpen}>
            <EditIcon />
          </IconButton>
        </Grid>
      </Grid>

      <Dialog
        fullScreen
        open={openDialog}
        onClose={handleDialogClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleDialogClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              UPDATE
            </Typography>

            <Button
              autoFocus
              color="inherit"
              onClick={() => {
                history.push(`/student/${code}`);
              }}
            >
              View Profile
            </Button>
            <Button autoFocus color="inherit" onClick={handleUpdate}>
              save
            </Button>
          </Toolbar>
        </AppBar>

        <Grid container direction="column" justify="center">
          <Grid item>
            <Grid container alignItems="center">
              <Grid item>
                <img
                  src={profileImage}
                  alt=""
                  style={{
                    borderRadius: '50%',
                    height: '130px',
                    width: '130px',
                  }}
                />
              </Grid>
              <Grid item>
                <Grid container direction="column">
                  <Grid item>{fullname}</Grid>
                  <Grid item>{code}</Grid>
                  <Grid item>
                    The {semester === '1' ? 'first' : 'second'} semester of{' '}
                    {schoolYear}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item>
            <TableContainer component={Paper}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <StyledTableCell align="center">Class </StyledTableCell>
                    <StyledTableCell align="center">
                      School Year
                    </StyledTableCell>
                    <StyledTableCell align="center">Semester</StyledTableCell>
                    <StyledTableCell align="center">Total GPA</StyledTableCell>
                    <StyledTableCell align="center">
                      GPA Semester
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      Classification
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      Shorten Credits
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      Reason
                      <AddWarningReasonButton
                        reason={reason}
                        newValue={newValue}
                        setNewValue={setNewValue}
                        code={code}
                        semester={semester}
                        schoolYear={schoolYear}
                        classname={classname}
                      />
                    </StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <StyledTableRow>
                    <StyledTableCell align="center">
                      {formatClassname(classname)}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {schoolYear}
                    </StyledTableCell>
                    <StyledTableCell align="center">{semester}</StyledTableCell>
                    <StyledTableCell align="center">
                      <TextField
                        type="number"
                        InputProps={{inputProps: {min: 0, max: 4}}}
                        defaultValue={gpa}
                        style={{width: '50px'}}
                        onChange={(event) => {
                          setNewValue({...newValue, gpa: event.target.value});
                        }}
                      />
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <TextField
                        type="number"
                        InputProps={{inputProps: {min: 0, max: 4}}}
                        defaultValue={gpaSemester}
                        style={{width: '50px'}}
                        onChange={(event) => {
                          setNewValue({
                            ...newValue,
                            gpaSemester: event.target.value,
                          });
                        }}
                      />
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {classification}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <TextField
                        type="number"
                        InputProps={{inputProps: {min: 0, max: 30}}}
                        defaultValue={fCredits}
                        style={{width: '50px'}}
                        onChange={(event) => {
                          setNewValue({
                            ...newValue,
                            fCredits: event.target.value,
                          });
                        }}
                      />
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {reason.map((item, index) => {
                        return (
                          <Grid container key={index}>
                            <Grid item>
                              <Typography
                                key={index}
                                variant="caption"
                                display="block"
                                gutterBottom
                                align="left"
                              >
                                {item}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <EditWarningReasonButton
                                reason={reason}
                                newValue={newValue}
                                setNewValue={setNewValue}
                                code={code}
                                semester={semester}
                                schoolYear={schoolYear}
                                classname={classname}
                                index={index}
                              />
                            </Grid>
                            <Grid item>
                              <DeleteWarningReasonButton
                                reason={reason}
                                newValue={newValue}
                                setNewValue={setNewValue}
                                code={code}
                                semester={semester}
                                schoolYear={schoolYear}
                                classname={classname}
                                index={index}
                              />
                            </Grid>
                          </Grid>
                        );
                      })}
                    </StyledTableCell>
                  </StyledTableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Grid>
        <LoadingModal loading={loading} setLoading={setLoading} />
        <ResponseModal
          open={open}
          setOpen={setOpen}
          responseJSON={responseJSON}
          isError={isError}
        />
      </Dialog>
    </>
  );
};

export default withRouter(Index);
