import React, {useState, useContext} from 'react';
import Cookies from 'universal-cookie';
import {
  IconButton,
  Modal,
  Backdrop,
  Fade,
  Grid,
  TextField,
} from '@material-ui/core';
import {StudentListContext} from '../../../../../../providers/StudentListContextProvider';
import LoadingModal from '../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../Common/ResponseModal';
import {makeStyles} from '@material-ui/core/styles';

import EditIcon from '@material-ui/icons/Edit';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const EditWarningReasonButton = ({
  reason,
  newValue,
  setNewValue,
  code,
  semester,
  schoolYear,
  classname,
  index,
}) => {
  const classes = useStyles();
  const [openAddWarning, setOpenAddWarning] = useState(false);
  const cookies = new Cookies();
  const token = cookies.get('token');
  var {setStudentList} = useContext(StudentListContext);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  const handleUpdate = (event) => {
    event.preventDefault();
    setLoading(true);
    var {reason} = newValue;

    fetch(` https://uet-backend.herokuapp.com/students/${code}/warning`, {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({
        classname,
        schoolYear,
        semester,
        warnings: {
          semester,
          reason,
          schoolYear,
        },
      }),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var studentListResponse = await fetch(
            `https://uet-backend.herokuapp.com/students/from/class/${classname}`
          );
          var studentListJson = await studentListResponse.json();

          if (studentListJson.docs.length) {
            setLoading(false);
            setStudentList(studentListJson.docs);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          setLoading(false);
          console.error('editWarningReasonButton', err);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
        alert(JSON.stringify(error));
      });
  };

  return (
    <div style={{position: 'relative', top: '-10px'}}>
      <IconButton
        onClick={() => {
          setOpenAddWarning(true);
        }}
      >
        <EditIcon fontSize="small" />
      </IconButton>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={openAddWarning}
        onClose={() => {
          setOpenAddWarning(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openAddWarning}>
          <div className={classes.paper}>
            <Grid container direction="column" spacing={3}>
              <Grid item>
                <h2 id="transition-modal-title">Edit Warning Reason</h2>
              </Grid>
              <Grid item>
                <TextField
                  fullWidth
                  defaultValue={reason[index]}
                  onChange={(event) => {
                    var newReason = reason;
                    newReason[index] = event.target.value;

                    setNewValue({
                      ...newValue,
                      reason: newReason,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <Grid container justify="flex-end">
                  <Grid item>
                    <IconButton onClick={handleUpdate}>
                      <DoneIcon fontSize="medium" />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <IconButton
                      onClick={() => {
                        setOpenAddWarning(false);
                      }}
                    >
                      <CloseIcon fontSize="medium" />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Fade>
      </Modal>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </div>
  );
};

export default EditWarningReasonButton;
