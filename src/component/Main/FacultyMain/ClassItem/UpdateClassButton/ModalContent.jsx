import React, {useEffect, useState, useContext} from 'react';
import {
  Avatar,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Grid,
  CircularProgress,
} from '@material-ui/core';
import {formatClassname, toCapitalize} from '../../../../../utils/string';
import LoadingModal from '../../../../Common/LoadingModal';
import ResponseModal from '../../../../Common/ResponseModal';
import {ClassListContext} from '../../../../../providers/ClassListContextProvider';

// classname: String
// selectedConsultant: String, ObjectID
const ModalContent = ({classname, selectedConsultant, selectedFaculty}) => {
  const [consultantList, setConsultantList] = useState([]);
  const [facultyList, setFacultyList] = useState([]);
  const {setClassList} = useContext(ClassListContext);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const [newClass, setNewClass] = useState({
    newClassname: classname,
    consultant: selectedConsultant,
    faculty: selectedFaculty,
  });

  useEffect(() => {
    fetch('https://uet-backend.herokuapp.com/consultants/')
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        var {docs} = json;
        setLoading(false);
        setConsultantList(docs);
      })
      .catch((err) => {
        setLoading(false);
        console.error(err);
      });
  }, []);

  useEffect(() => {
    fetch('https://uet-backend.herokuapp.com/faculties/')
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        setFacultyList(json.docs);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    var {newClassname, consultant, faculty} = newClass;

    fetch(`https://uet-backend.herokuapp.com/classes/${classname}`, {
      method: 'PATCH',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({newClassname, consultant, faculty}),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
          var classListJson = await classListResponse.json();

          if (classListJson.docs) {
            setLoading(false);
            setClassList(classListJson.docs);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          console.error('DeleteClassButton', err);
          setLoading(false);
        }
      });
  };

  return loading ? (
    <LoadingModal loading={loading} setLoading={setLoading} />
  ) : (
    <>
      <form>
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="text"
          label="Classname"
          name="classname"
          fullWidth
          defaultValue={formatClassname(classname)}
          onChange={(event) =>
            setNewClass({...newClass, newClassname: event.target.value})
          }
        />
        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="consultant-select">Consultant</InputLabel>
          <Select
            labelId="consultant-select"
            onChange={(event) =>
              setNewClass({...newClass, consultant: event.target.value})
            }
          >
            {consultantList === [] ? (
              <CircularProgress />
            ) : (
              consultantList.map((item, index) => {
                const {_id, fullname, academicRank, profileImage} = item;
                return (
                  <MenuItem key={index} value={_id}>
                    <Avatar
                      alt={fullname}
                      src={profileImage}
                      style={{marginRight: '10px'}}
                    />
                    {toCapitalize(academicRank)}. {fullname}
                  </MenuItem>
                );
              })
            )}
          </Select>
        </FormControl>
        <br />

        <FormControl style={{minWidth: 400, marginBottom: '40px'}}>
          <InputLabel id="faculty-select">Faculty</InputLabel>
          <Select
            labelId="faculty-select"
            onChange={(event) =>
              setNewClass({...newClass, faculty: event.target.value})
            }
          >
            {facultyList === [] ? (
              <CircularProgress />
            ) : (
              facultyList.map((facultyListItem, index) => {
                const {code, facultyName, _id} = facultyListItem;
                return (
                  <MenuItem key={index} value={_id}>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      alignItems="center"
                      spacing={4}
                    >
                      <Grid item>{facultyName}</Grid>
                      <Grid item>
                        <small>{code}</small>
                      </Grid>
                    </Grid>
                  </MenuItem>
                );
              })
            )}
          </Select>
        </FormControl>

        <br />
        <Button
          color="primary"
          variant="contained"
          fullWidth
          type="submit"
          onClick={handleSubmit}
        >
          Update Class {formatClassname(classname)}
        </Button>
      </form>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default ModalContent;
