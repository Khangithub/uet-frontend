import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Backdrop, Fade, IconButton, Modal} from '@material-ui/core';

import DeleteIcon from '@material-ui/icons/Delete';
import ModalContent from './ModalContent';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,

    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

// classname: String
const Index = ({classname}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  return (
    <div>
      <IconButton>
        <DeleteIcon
          onClick={() => {
            setOpen(true);
          }}
        />
      </IconButton>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <ModalContent classname={classname} setOpen={setOpen} />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default Index;
