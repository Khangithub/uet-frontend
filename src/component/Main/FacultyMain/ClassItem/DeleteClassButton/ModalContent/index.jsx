import React from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { formatClassname } from '../../../../../../utils/string';

import CancelButton from './CancelButton';
import DeleteButton from './DeleteButton';

const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	input: {
		display: 'none',
	},
}));

// classname: String
const ModalContent = ({ classname, setOpen }) => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<form>
				<Typography variant="h6" gutterBottom>
					<strong> Are you sure you want to delete the class {formatClassname(classname)} ?</strong>
				</Typography>

				<DeleteButton classname={classname} />
				<CancelButton setOpen={setOpen} />
			</form>
		</div>
	);
};

export default withRouter(ModalContent);
