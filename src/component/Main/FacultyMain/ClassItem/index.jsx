import React from 'react';
import {formatClassname, formatAcademicRank} from '../../../../utils/string';

import {withRouter} from 'react-router-dom';
import {Avatar, Grid, Paper} from '@material-ui/core';

import UpdateClassButton from './UpdateClassButton/';
import DeleteClassButton from './DeleteClassButton/';

//classList: Array
const ClassItem = ({classList, history, location, match}) => {
  return (
    <div>
      <Grid container spacing={2} direction="column" justify="flex-start">
        {classList.map((classListItem, index) => {
          const {consultant, faculty, classname} = classListItem;
          const {facultyName} = faculty;
          const {profileImage, fullname, academicRank} = consultant;

          return (
            <Grid container spacing={3} style={{margin: '2px 0px'}} key={index}>
              <Grid
                item
                xs={10}
                onClick={() => {
                  history.push(`/class/${classname}`);
                }}
              >
                <Paper
                  component="div"
                  elevation={8}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <div>
                    <strong>{formatClassname(classname)}</strong>
                  </div>

                  <div>
                    <Avatar alt={fullname} src={profileImage} />
                  </div>

                  <div>
                    <em>{`${formatAcademicRank(
                      academicRank
                    )}. ${fullname}`}</em>
                  </div>

                  <div>|</div>

                  <div>{facultyName}</div>
                </Paper>
              </Grid>
              <Grid item xs={2}>
                <Paper
                  component="div"
                  elevation={0}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <UpdateClassButton
                    classname={classname}
                    selectedConsultant={consultant._id}
                    selectedFaculty={faculty._id}
                  />
                  <DeleteClassButton classname={classname} />
                </Paper>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};

export default withRouter(ClassItem);
