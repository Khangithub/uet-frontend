import React, {useEffect, useState} from 'react';
import Chart from 'react-google-charts';
import RandomColor from 'randomcolor';

//classList: array
const ClassChart = ({classList}) => {
  const {innerWidth} = window;
  let [screenWidth, setScreenWidth] = useState(innerWidth);

  useEffect(() => {
    window.onresize = () => {
      setScreenWidth(window.innerWidth);
    };
  }, [innerWidth]);

  const data = [
    ['Classname', 'ClassSize', 'warnedLength', {role: 'style'}],
  ].concat(
    classList.map((classListItem) => {
      const {
        classname,
        faculty,
        classSize,
        warnedLength,
        currentSchoolYear,
        currentSemester,
      } = classListItem;
      const {code} = faculty;
      return [
        `${classname} (code: ${code}, schoolYear: ${currentSchoolYear}, semester: ${currentSemester})`,
        classSize,
        warnedLength,
        RandomColor(),
      ];
    })
  );

  return (
    <div>
      <Chart
        chartType="ColumnChart"
        width={(screenWidth * 80) / 100}
        height="400px"
        data={data}
      />
    </div>
  );
};

export default ClassChart;
