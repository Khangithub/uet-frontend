import React, {useContext} from 'react';
import ClassChart from './ClassChart';
import ClassItem from './ClassItem/';
import {ClassListContext} from '../../../providers/ClassListContextProvider';

import LoadingModal from '../../Common/LoadingModal';

const Index = ({code}) => {
  const {classList, getClassListLoading, setClassListLoading} = useContext(ClassListContext);

  const filterClassList = classList.filter((classListItem) => {
    return classListItem.faculty.code === code;
  });
  return (
    <div>
      {getClassListLoading ? (
        <LoadingModal loading={getClassListLoading} setLoading={setClassListLoading} />
      ) : (
        <>
          <ClassChart classList={filterClassList} />
          <ClassItem classList={filterClassList} />
        </>
      )}
    </div>
  );
};

export default Index;
