import React, {useContext} from 'react';
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  Grid,
} from '@material-ui/core';
import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';
import LoadingModal from '../Common/LoadingModal';
import list from '../Sidebar/ListItem';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import DuoIcon from '@material-ui/icons/Duo';

const useStyles = makeStyles({
  container: {marginTop: '30px'},
  MuiCardMediaRoot: {
    '& .MuiCardMedia-root': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    maxWidth: 345,
  },
  MuiSvgIconFontSizeLarge: {
    '& .MuiSvgIcon-fontSizeLarge': {
      fontSize: '5rem',
      color: 'rgba(0, 0, 0, 0.54)',
    },
  },
});

const HomeMain = ({history, location, match}) => {
  const classes = useStyles();
  const {
    currentUser,
    getCurrentUserLoading,
    setCurrentUserLoading,
  } = useContext(CurrentUserContext);

  return getCurrentUserLoading ? (
    <LoadingModal
      loading={getCurrentUserLoading}
      setLoading={setCurrentUserLoading}
    />
  ) : (
    <Grid
      container
      className={classes.container}
      justify="space-around"
      alignItems="center"
      spacing={1}
    >
      {list.map((listItem, index) => {
        return (
          <Grid
            style={{
              display: listItem.visibleList.includes(currentUser.role)
                ? 'flex'
                : 'none',
            }}
            item
            key={index}
            onClick={() => history.push(listItem.pathname)}
          >
            <Card className={classes.MuiCardMediaRoot}>
              <CardActionArea>
                <CardMedia
                  className={classes.MuiSvgIconFontSizeLarge}
                  component="div"
                  alt="Contemplative Reptile"
                  height="140"
                  children={listItem.icon}
                  title={listItem.name}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {listItem.name}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {listItem.description}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        );
      })}
      <Grid
        item
        onClick={() => window.open('https://zoom1205.herokuapp.com', '_blank')}
      >
        <Card className={classes.MuiCardMediaRoot}>
          <CardActionArea>
            <CardMedia
              className={classes.MuiSvgIconFontSizeLarge}
              component="div"
              alt="Contemplative Reptile"
              height="140"
              children={<DuoIcon fontSize="large" />}
              title="Meeting"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Meeting
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                allows existing users to video call to many others
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    </Grid>
  );
};

export default withRouter(HomeMain);
