import React from 'react';
import {Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import CancelButton from './CancelButton';
import DeleteConsultantButton from './DeleteConsultantButton';
import DeleteLeaderButton from './DeleteLeaderButton';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

// user: String
const ModalContent = ({user, setOpen}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <form>
        <Typography variant="h6" gutterBottom>
          <strong>
            Are you sure you want to delete user {user.fullname} ({user.code}) ?
          </strong>
        </Typography>

        {user.role === 'consultant' ? (
          <DeleteConsultantButton user={user} />
        ) : (
          <DeleteLeaderButton user={user} />
        )}
        <CancelButton setOpen={setOpen} />
      </form>
    </div>
  );
};

export default withRouter(ModalContent);
