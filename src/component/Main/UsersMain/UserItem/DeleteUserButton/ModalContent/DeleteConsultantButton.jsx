import React, {useState, useContext} from 'react';
import {Button} from '@material-ui/core';
import LoadingModal from '../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../Common/ResponseModal';
import {ClassListContext} from '../../../../../../providers/ClassListContextProvider';
import {NoneStudentListContext} from '../../../../../../providers/NoneStudentListContextProvider';
import Cookies from 'universal-cookie';

const DeleteConsultantButton = ({user}) => {
  const {classList, getClassListLoading, setClassListLoading} = useContext(
    ClassListContext
  );
  const {setNoneStudentList} = useContext(NoneStudentListContext);
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [open, setOpen] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  var taughtClassList = classList.filter((classItem) => {
    return classItem.consultant.code === user.code;
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    setClassListLoading(true);

    fetch(`https://uet-backend.herokuapp.com/users/as/consultant/${user.code}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer '.concat(token),
      },
      body: JSON.stringify({isConsultant: true}),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var noneStudentListResponse = await fetch(
            'https://uet-backend.herokuapp.com/users/none/student/user',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8',
              },
            }
          );

          var noneStudentListJson = await noneStudentListResponse.json();

          if (noneStudentListJson.users) {
            setNoneStudentList(noneStudentListJson.users);
            setClassListLoading(false);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          setClassListLoading(false);
          console.error('AddUserButton', err);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return getClassListLoading ? (
    <LoadingModal
      loading={getClassListLoading}
      setLoading={setClassListLoading}
    />
  ) : (
    <>
      <Button
        color="secondary"
        variant="contained"
        fullWidth
        style={{margin: '10px 0px'}}
        type="submit"
        disabled={taughtClassList.length > 0 ? true : false}
        onClick={handleSubmit}
      >
        Delete User {user.fullname} ({user.code})
      </Button>
      <LoadingModal
        loading={getClassListLoading}
        setLoading={setClassListLoading}
      />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default DeleteConsultantButton;
