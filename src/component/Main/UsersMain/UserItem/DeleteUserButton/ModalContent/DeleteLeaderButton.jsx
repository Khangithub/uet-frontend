import React, {useState, useContext} from 'react';
import {Button} from '@material-ui/core';
import LoadingModal from '../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../Common/ResponseModal';
import {FacultyContext} from '../../../../../../providers/FacultyContextProvider';
import {NoneStudentListContext} from '../../../../../../providers/NoneStudentListContextProvider';
import Cookies from 'universal-cookie';

const DeleteLeaderButton = ({user}) => {
  const {
    facultyList,
    getFacultyListLoading,
    setFacultyListLoading,
  } = useContext(FacultyContext);
  const {setNoneStudentList} = useContext(NoneStudentListContext);
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [open, setOpen] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  var deanedFacultyList = facultyList.filter((classItem) => {
    return classItem.dean.code === user.code;
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    setFacultyListLoading(true);

    fetch(`https://uet-backend.herokuapp.com/users/as/leader/${user.code}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer '.concat(token),
      },
      body: JSON.stringify({isConsultant: false}),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var noneStudentListResponse = await fetch(
            'https://uet-backend.herokuapp.com/users/none/student/user',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8',
              },
            }
          );

          var noneStudentListJson = await noneStudentListResponse.json();

          if (noneStudentListJson.users) {
            setNoneStudentList(noneStudentListJson.users);
            setFacultyListLoading(false);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          setFacultyListLoading(false);
          console.error('AddUserButton', err);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return getFacultyListLoading ? (
    <LoadingModal
      loading={getFacultyListLoading}
      setLoading={setFacultyListLoading}
    />
  ) : (
    <>
      <Button
        color="secondary"
        variant="contained"
        fullWidth
        style={{margin: '10px 0px'}}
        type="submit"
        disabled={deanedFacultyList.length > 0 ? true : false}
        onClick={handleSubmit}
      >
        Delete User {user.fullname} ({user.code})
      </Button>
      <LoadingModal
        loading={getFacultyListLoading}
        setLoading={setFacultyListLoading}
      />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default DeleteLeaderButton;
