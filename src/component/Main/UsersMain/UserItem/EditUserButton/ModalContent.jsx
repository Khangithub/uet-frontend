import React, {useState, useContext} from 'react';
import {
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
} from '@material-ui/core';
import ResponseModal from '../../../../Common/ResponseModal';
import LoadingModal from '../../../../Common/LoadingModal';
import Cookies from 'universal-cookie';

import {toCapitalize} from '../../../../../utils/string';
import {makeStyles} from '@material-ui/core/styles';
import {NoneStudentListContext} from '../../../../../providers/NoneStudentListContextProvider';
import {ClassListContext} from '../../../../../providers/ClassListContextProvider';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));
const ModalContent = ({user}) => {
  const classes = useStyles();
  const {
    getNoneStudentListLoading,
    setNoneStudentList,
    setNoneStudentListLoading,
  } = useContext(NoneStudentListContext);
  const {setClassList} = useContext(ClassListContext);
  const [newUser, setNewUser] = useState({
    fullname: user.fullname,
    code: user.code,
    role: user.role,
    academicRank: user.academicRank,
  });
  const [open, setOpen] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const cookies = new Cookies();
  const token = cookies.get('token');

  const handleSubmit = (event) => {
    event.preventDefault();
    setNoneStudentListLoading(true);
    var {fullname, role, code, academicRank} = newUser;

    fetch(`https://uet-backend.herokuapp.com/users/${user.code}`, {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({
        fullname,
        role,
        code,
        vnumail: code + '@vnu.edu.vn',
        academicRank,
        previousRole: user.role,
      }),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var noneStudentListResponse = await fetch(
            'https://uet-backend.herokuapp.com/users/none/student/user',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8',
              },
            }
          );

          var noneStudentListJson = await noneStudentListResponse.json();

          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
          var classListJson = await classListResponse.json();

          if (noneStudentListJson.users && classListJson.docs) {
            setNoneStudentList(noneStudentListJson.users);
            setClassList(classListJson.docs);
            setNoneStudentListLoading(false);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          setNoneStudentList(false);
          console.error('EditUserButton', err);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
        setNoneStudentListLoading(false);
      });
  };

  return (
    <div className={classes.root}>
      <form enctype="multipart/form-data">
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="text"
          label="Fullname"
          name="fullname"
          defaultValue={newUser.fullname}
          fullWidth
          onChange={(event) =>
            setNewUser({...newUser, fullname: event.target.value})
          }
        />
        <br />
        <TextField
          label="Code"
          name="code"
          defaultValue={newUser.code}
          fullWidth
          onChange={(event) =>
            setNewUser({...newUser, code: event.target.value})
          }
        />
        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="role-select">Role</InputLabel>
          <Select
            labelId="role-select"
            defaultValue={newUser.role}
            onChange={(event) =>
              setNewUser({...newUser, role: event.target.value})
            }
          >
            {[
              'consultant',
              'specialist',
              'uet-leader',
              'student-affair-leader',
              'academic-leader',
            ].map((roleItem, index) => {
              return (
                <MenuItem value={roleItem} key={index}>
                  {toCapitalize(roleItem.replaceAll('-', ' '))}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>

        <br />
        <FormControl style={{minWidth: 400, marginBottom: '30px'}}>
          <InputLabel id="rank-select">Academic Rank</InputLabel>
          <Select
            labelId="rank-select"
            defaultValue={newUser.academicRank}
            onChange={(event) =>
              setNewUser({...newUser, academicRank: event.target.value})
            }
          >
            {['master', 'phd', 'professor'].map((rankItem, index) => {
              return (
                <MenuItem value={rankItem} key={index}>
                  {toCapitalize(rankItem.replaceAll('-', ' '))}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <br />
        <Button
          color="primary"
          variant="contained"
          fullWidth
          onClick={handleSubmit}
          type="submit"
        >
          UPDATE User
        </Button>
      </form>
      <LoadingModal
        loading={getNoneStudentListLoading}
        setLoading={setNoneStudentListLoading}
      />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </div>
  );
};

export default ModalContent;
