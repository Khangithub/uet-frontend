import React, {useState, useEffect} from 'react';
import {Grid, Avatar, Paper} from '@material-ui/core';
import {withRouter} from 'react-router-dom';

import EditUserButton from './EditUserButton';
import DeleteUserButton from './DeleteUserButton/';

const Index = ({noneStudentList, history, location, match}) => {
  const {innerWidth} = window;
  let [screenWidth, setScreenWidth] = useState(innerWidth);

  useEffect(() => {
    window.onresize = () => {
      setScreenWidth(window.innerWidth);
    };
  }, [innerWidth]);

  return (
    <div>
      <Grid
        container
        spacing={2}
        direction="column"
        justify="flex-start"
        style={{width: `${screenWidth * 0.8}px`}}
      >
        {noneStudentList.map((noneStudentListItem, index) => {
          const {profileImage, fullname, role, code} = noneStudentListItem;

          return (
            <Grid container spacing={3} style={{margin: '2px 0px'}} key={index}>
              <Grid
                item
                xs={10}
                onClick={() => history.push(`/user/${role}/${code}`)}
              >
                <Paper
                  component="div"
                  elevation={8}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <div>
                    <Avatar alt={profileImage} src={profileImage} />
                  </div>

                  <div>
                    <strong>{fullname}</strong>
                  </div>

                  <div>
                    <em>{code}</em>
                  </div>

                  <div>
                    <small>{role}</small>
                  </div>
                </Paper>
              </Grid>
              <Grid item xs={2}>
                <Paper
                  component="div"
                  elevation={0}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <EditUserButton user={noneStudentListItem} />
                  <DeleteUserButton user={noneStudentListItem} />
                </Paper>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};

export default withRouter(Index);
