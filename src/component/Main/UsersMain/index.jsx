import React, {useContext} from 'react';
import {Grid} from '@material-ui/core';
import {NoneStudentListContext} from '../../../providers/NoneStudentListContextProvider';
import AddUserButton from './AddUserButton/';
import UserItem from './UserItem/';
import LoadingModal from '../../Common/LoadingModal';

const Index = () => {
  var {noneStudentList, getNoneStudentListLoading, setNoneStudentListLoading} = useContext(
    NoneStudentListContext
  );

  return (
    <div>
      <Grid container spacing={3} direction="column" justify="center">
        <Grid item>
          <AddUserButton />
        </Grid>
        <Grid item>
          {getNoneStudentListLoading || noneStudentList === null || noneStudentList === undefined ? (
            <LoadingModal loading={getNoneStudentListLoading} setLoading={setNoneStudentListLoading} />
          ) : (
            <UserItem noneStudentList={noneStudentList} />
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default Index;
