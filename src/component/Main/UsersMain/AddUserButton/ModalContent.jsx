import React, {useState, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Button,
  FormControl,
  InputLabel,
  Select,
  TextField,
  MenuItem,
} from '@material-ui/core';
import {toCapitalize} from '../../../../utils/string';
import ResponseModal from '../../../Common/ResponseModal';
import LoadingModal from '../../../Common/LoadingModal';
import Cookies from 'universal-cookie';
import {NoneStudentListContext} from '../../../../providers/NoneStudentListContextProvider';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const ModalContent = () => {
  const classes = useStyles();
  const {
    getNoneStudentListLoading,
    setNoneStudentList,
    setNoneStudentListLoading,
  } = useContext(NoneStudentListContext);
  const [user, setUser] = useState({
    fullname: '',
    role: '',
    code: '',
    academicRank: '',
  });

  const [open, setOpen] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const cookies = new Cookies();
  const token = cookies.get('token');

  const handleSubmit = (event) => {
    event.preventDefault();
    setNoneStudentListLoading(true);
    var {fullname, role, code, academicRank} = user;

    fetch('https://uet-backend.herokuapp.com/users/', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({fullname, role, code, academicRank}),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var noneStudentListResponse = await fetch(
            'https://uet-backend.herokuapp.com/users/none/student/user',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8',
              },
            }
          );

          var noneStudentListJson = await noneStudentListResponse.json();

          if (noneStudentListJson.users) {
            setNoneStudentList(noneStudentListJson.users);
            setNoneStudentListLoading(false);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          setNoneStudentListLoading(false);
          console.error('AddUserButton', err);
          alert(JSON.stringify(err));
        }
      })
      .catch((error) => {
        console.error(error);
        setNoneStudentListLoading(false);
      });
  };

  return (
    <div className={classes.root}>
      <form enctype="multipart/form-data">
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="text"
          label="Fullname"
          name="fullname"
          fullWidth
          onChange={(event) => setUser({...user, fullname: event.target.value})}
        />
        <br />
        <TextField
          label="Code"
          name="code"
          fullWidth
          onChange={(event) => setUser({...user, code: event.target.value})}
        />
        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="role-select">Role</InputLabel>
          <Select
            labelId="role-select"
            onChange={(event) => setUser({...user, role: event.target.value})}
          >
            {[
              'consultant',
              'specialist',
              'uet-leader',
              'student-affair-leader',
              'academic-leader',
            ].map((roleItem, index) => {
              return (
                <MenuItem value={roleItem} key={index}>
                  {toCapitalize(roleItem.replaceAll('-', ' '))}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>

        <br />
        <FormControl style={{minWidth: 400, marginBottom: '30px'}}>
          <InputLabel id="rank-select">Academic Rank</InputLabel>
          <Select
            labelId="rank-select"
            onChange={(event) =>
              setUser({...user, academicRank: event.target.value})
            }
          >
            {['master', 'phd', 'professor'].map((rankItem, index) => {
              return (
                <MenuItem value={rankItem} key={index}>
                  {toCapitalize(rankItem.replaceAll('-', ' '))}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <br />
        <Button
          color="primary"
          variant="contained"
          fullWidth
          onClick={handleSubmit}
          type="submit"
        >
          Add User
        </Button>
      </form>
      <LoadingModal
        loading={getNoneStudentListLoading}
        setLoading={setNoneStudentListLoading}
      />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </div>
  );
};

export default ModalContent;
