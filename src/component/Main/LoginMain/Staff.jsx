import React, {useContext, useState} from 'react';
import uetLogo from '../../../images/uet-logo.png';
import {
  Button,
  Grid,
  Link,
  TextField,
  Typography,
  IconButton,
  ClickAwayListener,
} from '@material-ui/core';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';

import {withRouter} from 'react-router-dom';
import Cookies from 'universal-cookie';
import {makeStyles} from '@material-ui/core/styles';
import LoadingModal from '../../Common/LoadingModal';

import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '1200px',
    position: 'relative',
    top: '50%',
    transform: 'translateY(50%)',
    [theme.breakpoints.up('sm')]: {
      marginTop: '-60px',
    },
  },
  logo: {height: '100px', width: '100px'},
  vnuMailInput: {
    width: '100%',
    minWidth: '350px',
    margin: '5px 0px',
  },
  passwordInput: {
    width: '100%',
    minWidth: '350px',
    margin: '5px 0px',
  },
  visiblePasswordButton: {
    position: 'relative',
    top: '-55px',
    right: '-150px',
  },
  loginButton: {
    width: '100%',
    minWidth: '350px',
    position: 'relative',
    top: '-30px',
  },
  MuiItem: {
    '& .MuiGrid-item': {
      padding: '20px',
    },
  },
}));

const Staff = ({history, isStudent, setStudent}) => {
  const classes = useStyles();
  const [sending, setSending] = useState(false);
  const {setUser, setCurrentUserLoading} = useContext(CurrentUserContext);

  const [account, setAccount] = useState({
    vnumail: '',
    password: '',
  });
  const [visiblePassword, setVisiblePassword] = useState(false);
  const [error, setError] = useState({
    vnumail: '',
    password: '',
  });

  const handleLogin = (event) => {
    event.preventDefault();
    setSending(true);
    var {vnumail, password} = account;

    fetch('https://uet-backend.herokuapp.com/users/login/staff', {
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({vnumail, password}),
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        setSending(false);
        if (json.message) {
          const {message} = json;

          switch (message) {
            case 'Mail not found':
              setError({vnumail: message, password: ''});
              break;
            case 'Wrong password':
              setError({vnumail: '', password: message});
              break;
            case 'Verify error':
              setError({
                vnumail: message,
                password: message,
              });
              break;
            case 'Emptied Mail':
              setError({
                vnumail: message,
                password: '',
              });
              break;
            case 'Emptied Password':
              setError({
                vnumail: '',
                password: message,
              });
              break;
            case 'Invalid Mail':
              setError({
                vnumail: message,
                password: '',
              });
              break;
            default:
              setError({vnumail: '', password: ''});
              break;
          }
        }

        if (json.token) {
          const {token} = json;
          const cookies = new Cookies();
          cookies.set('token', token);

          fetch('https://uet-backend.herokuapp.com/users/me', {
            method: 'GET',
            headers: {
              Authorization: 'Bearer '.concat(token),
              'content-type': 'application/json; charset=UTF-8',
            },
          })
            .then((response) => {
              return response.json();
            })
            .then((json) => {
              var {currentUser} = json;
              if (currentUser) {
                setUser({
                  ...currentUser,
                  isManager: ['specialist', 'student-affair-leader'].includes(
                    currentUser.role
                  )
                    ? true
                    : false,
                });
                setCurrentUserLoading(false);
                history.push('/');
              } else {
                alert(JSON.stringify(json));
              }
            })
            .catch((err) => {
              console.error(err);
            });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleKeyDown = (event) => {
    var charCode = typeof event.which == 'number' ? event.which : event.keyCode;
    return charCode === 13 ? handleLogin(event) : '';
  };

  const handleClickAway = () => {
    setError({vnumail: '', password: ''});
  };

  return (
    <div className={classes.root} style={isStudent ? {display: 'none'} : {}}>
      <Grid container justify="center" alignItems="center" direction="column">
        <Grid item>
          <img src={uetLogo} alt="uet-logo" className={classes.logo} />
        </Grid>
        <Grid item>
          <Typography variant="overline" display="block" gutterBottom>
            Login as Staff
          </Typography>
        </Grid>

        <Grid item>
          <ClickAwayListener onClickAway={handleClickAway}>
            <TextField
              error={error.vnumail === '' ? false : true}
              className={classes.vnuMailInput}
              type="text"
              variant="outlined"
              label="VNU-Mail"
              required
              name="vnumail"
              helperText={error.vnumail === '' ? null : error.vnumail}
              fullWidth
              onChange={(event) => {
                setError({...error, vnumail: ''});
                setAccount({...account, vnumail: event.target.value});
              }}
              onKeyDown={handleKeyDown}
            />
          </ClickAwayListener>
        </Grid>

        <Grid item>
          <ClickAwayListener onClickAway={handleClickAway}>
            <TextField
              error={error.password === '' ? false : true}
              className={classes.passwordInput}
              type={visiblePassword ? 'text' : 'password'}
              variant="outlined"
              label="Password"
              required
              name="password"
              helperText={error.password === '' ? null : error.password}
              onChange={(event) => {
                setError({...error, password: ''});
                setAccount({...account, password: event.target.value});
              }}
              onKeyDown={handleKeyDown}
            />
          </ClickAwayListener>
        </Grid>

        <Grid item>
          <IconButton
            className={classes.visiblePasswordButton}
            onClick={() => {
              setVisiblePassword(!visiblePassword);
            }}
          >
            {visiblePassword ? (
              <VisibilityOffIcon color="action" fontSize="small" />
            ) : (
              <VisibilityIcon color="action" fontSize="small" />
            )}
          </IconButton>
        </Grid>

        <Grid item>
          <Button
            color="primary"
            type="submit"
            className={classes.loginButton}
            variant="contained"
            onClick={handleLogin}
          >
            Login
          </Button>
        </Grid>

        <Grid item>
          <Grid
            container
            direction="row"
            justify="center"
            spacing={10}
            alignItems="center"
            className={classes.MuiItem}
          >
            <Grid item>
              <Link href="#"> {'forget password?'}</Link>
            </Grid>
            <Grid item>
              <Link
                href="#"
                onClick={() => {
                  setStudent(true);
                }}
              >
                {'i am a student'}
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <LoadingModal loading={sending} setLoading={setSending} />
    </div>
  );
};

export default withRouter(Staff);
