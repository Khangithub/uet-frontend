import React, {useState, useContext} from 'react';
import uetLogo from '../../../images/uet-logo.png';
import {
  Button,
  Grid,
  Link,
  TextField,
  Typography,
  IconButton,
  ClickAwayListener,
} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import Cookies from 'universal-cookie';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';
import {CurrentParentsContext} from '../../../providers/CurrentParentsContextProvider';
import LoadingModal from '../../Common/LoadingModal';

import {makeStyles} from '@material-ui/core/styles';

import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    width: '100%',
    maxWidth: '1200px',
    top: '50%',
    transform: 'translateY(50%)',
    [theme.breakpoints.up('sm')]: {
      marginTop: '-60px',
    },
  },

  logo: {height: '100px', width: '100px'},

  studentCodeInput: {
    width: '100%',
    minWidth: '350px',
    margin: '5px 0px',
  },
  passwordInput: {
    width: '100%',
    minWidth: '350px',
    margin: '5px 0px',
  },
  visiblePasswordButton: {
    position: 'relative',
    top: '-55px',
    right: '-150px',
  },
  loginButton: {
    width: '100%',
    minWidth: '350px',
    position: 'relative',
    top: '-30px',
  },
  MuiItem: {
    '& .MuiGrid-item': {
      padding: '20px',
    },
  },
}));

const Student = ({history, isStudent, setStudent}) => {
  const classes = useStyles();
  const {setUser, setCurrentUserLoading} = useContext(CurrentUserContext);
  const {setParents, setCurrentParentsLoading} = useContext(
    CurrentParentsContext
  );
  const [sending, setSending] = useState(false);
  const [account, setAccount] = useState({
    code: '',
    password: '',
  });
  const [visiblePassword, setVisiblePassword] = useState(false);
  const [error, setError] = useState({
    code: '',
    password: '',
  });

  const handleLogin = async (event) => {
    event.preventDefault();
    setSending(true);
    var {code, password} = account;

    fetch('https://uet-backend.herokuapp.com/users/login/student', {
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({code, password}),
    })
      .then((response) => {
        return response.json();
      })
      .then(async (json) => {
        setSending(false);
        if (json.message) {
          const {message} = json;

          switch (message) {
            case 'Code not found':
              setError({code: message, password: ''});
              break;
            case 'Wrong password':
              setError({code: '', password: message});
              break;
            case 'Verify error':
              setError({
                code: message,
                password: message,
              });
              break;
            case 'Emptied Code':
              setError({
                code: message,
                password: '',
              });
              break;
            case 'Emptied Password':
              setError({
                code: '',
                password: message,
              });
              break;
            case 'Invalid Student Code':
              setError({
                code: message,
                password: '',
              });
              break;
            default:
              setError({code: '', password: ''});
              break;
          }
        }

        if (json.token) {
          const {token} = json;
          const cookies = new Cookies();
          cookies.set('token', token);

          try {
            var currentUserResponse = await fetch(
              'https://uet-backend.herokuapp.com/users/me',
              {
                method: 'GET',
                headers: {
                  Authorization: 'Bearer '.concat(token),
                  'content-type': 'application/json; charset=UTF-8',
                },
              }
            );

            var currentUserJson = await currentUserResponse.json();
            var {currentUser} = currentUserJson;

            var currentParentsResponse = await fetch(
              'https://uet-backend.herokuapp.com/parents/me',
              {
                method: 'GET',
                headers: {
                  Authorization: 'Bearer '.concat(token),
                  'content-type': 'application/json; charset=UTF-8',
                },
              }
            );

            var currentParentsJson = await currentParentsResponse.json();
            var {currentParents} = currentParentsJson;

            if (currentUser && currentParents) {
              setUser({
                ...currentUser,
                isManager: ['specialist', 'student-affair-leader'].includes(
                  currentUser.role
                )
                  ? true
                  : false,
              });
              setCurrentUserLoading(false);
              setParents(currentParents);
              setCurrentParentsLoading(false);
              history.push('/');
            }
          } catch (err) {
            console.error(err);
            alert(JSON.stringify(err));
          }
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleKeyDown = (event) => {
    var charCode = typeof event.which == 'number' ? event.which : event.keyCode;
    return charCode === 13 ? handleLogin(event) : '';
  };

  const handleClickAway = () => {
    setError({code: '', password: ''});
  };

  return (
    <div className={classes.root} style={isStudent ? {} : {display: 'none'}}>
      <Grid container justify="center" alignItems="center" direction="column">
        <Grid item>
          <img src={uetLogo} alt="uet-logo" className={classes.logo} />
        </Grid>
        <Grid item>
          <Typography variant="overline" display="block" gutterBottom>
            Login as Student
          </Typography>
        </Grid>

        <Grid item>
          <ClickAwayListener onClickAway={handleClickAway}>
            <TextField
              error={error.code === '' ? false : true}
              className={classes.studentCodeInput}
              type="text"
              variant="outlined"
              label="Student Code"
              required
              name="code"
              helperText={error.code === '' ? null : error.code}
              fullWidth
              onChange={(event) => {
                setError({...error, code: ''});
                setAccount({...account, code: event.target.value});
              }}
              onKeyDown={handleKeyDown}
            />
          </ClickAwayListener>
        </Grid>

        <Grid item>
          <ClickAwayListener onClickAway={handleClickAway}>
            <TextField
              error={error.password === '' ? false : true}
              className={classes.passwordInput}
              type={visiblePassword ? 'text' : 'password'}
              variant="outlined"
              label="Password"
              required
              name="password"
              helperText={error.password === '' ? null : error.password}
              onChange={(event) => {
                setError({...error, password: ''});
                setAccount({...account, password: event.target.value});
              }}
              onKeyDown={handleKeyDown}
            />
          </ClickAwayListener>
        </Grid>

        <Grid item>
          <IconButton
            className={classes.visiblePasswordButton}
            onClick={() => {
              setVisiblePassword(!visiblePassword);
            }}
          >
            {visiblePassword ? (
              <VisibilityOffIcon color="action" fontSize="small" />
            ) : (
              <VisibilityIcon color="action" fontSize="small" />
            )}
          </IconButton>
        </Grid>

        <Grid item>
          <Button
            color="primary"
            type="submit"
            className={classes.loginButton}
            variant="contained"
            onClick={handleLogin}
          >
            Login
          </Button>
        </Grid>
        <Grid item>
          <Grid
            container
            direction="row"
            justify="center"
            spacing={10}
            alignItems="center"
            className={classes.MuiItem}
          >
            <Grid item>
              <Link href="#"> {'forget password?'}</Link>
            </Grid>
            <Grid item>
              <Link
                href="#"
                onClick={() => {
                  setStudent(false);
                }}
              >
                {'i am not a student'}
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>{' '}
      <LoadingModal loading={sending} setLoading={setSending} />
    </div>
  );
};

export default withRouter(Student);
