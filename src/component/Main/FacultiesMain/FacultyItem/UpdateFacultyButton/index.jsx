import React, {useState, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Backdrop,
  Fade,
  IconButton,
  Modal,
  CircularProgress,
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import {NoneStudentListContext} from '../../../../../providers/NoneStudentListContextProvider';
import {CurrentUserContext} from '../../../../../providers/CurrentUserContextProvider';
import ModalContent from './ModalContent';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

const Index = ({faculty}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  var {noneStudentList, getNoneStudentListLoading} = useContext(
    NoneStudentListContext
  );

  var {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <>
      <IconButton
        style={{display: currentUser.isManager ? 'flex' : 'none'}}
        onClick={() => {
          setOpen(true);
        }}
      >
        <EditIcon />
      </IconButton>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            {getNoneStudentListLoading ? (
              <CircularProgress size={160} />
            ) : (
              <ModalContent
                faculty={faculty}
                noneStudentList={noneStudentList}
              />
            )}
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default Index;
