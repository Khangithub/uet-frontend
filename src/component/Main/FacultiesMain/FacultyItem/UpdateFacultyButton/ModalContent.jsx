import React, {useState, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Button,
  FormControl,
  InputLabel,
  Select,
  TextField,
  MenuItem,
  Avatar,
} from '@material-ui/core';
import {toCapitalize} from '../../../../../utils/string';
import ResponseModal from '../../../../Common/ResponseModal';
import LoadingModal from '../../../../Common/LoadingModal';
import {FacultyContext} from '../../../../../providers/FacultyContextProvider';

import Cookies from 'universal-cookie';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const ModalContent = ({faculty, noneStudentList}) => {
  const classes = useStyles();
  const {setFacultyList} = useContext(FacultyContext);

  const [newFaculty, setNewFaculty] = useState({
    dean: faculty.dean._id,
    code: faculty.code,
    facultyName: faculty.facultyName,
    startYear: faculty.startYear,
  });

  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const cookies = new Cookies();
  const token = cookies.get('token');

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    var {dean, code, facultyName, startYear} = newFaculty;

    fetch(`https://uet-backend.herokuapp.com/faculties/${faculty.code}`, {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify({dean, newCode: code, facultyName, startYear}),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var facultyListResponse = await fetch(
            'https://uet-backend.herokuapp.com/faculties',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8',
              },
            }
          );
          var facultyListJson = await facultyListResponse.json();

          if (facultyListJson.docs) {
            setLoading(false);
            setFacultyList(facultyListJson.docs);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          console.error('UpdateFacultyButton', err);
          setLoading(false);
        }
      })
      .catch((err) => {
        console.error('UpdateFacultyButton', err);
        setLoading(false);
      });
  };

  return (
    <div className={classes.root}>
      <form enctype="multipart/form-data">
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="text"
          label="Faculty Name"
          name="facultyName"
          fullWidth
          defaultValue={newFaculty.facultyName}
          onChange={(event) =>
            setNewFaculty({...newFaculty, facultyName: event.target.value})
          }
        />
        <br />
        <TextField
          label="Code"
          name="code"
          defaultValue={newFaculty.code}
          fullWidth
          onChange={(event) =>
            setNewFaculty({...newFaculty, code: event.target.value})
          }
        />
        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="dean-select">Dean</InputLabel>
          <Select
            labelId="dean-select"
            defaultValue={newFaculty.dean}
            onChange={(event) =>
              setNewFaculty({...newFaculty, dean: event.target.value})
            }
          >
            {noneStudentList
              .filter((staff) => {
                return ['uet-leader', 'academic-leader'].includes(staff.role);
              })
              .map((staff, index) => {
                const {_id, fullname, role, profileImage} = staff;
                return (
                  <MenuItem key={index} value={_id}>
                    <Avatar
                      alt={fullname}
                      src={profileImage}
                      style={{marginRight: '10px'}}
                    />
                    <span>{toCapitalize(fullname)}</span> &nbsp; |&nbsp;
                    <small>{role}</small>
                  </MenuItem>
                );
              })}
          </Select>
        </FormControl>

        <br />
        <TextField
          type="number"
          label="Start Year"
          name="startYear"
          defaultValue={newFaculty.startYear}
          fullWidth
          onChange={(event) =>
            setNewFaculty({...newFaculty, startYear: event.target.value})
          }
          style={{marginBottom: '40px'}}
        />

        <br />
        <Button
          color="primary"
          variant="contained"
          fullWidth
          onClick={handleSubmit}
          type="submit"
        >
          Add Faculty
        </Button>
      </form>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </div>
  );
};

export default ModalContent;
