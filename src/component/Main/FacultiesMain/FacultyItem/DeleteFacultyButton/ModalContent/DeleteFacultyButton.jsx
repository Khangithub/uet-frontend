import React, {useState, useContext} from 'react';
import {ClassListContext} from '../../../../../../providers/ClassListContextProvider';
import {FacultyContext} from '../../../../../../providers/FacultyContextProvider';
import LoadingModal from '../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../Common/ResponseModal';
import Cookies from 'universal-cookie';

import {Button, CircularProgress} from '@material-ui/core';

const DeleteFacultyButton = ({faculty}) => {
  const {classList, getClassListLoading} = useContext(ClassListContext);
  const {
    setFacultyList,
    getFacultyListLoading,
    setFacultyListLoading,
  } = useContext(FacultyContext);
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [open, setOpen] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');

  const handleSubmit = () => {
    setFacultyListLoading(true);

    fetch(`https://uet-backend.herokuapp.com/faculties/${faculty.code}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer '.concat(token),
      },
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var facultyListResponse = await fetch(
            'https://uet-backend.herokuapp.com/faculties/',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8',
              },
            }
          );
          var facultyListJson = await facultyListResponse.json();

          if (facultyListJson.docs.length > 1) {
            setFacultyListLoading(false);
            setFacultyList(facultyListJson.docs);
            setResponseJSON(json);
            setOpen(true);
          }
        } catch (err) {
          console.error('DeleteFacultyButton', err);
          setFacultyListLoading(false);
          alert(JSON.stringify(err));
        }
      })
      .catch((err) => {
        console.error('DeleteFacultyButton', err);
        setFacultyListLoading(false);
        alert(JSON.stringify(err));
      });
  };

  return getClassListLoading ? (
    <CircularProgress size={160} />
  ) : (
    <>
      <Button
        color="secondary"
        variant="contained"
        fullWidth
        style={{margin: '10px 0px'}}
        type="submit"
        disabled={
          classList.filter((classListItem) => {
            return classListItem.faculty.code === faculty.code;
          }).length > 0
            ? true
            : false
        }
        onClick={handleSubmit}
      >
        Delete User {faculty.facultyName} ({faculty.code})
      </Button>

      <LoadingModal
        loading={getFacultyListLoading}
        setLoading={setFacultyListLoading}
      />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default DeleteFacultyButton;
