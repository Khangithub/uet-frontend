import React from 'react';
import {Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

import DeleteFacultyButton from './DeleteFacultyButton';
import CancelButton from './CancelButton';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const Index = ({faculty, setOpen}) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <form>
        <Typography variant="h6" gutterBottom>
          <strong>
            Are you sure you want to delete faculty {faculty.facultyName} (
            {faculty.code}) ?
          </strong>
        </Typography>

        <DeleteFacultyButton faculty={faculty} />
        <CancelButton setOpen={setOpen} />
      </form>
    </div>
  );
};

export default Index;
