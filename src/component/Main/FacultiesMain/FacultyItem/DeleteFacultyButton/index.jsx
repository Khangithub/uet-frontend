import React, {useState, useContext} from 'react';

import {CurrentUserContext} from '../../../../../providers/CurrentUserContextProvider';

import {makeStyles} from '@material-ui/core/styles';
import {
  Backdrop,
  CircularProgress,
  Fade,
  IconButton,
  Modal,
} from '@material-ui/core';

import ModalContent from './ModalContent/';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

const Index = ({faculty}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <>
      <IconButton
        style={{display: currentUser.isManager ? 'flex' : 'none'}}
        onClick={() => {
          setOpen(true);
        }}
      >
        <DeleteIcon />
      </IconButton>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <ModalContent faculty={faculty} setOpen={setOpen} />
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default Index;
