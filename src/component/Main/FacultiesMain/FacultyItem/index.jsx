import React, {useState, useEffect, useContext} from 'react';
import {Grid, Paper, Avatar} from '@material-ui/core';
import {FacultyContext} from '../../../../providers/FacultyContextProvider';
import {CurrentUserContext} from '../../../../providers/CurrentUserContextProvider';

import LoadingModal from '../../../Common/LoadingModal';
import UpdateFacultyButton from './UpdateFacultyButton/';
import DeleteFacultyButton from './DeleteFacultyButton/';
import {withRouter} from 'react-router-dom';

const Index = ({history, location, match}) => {
  const {innerWidth} = window;
  var {facultyList, getFacultyListLoading, setFacultyListLoading} = useContext(
    FacultyContext
  );
  var {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  let [screenWidth, setScreenWidth] = useState(innerWidth);

  useEffect(() => {
    window.onresize = () => {
      setScreenWidth(window.innerWidth);
    };
  }, [innerWidth]);

  return getFacultyListLoading || getCurrentUserLoading ? (
    <LoadingModal
      loading={getFacultyListLoading}
      setLoading={setFacultyListLoading}
    />
  ) : (
    <Grid
      container
      spacing={2}
      direction="column"
      justify="flex-start"
      style={{width: `${(screenWidth * 80) / 100}px`}}
    >
      {facultyList
        .filter((faculty) => {
          if (['uet-leader', 'academic-leader'].includes(currentUser.role)) {
            return faculty.dean.code === currentUser.code;
          }
          return faculty;
        })
        .map((faculty, index) => {
          const {dean, code, facultyName, startYear} = faculty;

          return (
            <Grid container spacing={3} style={{margin: '2px 0px'}} key={index}>
              <Grid
                item
                xs={currentUser.isManager ? 10 : 12}
                onClick={() => history.push(`/faculty/${code}/classes`)}
              >
                <Paper
                  component="div"
                  elevation={8}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <div>
                    <Avatar alt={dean.profileImage} src={dean.profileImage} />
                  </div>

                  <div>
                    <strong>{facultyName}</strong>
                  </div>

                  <div>
                    <em>{code}</em>
                  </div>

                  <div>
                    <small>since {startYear}</small>
                  </div>
                </Paper>
              </Grid>
              <Grid item xs={currentUser.isManager ? 2 : 0}>
                <Paper
                  component="div"
                  elevation={0}
                  style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    height: '65px',
                    alignItems: 'center',
                  }}
                >
                  <UpdateFacultyButton faculty={faculty} />
                  <DeleteFacultyButton faculty={faculty} />
                </Paper>
              </Grid>
            </Grid>
          );
        })}
    </Grid>
  );
};

export default withRouter(Index);
