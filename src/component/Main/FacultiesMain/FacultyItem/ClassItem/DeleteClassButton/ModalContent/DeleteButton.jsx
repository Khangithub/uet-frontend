import React, {useState, useContext} from 'react';
import {Button} from '@material-ui/core';
import LoadingModal from '../../../../../Common/LoadingModal';
import ResponseModal from '../../../../../Common/ResponseModal';
import {ClassListContext} from '../../../../../../providers/ClassListContextProvider';
import {formatClassname} from '../../../../../../utils/string';

const DeleteButton = ({classname}) => {
  const {setClassList} = useContext(ClassListContext);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);

    fetch(`https://uet-backend.herokuapp.com/classes/${classname}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
      },
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
          var classListJson = await classListResponse.json();

          if (classListJson.docs) {
            setLoading(false);
            setClassList(classListJson.docs);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          console.error('DeleteClassButton', err);
          setLoading(false);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      <Button
        color="secondary"
        variant="contained"
        fullWidth
        style={{margin: '10px 0px'}}
        type="submit"
        onClick={handleSubmit}
      >
        Delete Class {formatClassname(classname)}
      </Button>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default DeleteButton;
