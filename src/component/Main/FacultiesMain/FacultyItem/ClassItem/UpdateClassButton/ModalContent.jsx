import React, {useEffect, useState, useContext} from 'react';
import {
  Avatar,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@material-ui/core';
import {formatClassname, toCapitalize} from '../../../../../utils/string';
import LoadingModal from '../../../../Common/LoadingModal';
import ResponseModal from '../../../../Common/ResponseModal';
import {ClassListContext} from '../../../../../providers/ClassListContextProvider';

// classname: String
// selectedConsultant: String, ObjectID
const ModalContent = ({classname, selectedConsultant}) => {
  const [consultantList, setConsultantList] = useState([]);
  const {setClassList} = useContext(ClassListContext);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isError, setError] = useState(false);
  const [responseJSON, setResponseJSON] = useState('');
  const [newClass, setNewClass] = useState({
    newClassname: classname,
    consultant: selectedConsultant,
  });

  useEffect(() => {
    fetch('https://uet-backend.herokuapp.com/consultants/')
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        var {docs} = json;
        setLoading(false);
        setConsultantList(docs);
      })
      .catch((err) => {
        setLoading(false);
        console.error(err);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    var {newClassname, consultant} = newClass;

    fetch(`https://uet-backend.herokuapp.com/classes/${classname}`, {
      method: 'PATCH',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({newClassname, consultant}),
    })
      .then((response) => {
        setError(response.status === 200 ? false : true);
        return response.json();
      })
      .then(async (json) => {
        try {
          var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
          var classListJson = await classListResponse.json();

          if (classListJson.docs) {
            setLoading(false);
            setClassList(classListJson.docs);
            setOpen(true);
            setResponseJSON(json);
          }
        } catch (err) {
          console.error('DeleteClassButton', err);
          setLoading(false);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return loading ? (
    <LoadingModal loading={loading} setLoading={setLoading} />
  ) : (
    <>
      <form>
        <img
          src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
          alt=""
          style={{height: '65px'}}
        />
        <TextField
          type="text"
          label="Classname"
          name="classname"
          fullWidth
          defaultValue={formatClassname(classname)}
          onChange={(event) =>
            setNewClass({...newClass, newClassname: event.target.value})
          }
        />
        <br />

        <FormControl style={{minWidth: 400}}>
          <InputLabel id="consultant-select">Consultant</InputLabel>
          <Select
            labelId="consultant-select"
            onChange={(event) =>
              setNewClass({...newClass, consultant: event.target.value})
            }
          >
            {consultantList.map((item, index) => {
              const {_id, fullname, academicRank, profileImage} = item;
              return (
                <MenuItem key={index} value={_id}>
                  <Avatar
                    alt={fullname}
                    src={profileImage}
                    style={{marginRight: '10px'}}
                  />
                  {toCapitalize(academicRank)}. {fullname}
                </MenuItem>
              );
            })}
          </Select>
          <br />
          <Button
            color="secondary"
            variant="contained"
            fullWidth
            type="submit"
            onClick={handleSubmit}
          >
            Update Class {formatClassname(classname)}
          </Button>
        </FormControl>
      </form>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={open}
        setOpen={setOpen}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  );
};

export default ModalContent;
