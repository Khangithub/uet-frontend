import React, {useContext} from 'react';
import {
  Button,
  Modal,
  Backdrop,
  Fade,
  CircularProgress,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import ModalContent from './ModalContent';
import {NoneStudentListContext} from '../../../../providers/NoneStudentListContextProvider';
import {CurrentUserContext} from '../../../../providers/CurrentUserContextProvider';
import LoadingModal from '../../../Common/LoadingModal';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
  },
}));

const Index = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  var {
    noneStudentList,
    getNoneStudentListLoading,
    setNoneStudentListLoading,
  } = useContext(NoneStudentListContext);

  var {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <>
      <Button
        variant="contained"
        color="primary"
        onClick={handleOpen}
        style={{display: currentUser.isManager ? 'flex' : 'none'}}
      >
        Add Faculty
      </Button>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            {getNoneStudentListLoading ? (
              <LoadingModal
                loading={getNoneStudentListLoading}
                setLoading={setNoneStudentListLoading}
              />
            ) : (
              <ModalContent noneStudentList={noneStudentList} />
            )}
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default Index;
