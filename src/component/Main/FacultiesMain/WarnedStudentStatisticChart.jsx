import React, {useEffect, useState, useContext} from 'react';
import {ClassListContext} from '../../../providers/ClassListContextProvider';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';

import {groupClassesSameFaculty} from '../../Main/ClassMain/utils/array';
import Chart from 'react-google-charts';
import RandomColor from 'randomcolor';
import LoadingModal from '../../Common/LoadingModal';

const WarnedStudentStatisticChart = () => {
  const {classList, getClassListLoading} = useContext(ClassListContext);
  var {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const statisticClassList = groupClassesSameFaculty(classList);

  const {innerWidth} = window;
  let [screenWidth, setScreenWidth] = useState(innerWidth);

  useEffect(() => {
    window.onresize = () => {
      setScreenWidth(window.innerWidth);
    };
  }, [innerWidth]);

  var data;

  if (!currentUser.isManager) {
    data = [
      ['FacultyName', 'totalClassSize', 'totalWarnedLength', {role: 'style'}],
    ].concat(
      statisticClassList
        .filter((classListItem) => {
          return classListItem.faculty.dean.code === currentUser.code;
        })
        .map((classListItem) => {
          const {totalClassSize, totalWarnedLength, faculty} = classListItem;
          const {code} = faculty;
          return [
            `${faculty.facultyName} (code: ${code})`,
            totalClassSize,
            totalWarnedLength,
            RandomColor(),
          ];
        })
    );
  } else {
    data = [
      ['FacultyName', 'totalClassSize', 'totalWarnedLength', {role: 'style'}],
    ].concat(
      statisticClassList.map((classListItem) => {
        const {totalClassSize, totalWarnedLength, faculty} = classListItem;
        const {code} = faculty;
        return [
          `${faculty.facultyName} (code: ${code})`,
          totalClassSize,
          totalWarnedLength,
          RandomColor(),
        ];
      })
    );
  }

  return getClassListLoading || getCurrentUserLoading ? (
    <LoadingModal loading={getClassListLoading} setLoading={null} />
  ) : (
    <Chart
      style={{display: data.length > 1 ? 'flex' : 'none'}}
      chartType="ColumnChart"
      width={(screenWidth * 80) / 100}
      height="400px"
      data={data}
    />
  );
};

export default WarnedStudentStatisticChart;
