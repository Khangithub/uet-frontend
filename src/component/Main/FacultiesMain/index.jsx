import React from 'react';
import {Grid} from '@material-ui/core';
import AddFacultyButton from './AddFacultyButton/';
import FacultyItem from './FacultyItem/';
import WarnedStudentStatisticChart from './WarnedStudentStatisticChart';

const Index = () => {
  return (
    <div>
      <Grid container spacing={3} direction="column">
        <Grid item>
          <WarnedStudentStatisticChart />
        </Grid>
        <Grid item>
          <AddFacultyButton />
        </Grid>
        <Grid item>
          <FacultyItem />
        </Grid>
      </Grid>
    </div>
  );
};

export default Index;
