import React, {useState} from 'react';
import Cookies from 'universal-cookie';
import {
  IconButton,
  TextField,
  Grid,
  Snackbar,
  ClickAwayListener,
} from '@material-ui/core';

import MuiAlert from '@material-ui/lab/Alert';

import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import {makeStyles} from '@material-ui/core';

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const useStyles = makeStyles((theme) => ({
  MuiInputBaseInput: {
    '& .MuiInputBase-input': {
      minWidth: '300px',
    },
  },
  customePositionIconButton: {
    position: 'relative',
    top: '-50px',
    right: '-130px',
  },
}));

const ChangePassword = () => {
  const classes = useStyles();
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [confirmPasswordVisible, setConfirmPasswordVisible] = useState(false);
  const [error, setError] = useState('');
  const [open, setOpen] = useState(false);

  const [password, setPassword] = useState({
    newPass: '',
    confirmPass: '',
  });

  const cookies = new Cookies();
  const token = cookies.get('token');

  const handleSubmit = (event) => {
    var charCode = typeof event.which == 'number' ? event.which : event.keyCode;
    var {newPass, confirmPass} = password;
    if (charCode === 13) {
      if (confirmPass === '' || password === '') {
        return setError('Emptied Password');
      }

      if (newPass.trim() === confirmPass.trim()) {
        fetch('https://uet-backend.herokuapp.com/users/me', {
          method: 'PATCH',
          headers: {
            Authorization: 'Bearer '.concat(token),
            'content-type': 'application/json; charset=UTF-8',
          },
          body: JSON.stringify({newPass}),
        })
          .then((response) => {
            return response.json();
          })
          .then((json) => {
            var {doc} = json;
            if (doc) {
              setPassword({
                newPass: '',
                confirmPass: '',
              });
              setOpen(true);
            }
          })
          .catch((error) => {
            console.error(error);
          });
      } else {
        setError('Passwords Not Matched');
      }
    }
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  const handleClickAway = () => {
    setError('');
  };

  return (
    <>
      <ClickAwayListener onClickAway={handleClickAway}>
        <form noValidate autoComplete="off">
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item className={classes.MuiInputBaseInput}>
              <TextField
                type={passwordVisible ? 'text' : 'password'}
                variant="outlined"
                label="New Password"
                required
                onChange={(event) => {
                  setError('');
                  setPassword({...password, newPass: event.target.value});
                }}
                onKeyDown={handleSubmit}
                error={error === '' ? false : true}
                helperText={error}
              />
            </Grid>
            <Grid item>
              <IconButton
                className={classes.customePositionIconButton}
                onClick={() => {
                  setPasswordVisible(!passwordVisible);
                }}
              >
                {passwordVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
              </IconButton>
            </Grid>

            <Grid item className={classes.MuiInputBaseInput}>
              <TextField
                type={confirmPasswordVisible ? 'text' : 'password'}
                variant="outlined"
                label="Confirm Password"
                required
                onChange={(event) => {
                  setError('');
                  setPassword({...password, confirmPass: event.target.value});
                }}
                onKeyDown={handleSubmit}
                error={error === '' ? false : true}
                helperText={error}
              />
            </Grid>

            <Grid item>
              <IconButton
                className={classes.customePositionIconButton}
                onClick={() => {
                  setConfirmPasswordVisible(!confirmPasswordVisible);
                }}
              >
                {confirmPasswordVisible ? (
                  <VisibilityOffIcon />
                ) : (
                  <VisibilityIcon />
                )}
              </IconButton>
            </Grid>
          </Grid>
        </form>
      </ClickAwayListener>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Password successfully changed
        </Alert>
      </Snackbar>
    </>
  );
};

export default ChangePassword;
