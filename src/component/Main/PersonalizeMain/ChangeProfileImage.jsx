import React, {useState, useContext} from 'react';
import Cookies from 'universal-cookie';
import {IconButton, Grid} from '@material-ui/core';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';

import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import {storage} from '../../../firebase';

const ChangeProfileImage = ({profileImage}) => {
  const cookies = new Cookies();
  const token = cookies.get('token');
  const {setUser, setCurrentUserLoading} = useContext(CurrentUserContext);
  var [progress, setProgress] = useState(0);

  const handleChange = (event) => {
    var image = event.target.files[0];

    if (image) {
      const uploadTask = storage.ref(`images/${image.name}`).put(image);

      uploadTask.on(
        'state_changed',
        (snapshot) => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        (error) => {
          console.log(error);
        },
        () => {
          storage
            .ref('images')
            .child(image.name)
            .getDownloadURL()
            .then((url) => {
              fetch('https://uet-backend.herokuapp.com/users/me', {
                method: 'PATCH',
                headers: {
                  Authorization: 'Bearer '.concat(token),
                  'content-type': 'application/json; charset=UTF-8',
                },
                body: JSON.stringify({profileImage: url}),
              })
                .then((response) => {
                  return response.json();
                })
                .then((json) => {
                  var {doc} = json;
                  if (doc.profileImage) {
                    fetch('https://uet-backend.herokuapp.com/users/me', {
                      method: 'GET',
                      headers: {
                        Authorization: 'Bearer '.concat(token),
                        'content-type': 'application/json; charset=UTF-8',
                      },
                    })
                      .then((response) => {
                        return response.json();
                      })
                      .then((json) => {
                        var {currentUser} = json;

                        if (currentUser) {
                          setUser({
                            ...currentUser,
                            isManager: [
                              'specialist',
                              'student-affair-leader',
                            ].includes(currentUser.role)
                              ? true
                              : false,
                          });
                          setCurrentUserLoading(false);
                        }
                      })
                      .catch((err) => {
                        console.log('CurrentUserContextProvider', err);
                        setCurrentUserLoading(false);
                      });
                  }
                })
                .catch((error) => {
                  console.error(error);
                });
            });
        }
      );
    }
  };

  return (
    <Grid container direction="column" justify="center" alignItems="center">
      <Grid item>
        <img
          src={profileImage}
          alt="currentUser ProfileImage"
          style={{
            height: '270px',
            width: '270px',
            borderRadius: '50%',
          }}
        />
      </Grid>

      <Grid item>
        <input
          accept="image/*"
          style={{display: 'none'}}
          id="profileImage"
          type="file"
          onChange={handleChange}
        />
        <label htmlFor="profileImage">
          <IconButton
            style={{position: 'relative', top: '-80px', right: '-100px'}}
            aria-label="upload picture"
            component="span" // nhất định phải có cái này
          >
            <PhotoCameraIcon fontSize="large" />
          </IconButton>
        </label>
      </Grid>
      <Grid item>
        <progress
          value={progress}
          max="100"
          style={{position: 'relative', top: '-120px'}}
        />
      </Grid>
    </Grid>
  );
};

export default ChangeProfileImage;
