import React, {useContext, useEffect, useState} from 'react';
import {FacultyContext} from '../../../providers/FacultyContextProvider';
import {Grid, Paper, Avatar} from '@material-ui/core';
import UserCard from '../../Common/UserCard';
import {withRouter} from 'react-router-dom';
import LoadingModal from '../../Common/LoadingModal';

const AcacdemicLeaderMain = ({user, history, location, match}) => {
  const {facultyList, getFacultyListLoading, setFacultyListLoading} = useContext(FacultyContext);
  const {innerWidth} = window;
  let [screenWidth, setScreenWidth] = useState(innerWidth);

  useEffect(() => {
    window.onresize = () => {
      setScreenWidth(window.innerWidth);
    };
  }, [innerWidth]);

  return (
    <div>
      <Grid container alignItems="center" justify="center">
        <Grid item xs={4}>
          <UserCard user={user} />
        </Grid>
        <Grid item xs={8}>
          {getFacultyListLoading ? (
            <LoadingModal loading={getFacultyListLoading} setLoading={setFacultyListLoading} />
          ) : (
            <Grid
              container
              spacing={2}
              direction="column"
              justify="flex-start"
              style={{width: `${screenWidth * 0.6}px`}}
            >
              {facultyList
                .filter((faculty) => {
                  return faculty.dean.code === user.code;
                })
                .map((faculty, index) => {
                  const {dean, code, facultyName, startYear} = faculty;

                  return (
                    <Grid
                      item
                      xs={12}
                      onClick={() => history.push(`/faculty/${code}/classes`)}
                    >
                      <Paper
                        component="div"
                        elevation={8}
                        style={{
                          display: 'flex',
                          justifyContent: 'space-evenly',
                          height: '65px',
                          alignItems: 'center',
                        }}
                      >
                        <div>
                          <Avatar
                            alt={dean.profileImage}
                            src={dean.profileImage}
                          />
                        </div>

                        <div>
                          <strong>{facultyName}</strong>
                        </div>

                        <div>
                          <em>{code}</em>
                        </div>

                        <div>
                          <small>since {startYear}</small>
                        </div>
                      </Paper>
                    </Grid>
                  );
                })}
            </Grid>
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default withRouter(AcacdemicLeaderMain);
