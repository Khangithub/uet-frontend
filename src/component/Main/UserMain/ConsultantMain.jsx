import React, {useContext, useEffect, useState} from 'react';
import {ClassListContext} from '../../../providers/ClassListContextProvider';
import {Grid, Paper, Avatar} from '@material-ui/core';
import UserCard from '../../Common/UserCard';
import {withRouter} from 'react-router-dom';
import {formatClassname, formatAcademicRank} from '../../../utils/string';
import LoadingModal from '../../Common/LoadingModal';

const ConsultantMail = ({user, history, location, match}) => {
  const {classList, getClassListLoading, setClassListLoading} = useContext(ClassListContext);
  const {innerWidth} = window;
  let [screenWidth, setScreenWidth] = useState(innerWidth);

  useEffect(() => {
    window.onresize = () => {
      setScreenWidth(window.innerWidth);
    };
  }, [innerWidth]);

  return (
    <div>
      <Grid container alignItems="center" justify="center">
        <Grid item xs={4}>
          <UserCard user={user} />
        </Grid>
        <Grid item xs={8}>
          {getClassListLoading ? (
            <LoadingModal loading={getClassListLoading} setLoading={setClassListLoading} />
          ) : classList.length === 0 ? (
            'không có lớp học'
          ) : (
            <Grid
              container
              spacing={3}
              justify="center"
              alignItems="center"
              style={{margin: '2px 0px', width: `${screenWidth * 0.6}px`}}
            >
              {classList
                .filter((classListItem) => {
                  return classListItem.consultant.code === user.code;
                })
                .map((classListItem, index) => {
                  const {consultant, faculty, classname} = classListItem;
                  const {facultyName} = faculty;
                  const {profileImage, fullname, academicRank} = consultant;

                  return (
                    <Grid
                      item
                      xs={10}
                      key={index}
                      onClick={() => {
                        history.push(`/class/${classname}`);
                      }}
                    >
                      <Paper
                        component="div"
                        elevation={8}
                        style={{
                          display: 'flex',
                          justifyContent: 'space-evenly',
                          height: '65px',
                          alignItems: 'center',
                        }}
                      >
                        <div>
                          <strong>{formatClassname(classname)}</strong>
                        </div>

                        <div>
                          <Avatar alt={fullname} src={profileImage} />
                        </div>

                        <div>
                          <em>{`${formatAcademicRank(
                            academicRank
                          )}. ${fullname}`}</em>
                        </div>

                        <div>|</div>

                        <div>{facultyName}</div>
                      </Paper>
                    </Grid>
                  );
                })}
            </Grid>
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default withRouter(ConsultantMail);
