import React from 'react';
import {Grid} from '@material-ui/core';
import UserCard from '../../Common/UserCard';

const SpecialistMain = ({user}) => {
  return (
    <Grid container justify="center" alignItems="center">
      <Grid item>
        <UserCard user={user} />
      </Grid>
    </Grid>
  );
};

export default SpecialistMain;
