import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
	Accordion,
	AccordionSummary,
	Table,
	TableCell,
	TableRow,
	TableContainer,
	Paper,
	TableHead,
	TableBody,
	Typography,
	AccordionDetails,
} from '@material-ui/core';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ccd3ff',
		color: theme.palette.common.black,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

const useStyles = makeStyles({
	accordionContainer: {
		width: '100%',
		marginTop: '10px',
	},
	table: {
		minWidth: 700,
	},
});

const TrainingScoreTable = ({ accademicTrainningList }) => {
	const classes = useStyles();

	return (
		<div className={classes.accordionContainer}>
			<Accordion defaultExpanded={true}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="overline" display="block" gutterBottom>
						Training Score List
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<TableContainer component={Paper}>
						<Table className={classes.table}>
							<TableHead>
								<TableRow>
									<StyledTableCell align="center">Index</StyledTableCell>
									<StyledTableCell align="center">School Year</StyledTableCell>
									<StyledTableCell align="center">Semester</StyledTableCell>
									<StyledTableCell align="center">Training Score</StyledTableCell>
									<StyledTableCell align="center">Training Score Classification</StyledTableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{accademicTrainningList.map((student, index) => {
									var { schoolYear, semester, score, classification } = student;
									return (
										<StyledTableRow key={index}>
											<StyledTableCell align="center">{index + 1}</StyledTableCell>
											<StyledTableCell align="center">{schoolYear}</StyledTableCell>
											<StyledTableCell align="center">{semester}</StyledTableCell>
											<StyledTableCell align="center">{score}</StyledTableCell>
											<StyledTableCell align="center">{classification}</StyledTableCell>
										</StyledTableRow>
									);
								})}
							</TableBody>
						</Table>
					</TableContainer>
				</AccordionDetails>
			</Accordion>
		</div>
	);
};

export default TrainingScoreTable;
