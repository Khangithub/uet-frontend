import React from 'react';
import {
  Grid,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import EditParentsInfo from './EditParentsInfo/';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: '30px',
  },
}));

const Index = ({currentParents, code, isCurrentUser}) => {
  const classes = useStyles();
  const {
    dadAvatar,
    dadFullname,
    dadPhonenumber,
    dadJob,
    dadHometown,
    dadEmail,
    momAvatar,
    momFullname,
    momPhonenumber,
    momJob,
    momHometown,
    momEmail,
    parentPermanentAddress,
  } = currentParents;
  return (
    <div className={classes.root}>
      <Accordion defaultExpanded={true}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography variant="overline" display="block" gutterBottom>
            Parents Information
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container justify="center" alignItems="center" spacing={6}>
            <Grid item xs={12} sm={6}>
              <Grid container justify="space-between" alignItems="center">
                <Grid item>
                  <img
                    src={dadAvatar}
                    alt="dadAvatar"
                    style={{
                      width: '120px',
                      height: '160px',
                      borderRadius: '50%',
                    }}
                  />
                </Grid>
                <Grid item>
                  <Grid container direction="column">
                    <Grid item>
                      <strong>Fullname of Dad:</strong>
                      &nbsp;{dadFullname}
                    </Grid>
                    <Grid item>
                      <strong>Phonenumber of Dad:</strong>
                      &nbsp;{dadPhonenumber}
                    </Grid>
                    <Grid item>
                      <strong>Job of Dad:</strong>
                      &nbsp;{dadJob}
                    </Grid>
                    <Grid item>
                      <strong>Hometown of Dad:</strong>
                      &nbsp;{dadHometown}
                    </Grid>
                    <Grid item>
                      <strong>Email of Dad:</strong>
                      &nbsp;{dadEmail}
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Grid container justify="space-between" alignItems="center">
                <Grid item>
                  <img
                    src={momAvatar}
                    alt="momAvatar"
                    style={{
                      width: '120px',
                      height: '160px',
                      borderRadius: '50%',
                    }}
                  />
                </Grid>
                <Grid item>
                  <Grid container direction="column">
                    <Grid item>
                      <strong>Fullname of Mom:</strong>
                      &nbsp;{momFullname}
                    </Grid>
                    <Grid item>
                      <strong>Phonenumber of Mom:</strong>
                      &nbsp;{momPhonenumber}
                    </Grid>
                    <Grid item>
                      <strong>Job of Mom:</strong>
                      &nbsp;{momJob}
                    </Grid>
                    <Grid item>
                      <strong>Hometown of Mom:</strong>
                      &nbsp;{momHometown}
                    </Grid>
                    <Grid item>
                      <strong>Email of Mom:</strong>
                      &nbsp;{momEmail}
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Grid container alignItems="center" justify="space-between">
                <Grid item>
                  <strong>Permanent Address of Parents:</strong>
                  &nbsp;{parentPermanentAddress}
                </Grid>
                <Grid item style={{display: isCurrentUser ? 'flex' : 'none'}}>
                  <EditParentsInfo
                    currentParents={currentParents}
                    code={code}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default Index;
