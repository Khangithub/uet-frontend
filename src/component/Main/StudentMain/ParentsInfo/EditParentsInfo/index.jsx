import React, { useState, forwardRef, useContext } from 'react'
import { CurrentParentsContext } from '../../../../../providers/CurrentParentsContextProvider'
import { IconButton, Dialog, AppBar, Toolbar, Slide } from '@material-ui/core'
import { withRouter } from 'react-router-dom'
import LoadingModal from '../../../../Common/LoadingModal'
import ResponseModal from '../../../../Common/ResponseModal'

import Cookies from 'universal-cookie'

import EditIcon from '@material-ui/icons/Edit'
import CloseIcon from '@material-ui/icons/Close'
import DoneIcon from '@material-ui/icons/Done'

import { makeStyles } from '@material-ui/core/styles'
import ModalContent from './ModalContent/'

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3)
  }
}))

const Transition = forwardRef(function Transition (props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

const Index = ({ currentParents, code, history, location, match }) => {
  const [updatedParents, setUpdatedParents] = useState(currentParents)
  const cookies = new Cookies()
  const token = cookies.get('token')
  const classes = useStyles()
  const { setParents } = useContext(CurrentParentsContext)
  const [open, setOpen] = useState(false)
  const [openResponseModal, setOpenResponseModal] = useState(false)
  const [loading, setLoading] = useState(false)
  const [isError, setError] = useState(false)
  const [responseJSON, setResponseJSON] = useState('')

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleUpdate = () => {
    const {
      dadAvatar,
      dadFullname,
      dadPhonenumber,
      dadJob,
      dadHometown,
      dadEmail,
      momAvatar,
      momFullname,
      momPhonenumber,
      momJob,
      momHometown,
      momEmail,
      parentPermanentAddress
    } = updatedParents

    fetch(` https://uet-backend.herokuapp.com/parents/${code}`, {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8'
      },
      body: JSON.stringify({
        dadAvatar,
        dadFullname,
        dadPhonenumber,
        dadJob,
        dadHometown,
        dadEmail,
        momAvatar,
        momFullname,
        momPhonenumber,
        momJob,
        momHometown,
        momEmail,
        parentPermanentAddress
      })
    })
      .then(response => {
        setError(response.status === 200 ? false : true)
        return response.json()
      })
      .then(async json => {
        try {
          var currentParentsResponse = await fetch(
            'https://uet-backend.herokuapp.com/parents/me',
            {
              method: 'GET',
              headers: {
                Authorization: 'Bearer '.concat(token),
                'content-type': 'application/json; charset=UTF-8'
              }
            }
          )

          var currentParentsJson = await currentParentsResponse.json()
          var { currentParents } = currentParentsJson

          if (currentParents) {
            setParents(currentParents)
            setOpenResponseModal(true)
            setResponseJSON(json)
          }
        } catch (err) {
          console.error('EditParentsInfo', err)
          alert(JSON.stringify(err))
        }
      })
      .catch(error => {
        console.error(error)
        alert(JSON.stringify(error))
      })
  }

  return (
    <>
      <IconButton onClick={handleOpen}>
        <EditIcon fontSize='small' />
      </IconButton>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge='start'
              color='inherit'
              onClick={handleClose}
              aria-label='close'
            >
              <CloseIcon />
            </IconButton>

            <IconButton autoFocus color='inherit' onClick={handleUpdate}>
              <DoneIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <ModalContent
          updatedParents={updatedParents}
          setUpdatedParents={setUpdatedParents}
        />
      </Dialog>
      <LoadingModal loading={loading} setLoading={setLoading} />
      <ResponseModal
        open={openResponseModal}
        setOpen={setOpenResponseModal}
        responseJSON={responseJSON}
        isError={isError}
      />
    </>
  )
}

export default withRouter(Index)
