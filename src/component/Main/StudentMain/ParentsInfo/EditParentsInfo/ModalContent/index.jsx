import React from 'react';
import {Grid, TextField} from '@material-ui/core';
import DadAvatar from './DadAvatar';
import MomAvatar from './MomAvatar';

const Index = ({updatedParents, setUpdatedParents}) => {
  return (
    <Grid
      container
      alignItems="center"
      justify="center"
      direction="column"
      style={{width: '100%', minWidth: '300px'}}
    >
      <Grid item style={{marginTop: '50px'}}>
        <h2>Edit Parent Information</h2>
      </Grid>
      <Grid item>
        <Grid container justify="center" alignItems="center" spacing={3}>
          <Grid item>
            <DadAvatar
              dadAvatar={updatedParents.dadAvatar}
              updatedParents={updatedParents}
              setUpdatedParents={setUpdatedParents}
            />
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={3}>
              <Grid item>
                <TextField
                  label="Fullname of Dad"
                  defaultValue={updatedParents.dadFullname}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      dadFullname: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Phonenumber of Dad"
                  type="tel"
                  defaultValue={updatedParents.dadPhonenumber}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      dadPhonenumber: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Job of Dad"
                  defaultValue={updatedParents.dadJob}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      dadJob: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Hometown of Dad"
                  defaultValue={updatedParents.dadHometown}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      dadHometown: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Email of Dad"
                  defaultValue={updatedParents.dadEmail}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      dadEmail: event.target.value,
                    });
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Grid item>
        <Grid container justify="center" alignItems="center" spacing={3}>
          <Grid item>
            <MomAvatar
              momAvatar={updatedParents.momAvatar}
              updatedParents={updatedParents}
              setUpdatedParents={setUpdatedParents}
            />
          </Grid>
          <Grid item>
            <Grid container direction="row" spacing={3}>
              <Grid item>
                <TextField
                  label="Fullname of Mom"
                  defaultValue={updatedParents.momFullname}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      momFullname: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Phonenumber of Mom"
                  type="tel"
                  defaultValue={updatedParents.momPhonenumber}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      momPhonenumber: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Job of Mom"
                  defaultValue={updatedParents.momJob}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      momJob: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Hometown of Mom"
                  defaultValue={updatedParents.momHometown}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      momHometown: event.target.value,
                    });
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Email of Mom"
                  defaultValue={updatedParents.momEmail}
                  onChange={(event) => {
                    return setUpdatedParents({
                      ...updatedParents,
                      momEmail: event.target.value,
                    });
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Index;
