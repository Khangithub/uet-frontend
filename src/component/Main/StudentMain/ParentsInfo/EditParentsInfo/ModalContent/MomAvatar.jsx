import React, {useState} from 'react';
import {Grid, IconButton} from '@material-ui/core';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';

import {storage} from '../../../../../../firebase';

const DadAvatar = ({momAvatar, updatedParents,
  setUpdatedParents}) => {
  var [progress, setProgress] = useState(0);

  const handleChange = (event) => {
    var image = event.target.files[0];

    if (image) {
      const uploadTask = storage.ref(`images/${image.name}`).put(image);

      uploadTask.on(
        'state_changed',
        (snapshot) => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        (error) => {
          console.log(error);
        },
        () => {
          storage
            .ref('images')
            .child(image.name)
            .getDownloadURL()
            .then((url) => {
             setUpdatedParents({...updatedParents, momAvatar: url})
            });
        }
      );
    }
  };

  return (
    <Grid container direction="column" justify="center" alignItems="center">
      <Grid item>
        <img
          src={momAvatar}
          alt="currentUser ProfileImage"
          style={{
            width: '120px',
            height: '160px',
            borderRadius: '50%',
          }}
        />
      </Grid>

      <Grid item>
        <input
          accept="image/*"
          style={{display: 'none'}}
          id="profileImage"
          type="file"
          onChange={handleChange}
        />
        <label htmlFor="profileImage">
          <IconButton
            style={{position: 'relative', top: '-50px', right: '-40px'}}
            aria-label="upload picture"
            component="span" // nhất định phải có cái này
          >
            <PhotoCameraIcon fontSize="small" />
          </IconButton>
        </label>
      </Grid>
      <Grid item>
        <progress
          value={progress}
          max="100"
          style={{position: 'relative', top: '-100px', width: '80px'}}
        />
      </Grid>
    </Grid>
  );
};

export default DadAvatar;
