import React from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Typography, Avatar, IconButton } from '@material-ui/core';

import { convertFullnametoEngLish, formatClassname } from '../../../utils/string';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

// student: Object
const InfoBar = ({ student, history, location, match }) => {
	var { fullname, code, classname, profileImage } = student;

	return (
		<Grid container justify="space-between" alignItems="center">
			<Grid item>
				<IconButton
					onClick={() => {
						history.goBack();
					}}
				>
					<ArrowBackIcon fontSize="large" />
				</IconButton>
			</Grid>
			<Grid item>
				<Typography variant="overline" display="block" gutterBottom>
					{`Fullname: ${fullname} (${convertFullnametoEngLish(fullname)})`}
				</Typography>
			</Grid>
			<Grid item>
				<Typography variant="overline" display="block" gutterBottom>
					Code: {code}
				</Typography>
			</Grid>
			<Grid item>
				<Typography variant="overline" display="block" gutterBottom>
					Class: {formatClassname(classname)}
				</Typography>
			</Grid>
			<Grid item>
				<Avatar src={profileImage} alt="profileImage" />
			</Grid>
		</Grid>
	);
};

export default withRouter(InfoBar);
