import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
	Accordion,
	AccordionSummary,
	Table,
	TableCell,
	TableRow,
	TableContainer,
	Paper,
	TableHead,
	TableBody,
	Typography,
	AccordionDetails,
} from '@material-ui/core';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ccd3ff',
		color: theme.palette.common.black,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

const useStyles = makeStyles({
	accordionContainer: {
		marginTop: '10px',
		width: '100%',
	},
	table: {
		minWidth: 700,
	},
});

// student: Object
const GPATable = ({ student }) => {
	const classes = useStyles();
	var { scoreList, warnings, fCredits } = student;

	return (
		<div className={classes.accordionContainer}>
			<Accordion defaultExpanded={true}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="overline" display="block" gutterBottom>
						GPA Score List
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<TableContainer component={Paper}>
						<Table className={classes.table}>
							<TableHead>
								<TableRow>
									<StyledTableCell align="center">Index</StyledTableCell>
									<StyledTableCell align="center">School Year</StyledTableCell>
									<StyledTableCell align="center">Semester</StyledTableCell>
									<StyledTableCell align="center">GPA Semester</StyledTableCell>
									<StyledTableCell align="center">Total GPA</StyledTableCell>
									<StyledTableCell align="center">GPA Classification</StyledTableCell>
									<StyledTableCell align="center">Credits For Grade F</StyledTableCell>
									<StyledTableCell align="center">Warnings</StyledTableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{scoreList.map((item, index) => {
									var { schoolYear, semester, gpaSemester, gpa, classification } = item;
									var fCredit = fCredits.filter((item, index) => {
										return item.schoolYear === schoolYear && item.semester === semester;
									})[0];
									var warning = warnings.filter((item, index) => {
										return item.schoolYear === schoolYear && item.semester === semester;
									})[0];

									return (
										<StyledTableRow key={index}>
											<StyledTableCell align="center">{index + 1}</StyledTableCell>
											<StyledTableCell align="center">{schoolYear}</StyledTableCell>
											<StyledTableCell align="center">{semester}</StyledTableCell>
											<StyledTableCell align="center">{gpaSemester}</StyledTableCell>
											<StyledTableCell align="center">{gpa}</StyledTableCell>
											<StyledTableCell align="center">{classification}</StyledTableCell>
											<StyledTableCell align="center">
												{fCredit ? fCredit.credits : 0}
											</StyledTableCell>
											<StyledTableCell align="center">
												{warning
													? warning.reason.map((item, index) => {
															return (
																<div key={index}>
																	<Typography
																		key={index}
																		variant="caption"
																		display="block"
																		gutterBottom
																		align="left"
																	>
																		{item}
																	</Typography>
																	<hr />
																</div>
															);
													  })
													: ''}
											</StyledTableCell>
										</StyledTableRow>
									);
								})}
							</TableBody>
						</Table>
					</TableContainer>
				</AccordionDetails>
			</Accordion>
		</div>
	);
};

export default GPATable;
