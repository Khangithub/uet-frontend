import React, { useState } from 'react';
import { InputBase, makeStyles } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	InputBaseStyle: {
		'&::placeholder': {
			fontSize: '50px',
			lineHeight: '60px',
			fontFamily: 'medium-content-title-font, Georgia, Cambria, Times New Roman, Times, serif',
		},
		width: '100%',
		fontSize: '50px',
		lineHeight: '60px',
		fontFamily: 'medium-content-title-font, Georgia, Cambria, Times New Roman, Times, serif',
	},
}));

// openSearch: Boolean
// setOpenSearch: Function, no params
const SearchBox = ({ openSearch, setOpenSearch, history, location, match }) => {
	const classes = useStyles();
	const { innerWidth } = window;
	const [keyword, setKeyword] = useState('');

	const handleSubmit = (event) => {
		event.preventDefault();
		setOpenSearch(false);
		history.push({
			pathname: '/search',
			search: `?keyword=${keyword}`,
			state: {
				keyword,
			},
		});
	};

	return (
		<form noValidate autoComplete="off" onSubmit={handleSubmit}>
			<InputBase
				style={
					openSearch
						? { width: `${innerWidth * 0.9}px` }
						: {
								width: `${innerWidth * 0.9}px`,
								display: 'none',
						  }
				}
				placeholder='Type and press "Enter" to search…'
				classes={{
					input: classes.InputBaseStyle,
				}}
				onChange={(event) => {
					setKeyword(event.target.value);
				}}
				InputProps={{ fullWidth: true }}
				autoFocus
				margin
			/>
		</form>
	);
};

export default withRouter(SearchBox);
