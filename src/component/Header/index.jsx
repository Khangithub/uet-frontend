import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { AppBar, Grid, IconButton, Toolbar, ClickAwayListener } from '@material-ui/core';

import SettingButton from './SettingButton';

import SearchBox from './SearchBox';

import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import CancelIcon from '@material-ui/icons/Cancel';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: 36,
	},
	hide: {
		display: 'none',
	},
}));

//handleDrawerOpen: function, no params
//open: bool
const Index = ({ handleDrawerOpen, open, history, location, match }) => {
	const classes = useStyles();
	const { innerWidth } = window;
	let [screenWidth, setScreenWidth] = useState(innerWidth);
	var [openSearch, setOpenSearch] = useState(false);

	useEffect(() => {
		window.onresize = () => {
			setScreenWidth(window.innerWidth);
		};
	}, [innerWidth]);

	const handleClickAway = () => {
		setOpenSearch(false);
	};

	return (
		<ClickAwayListener onClickAway={handleClickAway}>
			<AppBar
				color="inherit"
				className={clsx(classes.appBar, {
					[classes.appBarShift]: open,
				})}
			>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						edge="start"
						className={clsx(classes.menuButton, {
							[classes.hide]: open,
						})}
					>
						<MenuIcon />
					</IconButton>
					<Grid container direction="row" justify="center" alignItems="center">
						<Grid container xs={6} justify={screenWidth < 600 ? 'center' : 'flex-start'}>
							<Grid item onClick={() => history.push('/')}>
								<img
									src="http://dangkyhoc.daotao.vnu.edu.vn/Images/logo.png"
									alt=""
									style={{ height: '65px' }}
								/>
							</Grid>
						</Grid>
						<Grid container xs={6} direction="row" justify="flex-end" alignItems="center">
							<Grid item>
								<IconButton onClick={() => setOpenSearch(!openSearch)}>
									{openSearch ? <CancelIcon fontSize="large" /> : <SearchIcon fontSize="large" />}
								</IconButton>
							</Grid>
							<Grid item>
								<SettingButton />
							</Grid>
						</Grid>
						<Grid item>
							<SearchBox openSearch={openSearch} setOpenSearch={setOpenSearch} />
						</Grid>
					</Grid>
				</Toolbar>
			</AppBar>
		</ClickAwayListener>
	);
};

export default withRouter(Index);
