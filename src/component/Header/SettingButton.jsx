import React, {useContext, useState} from 'react';
import Cookies from 'universal-cookie';
import {
  MenuItem,
  Grid,
  Divider,
  IconButton,
  ClickAwayListener,
  CircularProgress,
  Avatar,
} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';

import MailIcon from '@material-ui/icons/Mail';
import RoleIcon from '@material-ui/icons/Work';

import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import 'tippy.js/animations/perspective.css';

const SettingButton = ({history, match, location}) => {
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const cookies = new Cookies();

  const [visible, setVisible] = useState(false);

  const handleClick = () => {
    setVisible((prev) => !prev);
  };

  const handleClickAway = () => {
    setVisible(false);
  };

  return getCurrentUserLoading ? (
    <CircularProgress />
  ) : (
    <ClickAwayListener onClickAway={handleClickAway}>
      <Tippy
        visible={visible}
        theme={'light'}
        interactive={true}
        placement={'top'}
        maxWidth={500}
        content={
          <div>
            <MenuItem>
              <Grid
                containerdirection="column"
                justify="flex-start"
                alignItems="center"
              >
                <Grid item>
                  <div style={{display: 'flex', alignItems: 'center'}}>
                    <MailIcon
                      color="action"
                      fontSize="small"
                      style={{marginRight: '15px'}}
                    />
                    <span>{currentUser.vnumail}</span>
                  </div>
                </Grid>
                <Grid item>
                  <div style={{display: 'flex', alignItems: 'center'}}>
                    <RoleIcon
                      color="action"
                      fontSize="small"
                      style={{marginRight: '15px'}}
                    />
                    <span>{currentUser.role}</span>
                  </div>
                </Grid>
              </Grid>
            </MenuItem>
            <Divider />
            <MenuItem>My account</MenuItem>
            <MenuItem
              onClick={() => {
                cookies.remove('token');
                history.push('/login');
              }}
            >
              Logout
            </MenuItem>
          </div>
        }
      >
        <IconButton onClick={handleClick}>
          <Avatar
            src={currentUser.profileImage}
            alt={currentUser.profileImage}
          />
        </IconButton>
      </Tippy>
    </ClickAwayListener>
  );
};

export default withRouter(SettingButton);
