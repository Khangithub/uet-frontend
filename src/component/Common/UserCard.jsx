import React from 'react';
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
});
const UserCard = ({user}) => {
  const {profileImage, role, code, vnumail, fullname} = user;
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="140"
          image={profileImage}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {fullname}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            code: {code}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            role: {role}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            vnumail: {vnumail}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default UserCard;
