import React, {useState} from 'react';
import JSONPretty from 'react-json-pretty';

import {makeStyles} from '@material-ui/core/styles';
import {Backdrop, Button, Fade, Modal, Grid} from '@material-ui/core';
import flattenObject from '../../utils/object';
var JSONPrettyMon = require('react-json-pretty/dist/1337');

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    outline: 'none',
    padding: theme.spacing(2, 4, 3),
  },
}));

// open: Boolean
// setOpen: Function
// responseJSON: Object
// isError: Boolean
const ResponseModal = ({open, setOpen, responseJSON, isError}) => {
  const classes = useStyles();
  var flattenResponse = flattenObject(responseJSON);
  var message = '';

  if (responseJSON.message) {
    message = responseJSON.message;
  } else {
    var messageKey = Object.keys(flattenResponse).filter((key, index) => {
      return index && key.includes('message');
    })[0];
    message = flattenResponse[messageKey];
  }

  const [showFullJSON, setShowFullJSON] = useState(false);

  return (
    <div id="error">
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <h4 style={{color: isError ? '#f50057' : '#3f51b5'}}>
                  {isError ? `Error: ${message}` : `Message: ${message}`}
                </h4>
              </Grid>
              <Grid
                item
                style={
                  showFullJSON
                    ? {width: '100%', minWidth: '300px'}
                    : {display: 'none'}
                }
              >
                <JSONPretty
                  style={{
                    height: '400px',
                    overflow: 'scroll',
                  }}
                  data={flattenResponse}
                  theme={JSONPrettyMon}
                ></JSONPretty>
              </Grid>

              <Grid
                container
                direction="row"
                justify="space-between"
                spacing={3}
                alignItems="center"
              >
                <Grid item>
                  <Button
                    variant="contained"
                    fullWidth
                    onClick={() => {
                      setOpen(false);
                    }}
                  >
                    {isError ? 'Cancel' : 'OK'}
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant="contained"
                    fullWidth
                    color={isError ? 'secondary' : 'primary'}
                    onClick={() => {
                      setShowFullJSON(!showFullJSON);
                    }}
                  >
                    {showFullJSON
                      ? `Hide ${isError ? 'Error' : 'Message'}`
                      : `Show Full ${isError ? 'Error' : 'Message'}`}
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default ResponseModal;
