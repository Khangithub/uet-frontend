import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Fade, Backdrop, CircularProgress } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	paper: {
		outline: 'none',
	},
}));

const LoadingModal = ({ loading, setLoading }) => {
	const classes = useStyles();

	return (
		<Modal
			aria-labelledby="transition-modal-title"
			aria-describedby="transition-modal-description"
			className={classes.modal}
			open={loading}
			onClose={() => {
				setLoading(false);
			}}
			closeAfterTransition
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 500,
			}}
		>
			<Fade in={loading}>
				<div className={classes.paper}>
					<CircularProgress size={150} />
				</div>
			</Fade>
		</Modal>
	);
};

export default LoadingModal;
