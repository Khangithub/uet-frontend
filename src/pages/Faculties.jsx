import React, {useState} from 'react';
import Cookies from 'universal-cookie';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import Header from '../component/Header/';
import Sidebar from '../component/Sidebar/';
import FacultiesMain from '../component/Main/FacultiesMain';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
    marginTop: '65px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Faculties = ({history, location, match}) => {
  const cookies = new Cookies();
  const token = cookies.get('token');

  if (token === undefined) {
    history.push('/login');
  }
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Header open={open} handleDrawerOpen={handleDrawerOpen} />
      <Sidebar open={open} handleDrawerClose={handleDrawerClose} />
      <main className={classes.content}>
        <div className={classes.toolbar}>
          <FacultiesMain />
        </div>
      </main>
    </div>
  );
};

export default withRouter(Faculties);
