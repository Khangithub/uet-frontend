import React, { useContext, useState } from 'react';
import Cookies from 'universal-cookie';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { Grid } from '@material-ui/core';
import { CurrentUserContext } from '../providers/CurrentUserContextProvider';

import Header from '../component/Header';
import Sidebar from '../component/Sidebar';
import ChangeProfileImage from '../component/Main/PersonalizeMain/ChangeProfileImage';
import ChangePassword from '../component/Main/PersonalizeMain/ChangePassword';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
	},
	toolbar: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		padding: theme.spacing(0, 1),
		marginTop: '65px',
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
	},
}));

const Personalize = ({ history, location, match }) => {
	const classes = useStyles();
	const cookies = new Cookies();
	const token = cookies.get('token');

	if (token === undefined) {
		history.push('/login');
	}

	const { currentUser } = useContext(CurrentUserContext);
	const { profileImage } = currentUser;
	const [open, setOpen] = useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (
		<div className={classes.root}>
			<Header open={open} handleDrawerOpen={handleDrawerOpen} />
			<Sidebar open={open} handleDrawerClose={handleDrawerClose} />
			<main className={classes.content}>
				<div className={classes.toolbar}>
					<Grid container justify="space-evenly" alignItems="center">
						<Grid item>
							<ChangeProfileImage profileImage={profileImage} />
						</Grid>

						<Grid item>
							<ChangePassword />
						</Grid>
					</Grid>
				</div>
			</main>
		</div>
	);
};

export default withRouter(Personalize);
