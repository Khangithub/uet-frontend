import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import Header from '../component/Header';
import Sidebar from '../component/Sidebar';
import InfoBar from '../component/Main/StudentMain/InfoBar';
import ParentInfo from '../component/Main/StudentMain/ParentsInfo/';
import GPATable from '../component/Main/StudentMain/GPATable';
import LoadingModal from '../component/Common/LoadingModal';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    marginTop: '65px',
    display: 'block',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Student = ({history, location, match}) => {
  const classes = useStyles();
  const {studentCode} = match.params;
  const [student, setStudent] = useState({});
  const [parents, setParents] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchStudentData = async () => {
      try {
        var studentResponse = await fetch(
          `https://uet-backend.herokuapp.com/students/with/code/${studentCode}`
        );
        var studentJson = await studentResponse.json();

        var parentsResponse = await fetch(
          `https://uet-backend.herokuapp.com/parents/${studentCode}`
        );
        var parentsJson = await parentsResponse.json();

        if (studentJson.doc && parentsJson.doc) {
          setStudent(studentJson.doc);
          setParents(parentsJson.doc);
          setLoading(false);
        }
      } catch (err) {
        console.error('pages/Student.jsx', err);
        setLoading(false);
        alert(JSON.stringify(err));
      }
    };

    fetchStudentData();
  }, [studentCode]);

  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      {loading ||
      student === null ||
      student === undefined ||
      parents === null ||
      parents === undefined ? (
        <LoadingModal loading={loading} setLoading={setLoading} />
      ) : (
        <>
          <Header open={open} handleDrawerOpen={handleDrawerOpen} />
          <Sidebar open={open} handleDrawerClose={handleDrawerClose} />
          <main className={classes.content}>
            <div className={classes.toolbar}>
              <InfoBar student={student} />
              <GPATable student={student} />
              <ParentInfo
                currentParents={parents}
                code={studentCode}
                isCurrentUser={false}
              />
            </div>
          </main>
        </>
      )}
    </div>
  );
};

export default withRouter(Student);
