import React, {useState, useEffect} from 'react';
import Highlighter from 'react-highlight-words';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {
  Grid,
  List,
  ListItemAvatar,
  ListItemText,
  ListItem,
  Avatar,
  Divider,
  Typography,
} from '@material-ui/core';
import Header from '../component/Header/';
import Sidebar from '../component/Sidebar/';
import LoadingModal from '../component/Common/LoadingModal';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
    marginTop: '65px',
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Home = ({history, location, match}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  var {keyword} = location.state;
  const [result, setResult] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`https://uet-backend.herokuapp.com/users/search/${keyword}`)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        var {docs} = json;
        setLoading(false);
        setResult(docs);
      })
      .catch((error) => {
        setLoading(false);
        console.error(error);
      });
  }, [keyword]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Header open={open} handleDrawerOpen={handleDrawerOpen} />
      <Sidebar open={open} handleDrawerClose={handleDrawerClose} />
      <main className={classes.content}>
        <div className={classes.toolbar}>
          {loading ? (
            <LoadingModal loading={loading} setLoading={setLoading} />
          ) : (
            <div>
              <Typography
                variant="overline"
                display="block"
                gutterBottom
                align="left"
              >
                {result.length} {result.length > 1 ? 'results' : 'result'} for
                keyword '{keyword}'
              </Typography>
              <Grid container spacing={2}>
                {result.map((resultItem, index) => {
                  const {profileImage, fullname, code, role} = resultItem;

                  return (
                    <List
                      className={classes.listContainer}
                      key={index}
                      onClick={() => {
                        history.push(role === "student" ? `/student/${code}`:`/user/${role}/${code}`);
                      }}
                    >
                      <ListItem>
                        <ListItemAvatar>
                          <Avatar
                            src={profileImage}
                            sizes="small"
                            alt="profileImage"
                          />
                        </ListItemAvatar>
                        <ListItemText
                          primary={
                            <Highlighter
                              searchWords={[keyword]}
                              autoEscape={true}
                              textToHighlight={fullname}
                            />
                          }
                          secondary={
                            <>
                              <Divider />
                              <span>
                                {
                                  <Highlighter
                                    searchWords={[keyword]}
                                    autoEscape={true}
                                    textToHighlight={code}
                                  />
                                }
                                &nbsp;|&nbsp;
                                <Highlighter
                                  searchWords={[keyword]}
                                  autoEscape={true}
                                  textToHighlight={role}
                                />
                              </span>
                            </>
                          }
                        />
                      </ListItem>
                    </List>
                  );
                })}
              </Grid>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default withRouter(Home);
