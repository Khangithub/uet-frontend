import React, {useState} from 'react';
import Cookies from 'universal-cookie';

import {withRouter} from 'react-router-dom';
import Student from '../component/Main/LoginMain/Student';
import Staff from '../component/Main/LoginMain/Staff';

const Login = ({history, location, match}) => {
  const [isStudent, setStudent] = useState(true);
  const cookies = new Cookies();
  cookies.remove('token');

  return (
    <>
      <Student setStudent={setStudent} isStudent={isStudent} />
      <Staff setStudent={setStudent} isStudent={isStudent} />
    </>
  );
};

export default withRouter(Login);
