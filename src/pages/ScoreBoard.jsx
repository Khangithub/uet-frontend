import React, {useState, useEffect, useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import {CurrentUserContext} from '../providers/CurrentUserContextProvider';
import {CurrentParentsContext} from '../providers/CurrentParentsContextProvider';
import Cookies from 'universal-cookie';

import Header from '../component/Header';
import Sidebar from '../component/Sidebar';
import InfoBar from '../component/Main/StudentMain/InfoBar';
import GPATable from '../component/Main/StudentMain/GPATable';
import ParentInfo from '../component/Main/StudentMain/ParentsInfo/';
import LoadingModal from '../component/Common/LoadingModal';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    marginTop: '65px',
    display: 'block',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Student = ({history, location, match}) => {
  const classes = useStyles();
  const {studentCode} = match.params;
  const {currentUser, getCurrentUserLoading} = useContext(CurrentUserContext);
  const {currentParents, getCurrentParentsLoading} = useContext(
    CurrentParentsContext
  );
  const [student, setStudent] = useState({});
  const [getStudentLoading, setGetStudentLoading] = useState(true);

  const [open, setOpen] = useState(false);
  const cookies = new Cookies();
  const token = cookies.get('token');

  useEffect(() => {
    if (currentUser && currentUser.role !== 'student') {
      return history.goBack();
    }

    const fetchStudentScoreBoard = async () => {
      try {
        var studentResponse = await fetch(
          `https://uet-backend.herokuapp.com/students/with/code/${currentUser.code}`
        );
        var studentJson = await studentResponse.json();

        if (studentJson.doc) {
          setStudent(studentJson.doc);
          setGetStudentLoading(false);
        }
      } catch (err) {
        console.error('pages/ScoreBoard.jsx', err);
        setGetStudentLoading(false);
        alert(JSON.stringify(err));
      }
    };

    fetchStudentScoreBoard();
  }, [studentCode, currentUser, history, token]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      {getStudentLoading ||
      getCurrentParentsLoading ||
      getCurrentUserLoading ? (
        <LoadingModal
          getCurrentUserLoading={getStudentLoading}
          setLoading={setGetStudentLoading}
        />
      ) : (
        <>
          <Header open={open} handleDrawerOpen={handleDrawerOpen} />
          <Sidebar open={open} handleDrawerClose={handleDrawerClose} />
          <main className={classes.content}>
            <div className={classes.toolbar}>
              <InfoBar student={student} />
              <GPATable student={student} />
              <ParentInfo
                currentParents={currentParents}
                code={currentUser.code}
                isCurrentUser={true}
              />
            </div>
          </main>
        </>
      )}
    </div>
  );
};

export default withRouter(Student);
