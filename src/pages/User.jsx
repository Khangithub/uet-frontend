import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';
import Cookies from 'universal-cookie';

import Header from '../component/Header';
import Sidebar from '../component/Sidebar';
import ConsultantMain from '../component/Main/UserMain/ConsultantMain';
import SpecialistMain from '../component/Main/UserMain/SpecialistMain';
import StudentAffairLeaderMain from '../component/Main/UserMain/StudentAffairLeaderMain';
import UetLeaderMain from '../component/Main/UserMain/UetLeaderMain';
import AcacdemicLeaderMain from '../component/Main/UserMain/AcacdemicLeaderMain';
import LoadingModal from '../component/Common/LoadingModal';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    marginTop: '65px',
    display: 'block',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const User = ({history, location, match}) => {
  const classes = useStyles();
  const {code, role} = match.params;
  const [open, setOpen] = useState(false);
  var [loading, setLoading] = useState(true);
  var [user, setUser] = useState({});

  const cookies = new Cookies();
  const token = cookies.get('token');

  useEffect(() => {
    fetch(`https://uet-backend.herokuapp.com/users/${role}/${code}`, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer '.concat(token),
        'content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        setLoading(false);
        setUser({
          ...json.doc,
          isManager: [
            'specialist',
            'student-affair-leader',
          ].includes(json.doc.role)
            ? true
            : false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, [code, role, token]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <>
        <Header open={open} handleDrawerOpen={handleDrawerOpen} />
        <Sidebar open={open} handleDrawerClose={handleDrawerClose} />
        <main className={classes.content}>
          <div className={classes.toolbar}>
            {loading ? (
              <LoadingModal loading={loading} setLoading={setLoading} />
            ) : (
              (user.role === 'consultant' && <ConsultantMain user={user} />) ||
              (user.role === 'specialist' && <SpecialistMain user={user} />) ||
              (user.role === 'student-affair-leader' && (
                <StudentAffairLeaderMain user={user} />
              )) ||
              (user.role === 'uet-leader' && <UetLeaderMain user={user} />) ||
              (user.role === 'academic-leader' && (
                <AcacdemicLeaderMain user={user} />
              ))
            )}
          </div>
        </main>
      </>
    </div>
  );
};

export default withRouter(User);
