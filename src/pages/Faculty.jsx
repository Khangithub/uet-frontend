import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import Header from '../component/Header';
import Sidebar from '../component/Sidebar';

import FacultyMain from '../component/Main/FacultyMain/';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
    marginTop: '65px',
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Faculty = ({history, location, match}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  var {code} = match.params;

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Header open={open} handleDrawerOpen={handleDrawerOpen} />
      <Sidebar open={open} handleDrawerClose={handleDrawerClose} />
      <main className={classes.content}>
        <div className={classes.toolbar}>
          <FacultyMain code={code} />
        </div>
      </main>
    </div>
  );
};

export default withRouter(Faculty);
