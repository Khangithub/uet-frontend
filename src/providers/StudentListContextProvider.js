import React, { useState } from 'react'

export const StudentListContext = React.createContext()

const StudentListContextProvider = props => {
  var [studentList, setStudentList] = useState([])
  var [getStudentListLoading, setStudentListLoading] = useState(true)

  return (
    <StudentListContext.Provider
      value={{
        studentList,
        getStudentListLoading,
        setStudentList,
        setStudentListLoading
      }}
    >
      {props.children}
    </StudentListContext.Provider>
  )
}

export default StudentListContextProvider
