import React, {useEffect, useState} from 'react';
import Cookies from 'universal-cookie';

export const CurrentParentsContext = React.createContext();

export default function CurrentParentsContextProvider(props) {
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [currentParents, setParents] = useState({});
  let [getCurrentParentsLoading, setCurrentParentsLoading] = useState(true);

  useEffect(() => {
    const fetchCurrentParents = async () => {
      try {
        var currentParentsResponse = await fetch(
          'https://uet-backend.herokuapp.com/parents/me',
          {
            headers: {
              Authorization: 'Bearer '.concat(token),
              'content-type': 'application/json; charset=UTF-8',
            },
          }
        );

        var currentParentsJson = await currentParentsResponse.json();

        if (currentParentsJson.currentParents) {
          setCurrentParentsLoading(false);
          setParents(currentParentsJson.currentParents);
        }
      } catch (err) {
        console.error('currentParenstContextProvider', err);
        setCurrentParentsLoading(false);
      }
    };

    fetchCurrentParents();
  }, [token]);

  return (
    <CurrentParentsContext.Provider
      value={{
        currentParents,
        getCurrentParentsLoading,
        setParents,
        setCurrentParentsLoading,
      }}
    >
      {props.children}
    </CurrentParentsContext.Provider>
  );
}
