import React, {useEffect, useState} from 'react';
import Cookies from 'universal-cookie';

export const CurrentUserContext = React.createContext();

export default function CurrentUserContextProvider(props) {
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [currentUser, setUser] = useState({});
  let [getCurrentUserLoading, setCurrentUserLoading] = useState(true);

  useEffect(() => {
    const fetchCurrentUser = async () => {
      try {
        var currentUserResponse = await fetch(
          'https://uet-backend.herokuapp.com/users/me',
          {
            headers: {
              Authorization: 'Bearer '.concat(token),
              'content-type': 'application/json; charset=UTF-8',
            },
          }
        );

        var currentUserJson = await currentUserResponse.json();

        if (currentUserJson.currentUser.role) {
          setCurrentUserLoading(false);
          setUser({
            ...currentUserJson.currentUser,
            isManager: ['specialist', 'student-affair-leader'].includes(
              currentUserJson.currentUser.role
            )
              ? true
              : false,
          });
        }
      } catch (err) {
        console.error('CurrentUsercontextProvider', err);
        setCurrentUserLoading(false);
      }
    };

    fetchCurrentUser();
  }, [token]);

  return (
    <CurrentUserContext.Provider
      value={{
        currentUser,
        getCurrentUserLoading,
        setUser,
        setCurrentUserLoading,
      }}
    >
      {props.children}
    </CurrentUserContext.Provider>
  );
}
