import React, {useState, useEffect} from 'react';
import Cookies from 'universal-cookie';

export const FacultyContext = React.createContext();

const FacultyContextProvider = (props) => {
  var [facultyList, setFacultyList] = useState([]);
  var [getFacultyListLoading, setFacultyListLoading] = useState(true);

  const cookies = new Cookies();
  const token = cookies.get('token');

  useEffect(() => {
    const fetFacultyList = async (token) => {
      try {
        var facultyListResponse = await fetch(
          'https://uet-backend.herokuapp.com/faculties/',
          {
            method: 'GET',
            headers: {
              Authorization: 'Bearer '.concat(token),
              'content-type': 'application/json; charset=UTF-8',
            },
          }
        );
        var facultyListJson = await facultyListResponse.json();

        if (facultyListJson.docs.length > 0) {
          setFacultyListLoading(false);
          setFacultyList(facultyListJson.docs);
        }
      } catch (err) {
        console.error(err, 'FacultyContextProvider');
        setFacultyListLoading(false);
      }
    };

    fetFacultyList(token);
  }, [token]);

  return (
    <FacultyContext.Provider
      value={{
        facultyList,
        getFacultyListLoading,
        setFacultyList,
        setFacultyListLoading,
      }}
    >
      {props.children}
    </FacultyContext.Provider>
  );
};

export default FacultyContextProvider;
