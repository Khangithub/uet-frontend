import React, {useState, useEffect} from 'react';
import Cookies from 'universal-cookie';

export const NoneStudentListContext = React.createContext();

const NoneStudentListContextProvider = (props) => {
  var [noneStudentList, setNoneStudentList] = useState([]);
  var [getNoneStudentListLoading, setNoneStudentListLoading] = useState(true);

  const cookies = new Cookies();
  const token = cookies.get('token');

  useEffect(() => {
    const fetchNoneStudentList = async () => {
      try {
        var noneStudentListResponse = await fetch(
          'https://uet-backend.herokuapp.com/users/none/student/user',
          {
            method: 'GET',
            headers: {
              Authorization: 'Bearer '.concat(token),
              'content-type': 'application/json; charset=UTF-8',
            },
          }
        );

        var noneStudentListJson = await noneStudentListResponse.json();

        if (noneStudentListJson.users.length > 0) {
          setNoneStudentListLoading(false);
          setNoneStudentList(noneStudentListJson.users);
        }
      } catch (err) {
        console.error(err, 'NoneStudentListContext');
        setNoneStudentListLoading(false);
      }
    };

    fetchNoneStudentList();
  }, [token]);

  return (
    <NoneStudentListContext.Provider
      value={{
        noneStudentList,
        getNoneStudentListLoading,
        setNoneStudentList,
        setNoneStudentListLoading,
      }}
    >
      {props.children}
    </NoneStudentListContext.Provider>
  );
};

export default NoneStudentListContextProvider;
