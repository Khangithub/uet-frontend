import React, {useEffect, useState} from 'react';

export const ClassListContext = React.createContext();

function ClassListContextProvider(props){
  var [classList, setClassList] = useState([]);
  var [getClassListLoading, setClassListLoading] = useState(true);

  useEffect(() => {
    const fetchClassList = async () => {
      try {
        var classListResponse = await fetch('https://uet-backend.herokuapp.com/classes/');
        var classListJson = await classListResponse.json();

        if (classListJson.docs.length > 0) {
          setClassListLoading(false);
          setClassList(classListJson.docs);
        }
      } catch (err) {
        setClassListLoading(false);
        console.error('ClassListContextProvider', err);
      }
    };

    fetchClassList();
  }, [setClassList, setClassListLoading]);

  return (
    <ClassListContext.Provider
      value={{
        classList,
        getClassListLoading,
        setClassList,
        setClassListLoading,
      }}
    >
      {props.children}
    </ClassListContext.Provider>
  );
};

export default ClassListContextProvider;
