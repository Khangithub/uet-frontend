exports.getFromLocalStorage = name => {
  return JSON.parse(localStorage.getItem(name))
}

exports.toCapitalize = text => {
  return text.replace(/\b\w/g, l => l.toUpperCase())
}

exports.formatClassname = classname => {
  var uppercase = classname.toUpperCase()
  return (
    uppercase.slice(0, 3) +
    ' ' +
    uppercase.slice(3, 5) +
    ' ' +
    uppercase.slice(5, 8) +
    ' ' +
    uppercase.slice(8, 9)
  )
}

exports.formatAcademicRank = rank => {
  return (
    (rank === 'master' && 'Ms') ||
    (rank === 'phd' && 'Phd') ||
    (rank === 'professor' && 'Prof')
  )
}

exports.formatId = _id => {
  return _id.split('-')[0]
}

exports.convertFullnametoEngLish = fullname => {
  if (fullname === null || fullname === undefined) return fullname
  fullname = fullname.toLowerCase()
  fullname = fullname.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
  fullname = fullname.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
  fullname = fullname.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
  fullname = fullname.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
  fullname = fullname.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
  fullname = fullname.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
  fullname = fullname.replace(/đ/g, 'd')

  var order =
    fullname.split(' ')[fullname.split(' ').length - 2] +
    ' , ' +
    fullname
      .split(' ')
      .slice(0, fullname.split(' ').length - 2)
      .join(' ')
  return order.replace(/\b\w/g, l => l.toUpperCase())
}
